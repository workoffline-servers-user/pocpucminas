﻿using PocPucMinas.QosLib.Classes;
using PocPucMinas.QosLib.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Gelf.Extensions.Logging;
using Microsoft.Extensions.Logging;
using PocPucMinas.CommonLib.Extensions;
using PocPucMinas.CommonLib.DependencyInjection;

namespace PocPucMinas.QosLib.DependencyInjection
{
    public static class IServiceCollectionExtensions
    {
        public static IServiceCollection AdicionaModuloQos(this IServiceCollection input) => input
            .AddSingleton<IQosService, QosService>();

        public static void AdicionaModuloLog(this IServiceCollection services)
        {
            var configuration = services.GetConfiguration();
            services.AddLogging(configure =>
            {
                var loggingSectionAppSettings = configuration.GetSection("Logging");
                configure.AddConfiguration(loggingSectionAppSettings);

                if (!configuration.IsAmbienteProducao())
                {
                    configure.AddConsole();
                    configure.AddDebug();
                }

                configure.AddGelf(options =>
                {
                    options.AdditionalFields["Identificador do Servidor"] = configuration.GetIdentificadorServidor();
                    options.AdditionalFields["Identificador do Webservice"] = configuration.GetIdentificadorAplicacao();
                    options.AdditionalFields["Versão do Webservice"] = configuration.GetVersaoAplicacao();
                });
            });
        }
    }
}
