﻿using PocPucMinas.QosLib.Interfaces;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;

namespace PocPucMinas.QosLib.Classes
{
    public class QosService : IQosService
    {
        private const string TEMPLATE_QOS_MENSAGEM_COM_CONTEXTO = "{Qos}::{Contexto}::{Categoria}::";
        private const string TEMPLATE_QOS_MENSAGEM_SEM_CONTEXTO = "{Qos}::{Categoria}::";
        private const string QOS_ERRO = "ERRO";
        private const string QOS_ESTATISTICO = "ESTATISTICO";

        ILogger Logger;
        public QosService(ILogger<IQosService> logger)
        {
            Logger = logger;
        }

        private object[] GetParamsComContexto(string tipoQOS, string contexto, string categoria, object[] args)
        {
            var list = new List<object> { tipoQOS, contexto, categoria };
            list.AddRange(args);

            return list.ToArray();
        }

        private object[] GetParamsSemContexto(string tipoQOS, string categoria, object[] args)
        {
            var list = new List<object> { tipoQOS, categoria };
            list.AddRange(args);

            return list.ToArray();
        }

        public void GravaQosErro(string contexto, string categoria, string mensagem = "", params object[] args)
        {
            Logger.LogError(TEMPLATE_QOS_MENSAGEM_COM_CONTEXTO + mensagem, GetParamsComContexto(QOS_ERRO, contexto, categoria, args));
        }

        public void GravaQosErro(string categoria, string mensagem = "", params object[] args)
        {
            Logger.LogError(TEMPLATE_QOS_MENSAGEM_COM_CONTEXTO + mensagem, GetParamsSemContexto(QOS_ERRO, categoria, args));
        }

        public void GravaQosErro(Exception exception, string categoria, string mensagem = "", params object[] args)
        {
            Logger.LogError(exception, TEMPLATE_QOS_MENSAGEM_SEM_CONTEXTO + mensagem, GetParamsSemContexto(QOS_ERRO, categoria, args));
        }

        public void GravaQosErro(Exception exception, string contexto, string categoria, string mensagem = "", params object[] args)
        {
            Logger.LogError(exception, TEMPLATE_QOS_MENSAGEM_COM_CONTEXTO + mensagem, GetParamsComContexto(QOS_ERRO, contexto, categoria, args));
        }

        public void GravaQosEstatistico(string contexto, string categoria, string mensagem, params object[] args)
        {
            Logger.LogInformation(TEMPLATE_QOS_MENSAGEM_COM_CONTEXTO + mensagem, GetParamsComContexto(QOS_ESTATISTICO, contexto, categoria, args));
        }

        public void GravaQosEstatistico(string categoria, string mensagem, params object[] args)
        {
            Logger.LogInformation(TEMPLATE_QOS_MENSAGEM_SEM_CONTEXTO + mensagem, GetParamsSemContexto(QOS_ESTATISTICO, categoria, args));
        }
    }
}
