﻿using System;

namespace PocPucMinas.QosLib.Interfaces
{
    public interface IQosService
    {
        void GravaQosErro(string contexto, string categoria, string mensagem = "", params object[] args);
        void GravaQosErro(string categoria, string mensagem = "", params object[] args);
        void GravaQosErro(Exception exception, string categoria, string mensagem = "", params object[] args);
        void GravaQosErro(Exception exception, string contexto, string categoria, string mensagem = "", params object[] args);
        void GravaQosEstatistico(string contexto, string categoria, string mensagem, params object[] args);
        void GravaQosEstatistico(string categoria, string mensagem, params object[] args);
    }
}
