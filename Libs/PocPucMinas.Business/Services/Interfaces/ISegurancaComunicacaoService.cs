﻿using PocPucMinas.DataLib.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PocPucMinas.Business.Services.Interfaces
{
    public interface ISegurancaComunicacaoService
    {
        Task<int> EnviarMensagensParaDestinatariosPotencialmenteAtingidos();
        Task<List<int>> GetRegioesAtingidasAlertasNaUltimaHora();
        Task<List<DestinatarioAlerta>> RecuperarDestinatariosMensagens(int regiaoAlerta);
        Task<List<MensagemEnviada>> RecuperarMensagensEnviadas(int quantidadeMinutosPassados);
        Task<int> RecuperarQuantidadeDestinatariosAlertaPotencialmenteAtingidos(int regiaoAlerta);
    }
}
