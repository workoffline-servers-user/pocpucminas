﻿using PocPucMinas.DataLib.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PocPucMinas.Business.Services.Interfaces
{
    public interface IMonitoramentoBarragensService
    {
        Task<List<int>> GetIdsRegioesDeAlertaAtingidas();
        Task<List<MedicaoSensor>> GetListaDeMedicoesDeAltoRisco();
        Task<int> InserirMedicoesSimuladas(int quantidadePorSensor);
        Task<List<MedicaoSensor>> RecuperarMedicoes();
        Task<List<NivelRisco>> RecuperarNiveisDeRisco();
        Task<List<ParametroSensor>> RecuperarParametrosDeMedicao();
        Task<List<RegiaoAlerta>> RecuperarRegioesDeAlerta();
        Task<bool> VerificarExistenciaAlerta();
    }
}
