﻿using PocPucMinas.Business.Services.Interfaces;
using PocPucMinas.DataLib.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using PocPucMinas.AspNetCore.BusinessServices;
using PocPucMinas.CommonLib.Clients;
using PocPucMinas.CommonLib.Helpers;
using PocPucMinas.CommonLib.Services.Interfaces;
using PocPucMinas.DataDriver.UnitsOfWork;
using PocPucMinas.QosLib.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PocPucMinas.Business.Constants;

namespace PocPucMinas.Business.Services.Implementations.Aeroportos
{   
    /// <summary>
    /// Serviço responsável pela importação de aeroportos da plataforma Open Flight
    /// </summary>
    public class MonitoramentoBarragensService : BaseBusinessGenericService<MonitoramentoBarragensService>, IMonitoramentoBarragensService
    {
        /// <summary>
        /// Construtor padrão (invocado por DI)
        /// </summary>
        /// <param name="unitOfWork"></param>
        public MonitoramentoBarragensService(
            IBaseUnitOfWork unitOfWork,
            IQosService qosService,
            IAppSettingsLoaderService appSettingsLoaderService
        )
            : base(unitOfWork, qosService, appSettingsLoaderService)
        {

        }

        public async Task<List<ParametroSensor>> RecuperarParametrosDeMedicao()
        {
            return (await (_unitOfWork.Repository<ParametroSensor>().FindAllAsyncIncluding(
                                x => true,
                                x => x.Include(y => y.NivelRisco)
                                      .Include(y => y.TipoSensor)))).ToList();

        }

        public async Task<List<MedicaoSensor>> RecuperarMedicoes()
        {
            return (await _unitOfWork.Repository<MedicaoSensor>().FindAllAsyncIncluding(
                                x => true,
                                x => x.Include(y => y.Sensor)
                                      .ThenInclude(z => z.TipoSensor))).OrderBy(x => x.DataHoraMedicao).ToList();
        }

        public async Task<int> InserirMedicoesSimuladas(int quantidadePorSensor)
        {
            var listaSensores = await _unitOfWork.Repository<Sensor>().GetAllAsync();
            var listaTiposSensor = await _unitOfWork.Repository<TipoSensor>().GetAllAsync();

            var idMin = listaTiposSensor.Min(x => x.TipoSensorId);
            var idMax = listaTiposSensor.Max(x => x.TipoSensorId);

            var rand = new Random();
            foreach (var sensor in listaSensores)
            {
                for (var i = 0; i < quantidadePorSensor; i++)
                {
                    var medicao = new MedicaoSensor
                    {
                        SensorId = sensor.SensorId,
                        RegiaoAlertaId = sensor.RegiaoAlertaId,
                        DataHoraMedicao = DateTime.Now,
                        ValorMedido = rand.Next(idMax * -15, idMax * 15)
                    };

                    await _unitOfWork.Repository<MedicaoSensor>().AddAsync(medicao);
                }
            }

            return await _unitOfWork.Commit();

        }

        public async Task<List<MedicaoSensor>> GetListaDeMedicoesDeAltoRisco()
        {
            var lista = new List<MedicaoSensor>();
            var nivelRiscoAlto = await _unitOfWork.Repository<NivelRisco>().FindAsync(x => x.Descricao == "Alto");
            var parametros = (await (_unitOfWork.Repository<ParametroSensor>().FindAllAsync(x => x.NivelRiscoId == nivelRiscoAlto.NivelRiscoId))).ToList();

            foreach (var parametro in parametros)
            {
                var dataHora = DateTime.Now.AddHours(-1);
                var listaMedicoesTipoSensor = await (_unitOfWork.Repository<MedicaoSensor>()
                    .FindAllAsyncIncluding(o => o.DataHoraMedicao > dataHora && o.Sensor.TipoSensorId == parametro.TipoSensorId &&
                                (o.ValorMedido >= parametro.AnormalAcimaDe || o.ValorMedido <= parametro.AnormalAbaixoDe), o => o.Include(x => x.Sensor)));

                lista.AddRange(listaMedicoesTipoSensor);
            }

            return lista;
        }

        public async Task<List<NivelRisco>> RecuperarNiveisDeRisco()
        {
            return (await (_unitOfWork.Repository<NivelRisco>().FindAllAsync(x => true))).ToList();
        }

        public async Task<List<RegiaoAlerta>> RecuperarRegioesDeAlerta()
        {
            return (await (_unitOfWork.Repository<RegiaoAlerta>().FindAllAsync(x => true))).ToList();
        }

        public async Task<List<int>> GetIdsRegioesDeAlertaAtingidas()
        {
            var listaMedicoes = await GetListaDeMedicoesDeAltoRisco();
            return listaMedicoes.Select(x => x.RegiaoAlertaId).Distinct().OrderBy(x => x).ToList();
        }


        public async Task<bool> VerificarExistenciaAlerta()
        {
            return (await GetListaDeMedicoesDeAltoRisco()).Count > 0;
        }



    }

}
