﻿using Microsoft.EntityFrameworkCore;
using PocPucMinas.AspNetCore.BusinessServices;
using PocPucMinas.AspNetCore.Security.Services.Interfaces;
using PocPucMinas.Business.Constants;
using PocPucMinas.Business.Extensions;
using PocPucMinas.Business.Services.Interfaces;
using PocPucMinas.CommonLib.Clients;
using PocPucMinas.CommonLib.Services.Interfaces;
using PocPucMinas.DataDriver.UnitsOfWork;
using PocPucMinas.DataLib.Models;
using PocPucMinas.QosLib.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PocPucMinas.Business.Services.Implementations
{
    public class SegurancaComunicacaoService : BaseBusinessGenericService<SegurancaComunicacaoService>, ISegurancaComunicacaoService
    {
        private readonly IAutenticacaoService _autenticacaoService;

        public SegurancaComunicacaoService(
            IBaseUnitOfWork unitOfWork,
            IQosService qosService,
            IAutenticacaoService autenticacaoService,
            IAppSettingsLoaderService appSettingsLoaderService
        )
            : base(unitOfWork, qosService, appSettingsLoaderService)
        {
            _autenticacaoService = autenticacaoService;
        }

        public async Task<List<DestinatarioAlerta>> RecuperarDestinatariosMensagens(int regiaoAlerta)
        {
            return (await (_unitOfWork.Repository<DestinatarioAlerta>().FindAllAsyncIncluding(x => x.RegiaoAlertaId == regiaoAlerta, x => x.Include(o => o.RegiaoAlerta)))).ToList();
        }

        public async Task<int> RecuperarQuantidadeDestinatariosAlertaPotencialmenteAtingidos(int regiaoAlerta)
        {
            return (await (_unitOfWork.Repository<DestinatarioAlerta>().FindAllAsync(x => x.RegiaoAlertaId == regiaoAlerta))).Count();
        }

        public async Task<List<int>> GetRegioesAtingidasAlertasNaUltimaHora()
        {
            var listaDeRegioes = new List<int>();
            var baseUrlApiMonitoramentoBarragens = _configuration.GetUrlBaseModuloMonitoramentoBarragens();
            var accessToken = _autenticacaoService.ObtemAccessTokenAtual();

            using (var client = new AspNetCoreWebApiClient(baseUrlApiMonitoramentoBarragens, accessToken))
            {
                listaDeRegioes = await client.Get<List<int>>("Alerta/GetIdsRegioesDeAlertaAtingidas");
            }

            return listaDeRegioes;
        }

        public async Task<int> EnviarMensagensParaDestinatariosPotencialmenteAtingidos()
        {
            var regioes = await GetRegioesAtingidasAlertasNaUltimaHora();
            var contadorEnvioMensagens = 0;
            foreach (var regiao in regioes)
            {
                contadorEnvioMensagens += await (EnviarMensagens(regiao));
            }

            return contadorEnvioMensagens;
        }

        private async Task<int> EnviarMensagens(int regiaoAlerta)
        {
            var listaDestinatarios = (await (_unitOfWork.Repository<DestinatarioAlerta>().FindAllAsyncIncluding(x => x.RegiaoAlertaId == regiaoAlerta, x => x.Include(y => y.RegiaoAlerta)))).ToList();

            //recupera lista de mensagens enviadas na última hora
            var listaMensagensEnviadasUltimaHora = await RecuperarMensagensEnviadas(60);
            foreach (var destinatario in listaDestinatarios)
            {
                var mensagem = $"MiningCorp informa: Atenção morador da localidade de {destinatario.RegiaoAlerta.Descricao}, existe alto risco de rompimento de barragem em sua região, e todos os moradores devem evacuar a área imediatamente!";

                if (!string.IsNullOrEmpty(destinatario.TelefoneCelularSMS))
                {
                    var canalComunicacao = (int)CanalComunicacao.SMS;
                    if (!listaMensagensEnviadasUltimaHora.Any(x => x.DestinatarioAlertaId == destinatario.DestinatarioAlertaId
                            && x.CanalComunicacaoId == canalComunicacao))
                    {
                        await _unitOfWork.Repository<MensagemEnviada>().AddAsync(new MensagemEnviada()
                        {
                            DestinatarioAlertaId = destinatario.DestinatarioAlertaId,
                            CanalComunicacaoId = canalComunicacao,
                            ConteudoMensagem = mensagem,
                            DataHoraEnvio = DateTime.Now
                        });
                    }


                }

                if (!string.IsNullOrEmpty(destinatario.TelefoneCelularWhatsApp))
                {
                    var canalComunicacao = (int)CanalComunicacao.WHATSAPP;
                    if (!listaMensagensEnviadasUltimaHora.Any(x => x.DestinatarioAlertaId == destinatario.DestinatarioAlertaId
                            && x.CanalComunicacaoId == canalComunicacao))
                    {
                        await _unitOfWork.Repository<MensagemEnviada>().AddAsync(new MensagemEnviada()
                        {
                            DestinatarioAlertaId = destinatario.DestinatarioAlertaId,
                            CanalComunicacaoId = canalComunicacao,
                            ConteudoMensagem = mensagem,
                            DataHoraEnvio = DateTime.Now
                        });
                    }
                }

                if (!string.IsNullOrEmpty(destinatario.EnderecoEmail))
                {
                    var canalComunicacao = (int)CanalComunicacao.EMAIL;
                    if (!listaMensagensEnviadasUltimaHora.Any(x => x.DestinatarioAlertaId == destinatario.DestinatarioAlertaId
                            && x.CanalComunicacaoId == canalComunicacao))
                    {
                        await _unitOfWork.Repository<MensagemEnviada>().AddAsync(new MensagemEnviada()
                        {
                            DestinatarioAlertaId = destinatario.DestinatarioAlertaId,
                            CanalComunicacaoId = canalComunicacao,
                            ConteudoMensagem = mensagem,
                            DataHoraEnvio = DateTime.Now
                        });
                    }
                }

            }

            return await _unitOfWork.Commit();
        }

        public async Task<List<MensagemEnviada>> RecuperarMensagensEnviadas(int quantidadeMinutosPassados)
        {
            return (await _unitOfWork.Repository<MensagemEnviada>().FindAllAsyncIncluding(
                                x => x.DataHoraEnvio < DateTime.Now.AddMinutes(-quantidadeMinutosPassados),
                                x => x.Include(y => y.CanalComunicacao).Include(y => y.DestinatarioAlerta))).OrderBy(x => x.DataHoraEnvio).ToList();

        }

    }

}
