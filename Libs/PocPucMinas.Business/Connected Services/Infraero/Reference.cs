﻿//------------------------------------------------------------------------------
// <gerado automaticamente>
//     Esse código foi gerado por uma ferramenta.
//     //
//     As alterações no arquivo poderão causar comportamento incorreto e serão perdidas se
//     o código for gerado novamente.
// </gerado automaticamente>
//------------------------------------------------------------------------------

namespace Infraero
{
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="Infraero.IConsultaVoos")]
    public interface IConsultaVoos
    {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ConsultarVoosSentido", ReplyAction="http://tempuri.org/IConsultaVoos/ConsultarVoosSentidoResponse")]
        System.Threading.Tasks.Task<string> ConsultarVoosSentidoAsync(string icao, string idioma, bool partida, bool exibirFinalizados, int registrosPagina, int pagina);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ConsultarVoosCiaAerea", ReplyAction="http://tempuri.org/IConsultaVoos/ConsultarVoosCiaAereaResponse")]
        System.Threading.Tasks.Task<string> ConsultarVoosCiaAereaAsync(string icao, string idioma, bool partida, string siglaCiaAerea, bool exibirFinalizados, int registrosPagina, int pagina);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ConsultarVoosNumero", ReplyAction="http://tempuri.org/IConsultaVoos/ConsultarVoosNumeroResponse")]
        System.Threading.Tasks.Task<string> ConsultarVoosNumeroAsync(string icao, string idioma, bool partida, string numeroVoo, bool exibirFinalizados, int registrosPagina, int pagina);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ListarAeroportos", ReplyAction="http://tempuri.org/IConsultaVoos/ListarAeroportosResponse")]
        System.Threading.Tasks.Task<string> ListarAeroportosAsync(string idioma);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ListarCompanhias", ReplyAction="http://tempuri.org/IConsultaVoos/ListarCompanhiasResponse")]
        System.Threading.Tasks.Task<string> ListarCompanhiasAsync(string icao, string idioma);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    public interface IConsultaVoosChannel : Infraero.IConsultaVoos, System.ServiceModel.IClientChannel
    {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("dotnet-svcutil", "1.0.0.1")]
    public partial class ConsultaVoosClient : System.ServiceModel.ClientBase<Infraero.IConsultaVoos>, Infraero.IConsultaVoos
    {
        
    /// <summary>
    /// Implemente este método parcial para configurar o ponto de extremidade de serviço.
    /// </summary>
    /// <param name="serviceEndpoint">O ponto de extremidade a ser configurado</param>
    /// <param name="clientCredentials">As credenciais do cliente</param>
    static partial void ConfigureEndpoint(System.ServiceModel.Description.ServiceEndpoint serviceEndpoint, System.ServiceModel.Description.ClientCredentials clientCredentials);
        
        public ConsultaVoosClient() : 
                base(ConsultaVoosClient.GetDefaultBinding(), ConsultaVoosClient.GetDefaultEndpointAddress())
        {
            this.Endpoint.Name = EndpointConfiguration.Srv.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public ConsultaVoosClient(EndpointConfiguration endpointConfiguration) : 
                base(ConsultaVoosClient.GetBindingForEndpoint(endpointConfiguration), ConsultaVoosClient.GetEndpointAddress(endpointConfiguration))
        {
            this.Endpoint.Name = endpointConfiguration.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public ConsultaVoosClient(EndpointConfiguration endpointConfiguration, string remoteAddress) : 
                base(ConsultaVoosClient.GetBindingForEndpoint(endpointConfiguration), new System.ServiceModel.EndpointAddress(remoteAddress))
        {
            this.Endpoint.Name = endpointConfiguration.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public ConsultaVoosClient(EndpointConfiguration endpointConfiguration, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(ConsultaVoosClient.GetBindingForEndpoint(endpointConfiguration), remoteAddress)
        {
            this.Endpoint.Name = endpointConfiguration.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public ConsultaVoosClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress)
        {
        }
        
        public System.Threading.Tasks.Task<string> ConsultarVoosSentidoAsync(string icao, string idioma, bool partida, bool exibirFinalizados, int registrosPagina, int pagina)
        {
            return base.Channel.ConsultarVoosSentidoAsync(icao, idioma, partida, exibirFinalizados, registrosPagina, pagina);
        }
        
        public System.Threading.Tasks.Task<string> ConsultarVoosCiaAereaAsync(string icao, string idioma, bool partida, string siglaCiaAerea, bool exibirFinalizados, int registrosPagina, int pagina)
        {
            return base.Channel.ConsultarVoosCiaAereaAsync(icao, idioma, partida, siglaCiaAerea, exibirFinalizados, registrosPagina, pagina);
        }
        
        public System.Threading.Tasks.Task<string> ConsultarVoosNumeroAsync(string icao, string idioma, bool partida, string numeroVoo, bool exibirFinalizados, int registrosPagina, int pagina)
        {
            return base.Channel.ConsultarVoosNumeroAsync(icao, idioma, partida, numeroVoo, exibirFinalizados, registrosPagina, pagina);
        }
        
        public System.Threading.Tasks.Task<string> ListarAeroportosAsync(string idioma)
        {
            return base.Channel.ListarAeroportosAsync(idioma);
        }
        
        public System.Threading.Tasks.Task<string> ListarCompanhiasAsync(string icao, string idioma)
        {
            return base.Channel.ListarCompanhiasAsync(icao, idioma);
        }
        
        public virtual System.Threading.Tasks.Task OpenAsync()
        {
            return System.Threading.Tasks.Task.Factory.FromAsync(((System.ServiceModel.ICommunicationObject)(this)).BeginOpen(null, null), new System.Action<System.IAsyncResult>(((System.ServiceModel.ICommunicationObject)(this)).EndOpen));
        }
        
        public virtual System.Threading.Tasks.Task CloseAsync()
        {
            return System.Threading.Tasks.Task.Factory.FromAsync(((System.ServiceModel.ICommunicationObject)(this)).BeginClose(null, null), new System.Action<System.IAsyncResult>(((System.ServiceModel.ICommunicationObject)(this)).EndClose));
        }
        
        private static System.ServiceModel.Channels.Binding GetBindingForEndpoint(EndpointConfiguration endpointConfiguration)
        {
            if ((endpointConfiguration == EndpointConfiguration.Srv))
            {
                System.ServiceModel.BasicHttpBinding result = new System.ServiceModel.BasicHttpBinding();
                result.MaxBufferSize = int.MaxValue;
                result.ReaderQuotas = System.Xml.XmlDictionaryReaderQuotas.Max;
                result.MaxReceivedMessageSize = int.MaxValue;
                result.AllowCookies = true;
                return result;
            }
            throw new System.InvalidOperationException(string.Format("Não foi possível encontrar o ponto de extremidade com o nome \'{0}\'.", endpointConfiguration));
        }
        
        private static System.ServiceModel.EndpointAddress GetEndpointAddress(EndpointConfiguration endpointConfiguration)
        {
            if ((endpointConfiguration == EndpointConfiguration.Srv))
            {
                return new System.ServiceModel.EndpointAddress("http://voos.infraero.gov.br/wsvoosmobile/ConsultaVoos.svc");
            }
            throw new System.InvalidOperationException(string.Format("Não foi possível encontrar o ponto de extremidade com o nome \'{0}\'.", endpointConfiguration));
        }
        
        private static System.ServiceModel.Channels.Binding GetDefaultBinding()
        {
            return ConsultaVoosClient.GetBindingForEndpoint(EndpointConfiguration.Srv);
        }
        
        private static System.ServiceModel.EndpointAddress GetDefaultEndpointAddress()
        {
            return ConsultaVoosClient.GetEndpointAddress(EndpointConfiguration.Srv);
        }
        
        public enum EndpointConfiguration
        {
            
            Srv,
        }
    }
}
