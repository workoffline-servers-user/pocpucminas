﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace PocPucMinas.Business.Extensions
{
    public static class IConfigurationExtensions
    {
        public static string GetUrlBaseModuloMonitoramentoBarragens(this IConfiguration configuration)
        {
            return configuration.GetValue<string>("UrlBaseModuloMonitoramentoBarragens");
        }

    }
}
