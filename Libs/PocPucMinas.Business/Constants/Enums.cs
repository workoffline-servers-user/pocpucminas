﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PocPucMinas.Business.Constants
{    public enum CanalComunicacao : int
    {
        EMAIL = 1,
        SMS = 2,
        WHATSAPP = 3
    }
}
