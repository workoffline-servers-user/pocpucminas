﻿using PocPucMinas.AspNetCore.Security.Services.Classes;
using PocPucMinas.AspNetCore.Security.DTO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using PocPucMinas.AspNetCore.Security.Services.Interfaces;

namespace PocPucMinas.AspNetCore.Security.Attributes
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class AutorizacaoApiAttribute : Attribute, IAuthorizationFilter
    {
        private readonly string[] _listaDeClientesAutorizados;
        
        public AutorizacaoApiAttribute(string[] listaDeClientesAutorizados = default)
        {
            _listaDeClientesAutorizados = listaDeClientesAutorizados;
        }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            //já recupera o token a partir do Header "Authorization" internamente no manipulador de Token
            //var authorizationHeader = context.HttpContext.Request.Headers["Authorization"];            
            var servicoAutenticacao = (IAutenticacaoService) context.HttpContext.RequestServices.GetService(typeof(IAutenticacaoService));

            //Verifica se o usuário está autenticado
            var jwtPayload = servicoAutenticacao.GetPayloadDoAccessTokenAtual();
            if (jwtPayload == default)
            {
                //acesso negado
                context.Result = new StatusCodeResult((int)HttpStatusCode.Forbidden);
                return;
            }

            var identificadorClienteApi = jwtPayload["IdentificadorClienteApi"].ToString();
            if(identificadorClienteApi == default)
            {
                //acesso negado
                context.Result = new StatusCodeResult((int)HttpStatusCode.Forbidden);
                return;
            }

            if (_listaDeClientesAutorizados != default(string[]) && _listaDeClientesAutorizados.Length > 0)
            {
                if((string) jwtPayload["TipoUsuario"] == "")
                {                    
                    if (!_listaDeClientesAutorizados.Contains(identificadorClienteApi))
                    {
                        context.Result = new StatusCodeResult((int)HttpStatusCode.Forbidden);
                    }
                    else
                    {
                        //acesso autorizado
                        return;
                    }
                }
                else
                {
                    //acesso negado
                    context.Result = new StatusCodeResult((int)HttpStatusCode.Forbidden);
                    return;
                }
            }
            else
            {
                //acesso autorizado
                return;
            }
            
        }

    }

}
