using PocPucMinas.DataLib.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using PocPucMinas.DataDriver.Context;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using System.Linq;
using PocPucMinas.CommonLib.Generators;

namespace PocPucMinas.DataLib.Context
{
    public class PocPucMinasDbContextInitializer : IDefaultDbContextInitializer
    {
        private readonly PocPucMinasDbContext _context;

        public PocPucMinasDbContextInitializer(PocPucMinasDbContext context)
        {
            _context = context;
        }

        public DbContext GetContext()
        {
            return _context;
        }

        public bool EnsureCreated()
        {
            return _context.Database.EnsureCreated();
        }

        public void Migrate()
        {
            _context.Database.Migrate();
        }

        public async Task Seed()
        {
            await CriarUsuarios();

            if (await _context.RegioesAlerta.CountAsync() == 0)
            {
                await CriarRegioesAlerta();
                _context.SaveChanges();
            }

            if (await _context.DestinatariosAlerta.CountAsync() == 0)
            {
                await CriarDestinatariosAlertas();
                _context.SaveChanges();
            }

            if (await _context.CanaisComunicacaoMensagem.CountAsync() == 0)
            {
                await CriarCanaisDeComunicacao();
                _context.SaveChanges();
            }

            if (await _context.TiposSensores.CountAsync() == 0)
            {
                await CriarTiposDeSensores();
                _context.SaveChanges();
            }

            if (await _context.Sensores.CountAsync() == 0)
            {
                await CriarSensores();
                _context.SaveChanges();
            }
            
            if (await _context.NiveisRisco.CountAsync() == 0)
            {
                await CriarNiveisDeRisco();
                _context.SaveChanges();
            }

            if (await _context.ParametrosSensor.CountAsync() == 0)
            {
                await CriarParametros();
                _context.SaveChanges();
            }

        }

        private async Task CriarUsuarios()
        {
            var token1 = "45f6024b-b563-403d-a4a5-c1a1de91b772";
            if (! await _context.UsuariosApi.AnyAsync(x => x.TokenDeAcesso == token1))
            {
                var user1 = new UsuarioApi
                {
                    TokenDeAcesso = token1,
                    SenhaDeAcesso = "123456",
                    DataCadastro = DateTime.Now,
                    Ativo = true,
                    Nome = "Eduardo Caetano de Oliveira Alves"
                };

                await _context.UsuariosApi.AddAsync(user1);
            }

            var token2 = "7ed24851-487d-43b2-9e06-5fa283001529";
            if (!await _context.UsuariosApi.AnyAsync(x => x.TokenDeAcesso == token2))
            {
                var user2 = new UsuarioApi
                {
                    TokenDeAcesso = token2,
                    SenhaDeAcesso = "654321",
                    DataCadastro = DateTime.Now,
                    Ativo = true,
                    Nome = "Adriana Caetano de Oliveira Alves"
                };

                await _context.UsuariosApi.AddAsync(user2);
            }

            _context.SaveChanges();

        }
        private async Task CriarCanaisDeComunicacao()
        {
            var listaDeCanaisDeComunicacao = new List<CanalComunicacaoMensagem>
            {
                new CanalComunicacaoMensagem
                {
                    NomeCanalComunicacao = "E-mail",
                    Ativo = true
                },
                new CanalComunicacaoMensagem
                {
                    NomeCanalComunicacao = "SMS",
                    Ativo = true
                },
                new CanalComunicacaoMensagem
                {
                    NomeCanalComunicacao = "WhatsApp",
                    Ativo = true
                }
            };

            await _context.CanaisComunicacaoMensagem.AddRangeAsync(listaDeCanaisDeComunicacao);
        }

        private async Task CriarRegioesAlerta()
        {
            var lista = new List<RegiaoAlerta>
            {
                new RegiaoAlerta
                {
                    Descricao = "S�o Jo�o Del Rey",
                    Latitude = GetRandomNumberInRange(-19.617850, -19.597151),
                    Longitude = GetRandomNumberInRange(-44.819806, -44.519806),
                    RaioAbrangenciaEmMetros = 5000,
                    Ativo = true
                },
                new RegiaoAlerta
                {
                    Descricao = "Congonhas",
                    Latitude = GetRandomNumberInRange(-19.617850, -19.597151),
                    Longitude = GetRandomNumberInRange(-44.819806, -44.519806),
                    RaioAbrangenciaEmMetros = 7000,
                    Ativo = true
                },
                new RegiaoAlerta
                {
                    Descricao = "Conselheiro Lafaiete",
                    Latitude = GetRandomNumberInRange(-19.617850, -19.597151),
                    Longitude = GetRandomNumberInRange(-44.819806, -44.519806),
                    RaioAbrangenciaEmMetros = 10000,
                    Ativo = true
                },
                new RegiaoAlerta
                {
                    Descricao = "Ouro Preto",
                    Latitude = GetRandomNumberInRange(-19.617850, -19.597151),
                    Longitude = GetRandomNumberInRange(-44.819806, -44.519806),
                    RaioAbrangenciaEmMetros = 20000,
                    Ativo = true
                },
            };

            await _context.RegioesAlerta.AddRangeAsync(lista);
        }

        private async Task CriarDestinatariosAlertas()
        {
            var listaRegioes = await _context.RegioesAlerta.ToListAsync();

            foreach(var regiaoAlerta in listaRegioes)
            {
                for (var i = 0; i < 50; i++)
                {
                    var rand = new Random();                    
                    var destinatario = new DestinatarioAlerta
                    {
                        RegiaoAlertaId = regiaoAlerta.RegiaoAlertaId,
                        EnderecoEmail = rand.Next(1, 10) > 5 ? TestDataGenerator.Email() : null,
                        TelefoneCelularSMS = rand.Next(1, 10) > 5 ? TestDataGenerator.Celular() : null,
                        TelefoneCelularWhatsApp = rand.Next(1, 10) > 5 ? TestDataGenerator.Celular() : null,
                        Ativo = true,
                    };

                    await _context.DestinatariosAlerta.AddAsync(destinatario);
                }

            }

        }        

        private async Task CriarTiposDeSensores()
        {
            var listaDeTiposDeSensores = new List<TipoSensor>
            {
                new TipoSensor
                {
                    Descricao = "Trepida��o"
                },
                new TipoSensor
                {
                    Descricao = "Press�o"
                },
                new TipoSensor
                {
                    Descricao = "Temperatura"
                }
            };

            await _context.TiposSensores.AddRangeAsync(listaDeTiposDeSensores);
        }

        private async Task CriarSensores()
        {
            var listaDeTiposDeSensores = await _context.TiposSensores.ToListAsync();
            var listaDeRegioes = await _context.RegioesAlerta.ToListAsync();
            var listaDeSensores = new List<Sensor>();

            foreach(var regiaoAlerta in listaDeRegioes)
            {
                for (var i = 0; i < 20; i++)
                {
                    var rand = new Random();
                    var tipoRand = listaDeTiposDeSensores[rand.Next(0, listaDeTiposDeSensores.Count - 1)];

                    var latitude = GetRandomNumberInRange(-19.617850, -19.597151);
                    var longitude = GetRandomNumberInRange(-44.819806, -44.519806);

                    var sensor = new Sensor
                    {
                        TipoSensorId = tipoRand.TipoSensorId,
                        RegiaoAlertaId = regiaoAlerta.RegiaoAlertaId,
                        NumeroDeSerie = new Guid().ToString(),
                        Latitude = latitude,
                        Longitude = longitude
                    };

                    listaDeSensores.Add(sensor);
                }
            }

            

            await _context.Sensores.AddRangeAsync(listaDeSensores);
        }

        private async Task CriarNiveisDeRisco()
        {
            var lista = new List<NivelRisco>
            {
                new NivelRisco
                {
                    Descricao = "Baixo"
                },
                new NivelRisco
                {
                    Descricao = "M�dio"
                },
                new NivelRisco
                {
                    Descricao = "Alto"
                }
            };

            await _context.NiveisRisco.AddRangeAsync(lista);
        }

        private async Task CriarParametros()
        {
            var lista = new List<ParametroSensor>();
            var listaDeTiposDeSensores = await _context.TiposSensores.ToListAsync();
            var listaNiveisDeRisco = await _context.NiveisRisco.ToListAsync();

            foreach(var tipoSensor in listaDeTiposDeSensores)
            {
                foreach(var nivelDeRisco in listaNiveisDeRisco)
                {
                    var parametro = new ParametroSensor
                    {
                        TipoSensorId = tipoSensor.TipoSensorId,
                        NivelRiscoId = nivelDeRisco.NivelRiscoId,
                        AnormalAbaixoDe = -10 * nivelDeRisco.NivelRiscoId,
                        AnormalAcimaDe = 10 * nivelDeRisco.NivelRiscoId
                    };

                    lista.Add(parametro);
                }
            }
            
            await _context.ParametrosSensor.AddRangeAsync(lista);
        }

        public decimal GetRandomNumberInRange(double minNumber, double maxNumber)
        {
            return Convert.ToDecimal(new Random().NextDouble() * (maxNumber - minNumber) + minNumber);
        }

    }

}
