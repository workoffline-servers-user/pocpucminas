﻿using PocPucMinas.DataLib.Models;
using Microsoft.EntityFrameworkCore;
using PocPucMinas.CommonLib.DataContext;

namespace PocPucMinas.DataLib.Context
{
    public class PocPucMinasDbContext : DataContext
    {
        public PocPucMinasDbContext(DbContextOptions<PocPucMinasDbContext> options) : base(options)
        {

        }

        public DbSet<CanalComunicacaoMensagem> CanaisComunicacaoMensagem { get; set; }

        public DbSet<DestinatarioAlerta> DestinatariosAlerta { get; set; }

        public DbSet<MedicaoSensor> MedicoesSensores { get; set; }

        public DbSet<MensagemEnviada> MensagensEnviadas { get; set; }

        public DbSet<RegiaoAlerta> RegioesAlerta { get; set; }

        public DbSet<Sensor> Sensores { get; set; }

        public DbSet<TipoSensor> TiposSensores { get; set; }

        public DbSet<UsuarioApi> UsuariosApi { get; set; }

        public DbSet<ParametroSensor> ParametrosSensor { get; set; }

        public DbSet<NivelRisco> NiveisRisco { get; set; }


        protected override void AddIndexes(ModelBuilder modelBuilder)
        {
            //Destinatários
            var entidadeAeroportos = modelBuilder.Entity<DestinatarioAlerta>();

            entidadeAeroportos
                .HasIndex(p => p.RegiaoAlertaId);


            //Medições
            var medicoesSensores = modelBuilder.Entity<MedicaoSensor>();

            medicoesSensores
                .HasIndex(p => p.SensorId);


            //Mensagens Enviadas
            var mensagensEnviadas = modelBuilder.Entity<MensagemEnviada>();

            mensagensEnviadas
                .HasIndex(p => p.DestinatarioAlertaId);


            //Sensores
            var sensores = modelBuilder.Entity<Sensor>();

            sensores
                .HasIndex(p => p.TipoSensorId);


            //Usuários
            var usuarios = modelBuilder.Entity<UsuarioApi>();

            usuarios
                .HasIndex(p => p.TokenDeAcesso)
                .IsUnique();

            //Parametros
            var parametrosSensor = modelBuilder.Entity<ParametroSensor>();

            parametrosSensor
                .HasIndex(p => new { p.NivelRiscoId, p.TipoSensorId })
                .IsUnique();

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);            
        }

    }

}
