using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using PocPucMinas.CommonLib.Services.Implementations;
using System.IO;

namespace PocPucMinas.DataLib.Context
{
    public class PocPucMinasContextFactory : IDesignTimeDbContextFactory<PocPucMinasDbContext>
    {
        public PocPucMinasDbContext CreateDbContext(string[] args)
        {
            var appSettingsLoader = new AppSettingsLoaderService();
            var config = appSettingsLoader.GetConfiguration();
            
            var optionsBuilder = new DbContextOptionsBuilder<PocPucMinasDbContext>();
            optionsBuilder.UseNpgsql(config.GetConnectionString("PostgreSQL"));

            return new PocPucMinasDbContext(optionsBuilder.Options);
        }
    }
}
