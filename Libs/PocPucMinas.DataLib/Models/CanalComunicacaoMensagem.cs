using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PocPucMinas.DataLib.Models
{
    public class CanalComunicacaoMensagem
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CanalComunicacaoMensagemId { get; set; }

        public string NomeCanalComunicacao { get; set; }

        public bool Ativo { get; set; }
                        
    }
}
