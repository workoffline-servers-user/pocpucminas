using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PocPucMinas.DataLib.Models
{
    public class MedicaoSensor
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long MedicaoSensorId { get; set; }

        [Required]
        public int SensorId { get; set; }

        [ForeignKey("SensorId")]
        public virtual Sensor Sensor { get; set; }

        [Required]
        public int RegiaoAlertaId { get; set; }

        [ForeignKey("RegiaoAlertaId")]
        public virtual RegiaoAlerta RegiaoAlerta { get; set; }

        [Required]
        public decimal ValorMedido { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime DataHoraMedicao { get; set; }

    }
}
