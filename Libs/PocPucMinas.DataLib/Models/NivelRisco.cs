using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PocPucMinas.DataLib.Models
{
    public class NivelRisco
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int NivelRiscoId { get; set; }

        public string Descricao { get; set; }
                
    }
}
