using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PocPucMinas.DataLib.Models
{
    public class ParametroSensor
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ParametroSensorId { get; set; }

        public decimal AnormalAbaixoDe { get; set; }

        public decimal AnormalAcimaDe { get; set; }
        
        [Required]
        public int NivelRiscoId { get; set; }

        [ForeignKey("NivelRiscoId")]
        public virtual NivelRisco NivelRisco { get; set; }

        [Required]
        public int TipoSensorId { get; set; }

        [ForeignKey("TipoSensorId")]
        public virtual TipoSensor TipoSensor { get; set; }
                
    }
}
