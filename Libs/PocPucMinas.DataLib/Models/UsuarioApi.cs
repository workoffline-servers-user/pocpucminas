using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PocPucMinas.DataLib.Models
{
    public class UsuarioApi
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UsuarioApiId { get; set; }

        [Required]
        [StringLength(150, MinimumLength = 3)]
        public string Nome { get; set; }

        [StringLength(150, MinimumLength = 10)]  
        public string TokenDeAcesso { get; set; }

        [StringLength(150, MinimumLength = 10)]
        public string SenhaDeAcesso { get; set; }

        [Required]
        public bool Ativo { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime DataCadastro { get; set; } 



        
    }
}
