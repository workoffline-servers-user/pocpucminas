using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PocPucMinas.DataLib.Models
{
    public class DestinatarioAlerta
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long DestinatarioAlertaId { get; set; }

        [StringLength(150, MinimumLength = 3)]
        [DataType(DataType.EmailAddress)]
        public string EnderecoEmail { get; set; }

        [StringLength(15, MinimumLength = 10)]
        public string TelefoneCelularSMS { get; set; }

        [StringLength(15, MinimumLength = 10)]
        public string TelefoneCelularWhatsApp { get; set; }

        public int RegiaoAlertaId { get; set; }

        [ForeignKey("RegiaoAlertaId")]
        public virtual RegiaoAlerta RegiaoAlerta { get; set; }

        public bool Ativo { get; set; }        
    }
}
