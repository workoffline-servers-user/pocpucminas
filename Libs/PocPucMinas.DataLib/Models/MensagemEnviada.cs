using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PocPucMinas.DataLib.Models
{
    public class MensagemEnviada
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MensagemEnviadaId { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime DataHoraEnvio { get; set; }

        [StringLength(4000, MinimumLength = 3)]
        [DataType(DataType.Text)]
        public string ConteudoMensagem { get; set; }

        [Required]
        public int CanalComunicacaoId { get; set; }

        [ForeignKey("CanalComunicacaoId")]
        public virtual CanalComunicacaoMensagem CanalComunicacao { get; set; }

        [Required]
        public long DestinatarioAlertaId { get; set; }

        [ForeignKey("DestinatarioAlertaId")]
        public virtual DestinatarioAlerta DestinatarioAlerta { get; set; }
                
    }
}
