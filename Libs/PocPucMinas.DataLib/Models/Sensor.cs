using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PocPucMinas.DataLib.Models
{
    public class Sensor
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SensorId { get; set; }

        [Required]
        public string NumeroDeSerie { get; set; }

        [Required]
        public decimal Latitude { get; set; }

        [Required]
        public decimal Longitude { get; set; }

        [Required]
        public int RegiaoAlertaId { get; set; }

        [ForeignKey("RegiaoAlertaId")]
        public virtual RegiaoAlerta RegiaoAlerta { get; set; }

        [Required]
        public int TipoSensorId { get; set; }

        [ForeignKey("TipoSensorId")]
        public virtual TipoSensor TipoSensor { get; set; }
                
    }
}
