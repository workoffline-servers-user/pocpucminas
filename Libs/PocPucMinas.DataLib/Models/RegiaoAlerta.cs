using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PocPucMinas.DataLib.Models
{
    public class RegiaoAlerta
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int RegiaoAlertaId { get; set; }

        public string Descricao { get; set; }

        public bool Ativo { get; set; }

        [Required]
        public decimal Latitude { get; set; }

        [Required]
        public decimal Longitude { get; set; }

        [Required]
        public int RaioAbrangenciaEmMetros { get; set; }
    }
}
