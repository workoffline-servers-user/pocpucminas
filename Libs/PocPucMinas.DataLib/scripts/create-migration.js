const timestamp = Math.floor(new Date().getTime()/1000).toString();
const execSync = require("child_process").execSync;
const pathStartupProject = '../../Apps/PocPucMinas/PocPucMinas.WebApi';
execSync(`dotnet ef migrations add ${timestamp} -s ${pathStartupProject}`, { stdio: 'inherit' });
execSync(`dotnet ef database update -s ${pathStartupProject}`, { stdio: 'inherit' });