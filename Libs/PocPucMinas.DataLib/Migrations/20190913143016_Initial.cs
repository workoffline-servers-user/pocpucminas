﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace PocPucMinas.DataLib.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "canais_comunicacao_mensagem",
                columns: table => new
                {
                    canal_comunicacao_mensagem_id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    nome_canal_comunicacao = table.Column<string>(nullable: true),
                    ativo = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_canais_comunicacao_mensagem", x => x.canal_comunicacao_mensagem_id);
                });

            migrationBuilder.CreateTable(
                name: "niveis_risco",
                columns: table => new
                {
                    nivel_risco_id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    descricao = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_niveis_risco", x => x.nivel_risco_id);
                });

            migrationBuilder.CreateTable(
                name: "regioes_alerta",
                columns: table => new
                {
                    regiao_alerta_id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    descricao = table.Column<string>(nullable: true),
                    ativo = table.Column<bool>(nullable: false),
                    latitude = table.Column<decimal>(nullable: false),
                    longitude = table.Column<decimal>(nullable: false),
                    raio_abrangencia_em_metros = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_regioes_alerta", x => x.regiao_alerta_id);
                });

            migrationBuilder.CreateTable(
                name: "tipos_sensores",
                columns: table => new
                {
                    tipo_sensor_id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    descricao = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_tipos_sensores", x => x.tipo_sensor_id);
                });

            migrationBuilder.CreateTable(
                name: "usuarios_api",
                columns: table => new
                {
                    usuario_api_id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    nome = table.Column<string>(maxLength: 150, nullable: false),
                    token_de_acesso = table.Column<string>(maxLength: 150, nullable: true),
                    senha_de_acesso = table.Column<string>(maxLength: 150, nullable: true),
                    ativo = table.Column<bool>(nullable: false),
                    data_cadastro = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_usuarios_api", x => x.usuario_api_id);
                });

            migrationBuilder.CreateTable(
                name: "destinatarios_alerta",
                columns: table => new
                {
                    destinatario_alerta_id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    endereco_email = table.Column<string>(maxLength: 150, nullable: true),
                    telefone_celular_sms = table.Column<string>(maxLength: 15, nullable: true),
                    telefone_celular_whats_app = table.Column<string>(maxLength: 15, nullable: true),
                    regiao_alerta_id = table.Column<int>(nullable: false),
                    ativo = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_destinatarios_alerta", x => x.destinatario_alerta_id);
                    table.ForeignKey(
                        name: "fk_destinatarios_alerta_regioes_alerta_regiao_alerta_id",
                        column: x => x.regiao_alerta_id,
                        principalTable: "regioes_alerta",
                        principalColumn: "regiao_alerta_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "parametros_sensor",
                columns: table => new
                {
                    parametro_sensor_id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    anormal_abaixo_de = table.Column<decimal>(nullable: false),
                    anormal_acima_de = table.Column<decimal>(nullable: false),
                    nivel_risco_id = table.Column<int>(nullable: false),
                    tipo_sensor_id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_parametros_sensor", x => x.parametro_sensor_id);
                    table.ForeignKey(
                        name: "fk_parametros_sensor_niveis_risco_nivel_risco_id",
                        column: x => x.nivel_risco_id,
                        principalTable: "niveis_risco",
                        principalColumn: "nivel_risco_id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "fk_parametros_sensor_tipos_sensores_tipo_sensor_id",
                        column: x => x.tipo_sensor_id,
                        principalTable: "tipos_sensores",
                        principalColumn: "tipo_sensor_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "sensores",
                columns: table => new
                {
                    sensor_id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    numero_de_serie = table.Column<string>(nullable: false),
                    latitude = table.Column<decimal>(nullable: false),
                    longitude = table.Column<decimal>(nullable: false),
                    regiao_alerta_id = table.Column<int>(nullable: false),
                    tipo_sensor_id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_sensores", x => x.sensor_id);
                    table.ForeignKey(
                        name: "fk_sensores_regioes_alerta_regiao_alerta_id",
                        column: x => x.regiao_alerta_id,
                        principalTable: "regioes_alerta",
                        principalColumn: "regiao_alerta_id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "fk_sensores_tipos_sensores_tipo_sensor_id",
                        column: x => x.tipo_sensor_id,
                        principalTable: "tipos_sensores",
                        principalColumn: "tipo_sensor_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "mensagens_enviadas",
                columns: table => new
                {
                    mensagem_enviada_id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    data_hora_envio = table.Column<DateTime>(nullable: false),
                    conteudo_mensagem = table.Column<string>(maxLength: 4000, nullable: true),
                    canal_comunicacao_id = table.Column<int>(nullable: false),
                    destinatario_alerta_id = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_mensagens_enviadas", x => x.mensagem_enviada_id);
                    table.ForeignKey(
                        name: "fk_mensagens_enviadas_canais_comunicacao_mensagem_canal_comuni~",
                        column: x => x.canal_comunicacao_id,
                        principalTable: "canais_comunicacao_mensagem",
                        principalColumn: "canal_comunicacao_mensagem_id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "fk_mensagens_enviadas_destinatarios_alerta_destinatario_alerta~",
                        column: x => x.destinatario_alerta_id,
                        principalTable: "destinatarios_alerta",
                        principalColumn: "destinatario_alerta_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "medicoes_sensores",
                columns: table => new
                {
                    medicao_sensor_id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    sensor_id = table.Column<int>(nullable: false),
                    regiao_alerta_id = table.Column<int>(nullable: false),
                    valor_medido = table.Column<decimal>(nullable: false),
                    data_hora_medicao = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_medicoes_sensores", x => x.medicao_sensor_id);
                    table.ForeignKey(
                        name: "fk_medicoes_sensores_regioes_alerta_regiao_alerta_id",
                        column: x => x.regiao_alerta_id,
                        principalTable: "regioes_alerta",
                        principalColumn: "regiao_alerta_id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "fk_medicoes_sensores_sensores_sensor_id",
                        column: x => x.sensor_id,
                        principalTable: "sensores",
                        principalColumn: "sensor_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "ix_destinatarios_alerta_regiao_alerta_id",
                table: "destinatarios_alerta",
                column: "regiao_alerta_id");

            migrationBuilder.CreateIndex(
                name: "ix_medicoes_sensores_regiao_alerta_id",
                table: "medicoes_sensores",
                column: "regiao_alerta_id");

            migrationBuilder.CreateIndex(
                name: "ix_medicoes_sensores_sensor_id",
                table: "medicoes_sensores",
                column: "sensor_id");

            migrationBuilder.CreateIndex(
                name: "ix_mensagens_enviadas_canal_comunicacao_id",
                table: "mensagens_enviadas",
                column: "canal_comunicacao_id");

            migrationBuilder.CreateIndex(
                name: "ix_mensagens_enviadas_destinatario_alerta_id",
                table: "mensagens_enviadas",
                column: "destinatario_alerta_id");

            migrationBuilder.CreateIndex(
                name: "ix_parametros_sensor_tipo_sensor_id",
                table: "parametros_sensor",
                column: "tipo_sensor_id");

            migrationBuilder.CreateIndex(
                name: "IX_parametros_sensor_nivel_risco_id_tipo_sensor_id",
                table: "parametros_sensor",
                columns: new[] { "nivel_risco_id", "tipo_sensor_id" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_sensores_regiao_alerta_id",
                table: "sensores",
                column: "regiao_alerta_id");

            migrationBuilder.CreateIndex(
                name: "ix_sensores_tipo_sensor_id",
                table: "sensores",
                column: "tipo_sensor_id");

            migrationBuilder.CreateIndex(
                name: "IX_usuarios_api_token_de_acesso",
                table: "usuarios_api",
                column: "token_de_acesso",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "medicoes_sensores");

            migrationBuilder.DropTable(
                name: "mensagens_enviadas");

            migrationBuilder.DropTable(
                name: "parametros_sensor");

            migrationBuilder.DropTable(
                name: "usuarios_api");

            migrationBuilder.DropTable(
                name: "sensores");

            migrationBuilder.DropTable(
                name: "canais_comunicacao_mensagem");

            migrationBuilder.DropTable(
                name: "destinatarios_alerta");

            migrationBuilder.DropTable(
                name: "niveis_risco");

            migrationBuilder.DropTable(
                name: "tipos_sensores");

            migrationBuilder.DropTable(
                name: "regioes_alerta");
        }
    }
}
