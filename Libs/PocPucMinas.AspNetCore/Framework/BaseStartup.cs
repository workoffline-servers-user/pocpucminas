using PocPucMinas.AspNetCore.DependencyInjection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection.Extensions;
using PocPucMinas.QosLib.DependencyInjection;
using PocPucMinas.CommonLib.DependencyInjection;

namespace PocPucMinas.AspNetCore.Framework
{
    public abstract class BaseStartup
    {
        public IHostingEnvironment HostingEnvironment { get; protected set; }
        public IConfiguration Configuration { get; set;  }
        public ILoggerFactory LoggerFactory { get; }
        
        protected BaseStartup(IConfiguration configuration, IHostingEnvironment hostingEnvironment,
            ILoggerFactory loggerFactory)
        {
            //injeta as dependências
            Configuration = configuration;
            HostingEnvironment = hostingEnvironment;
            LoggerFactory = loggerFactory;

            //define os parametros de configuração e ambiente
            this.DefineParametrosDeConfiguracaoEAmbiente();
        }

        protected void InjetaServicosPadroesDoFramework(IServiceCollection services, params string[] hostsAutorizadosCORS)
        {
            services.AdicionaAppSettingsLoader();

            //Define configurações para o IConfiguration após injetar o AppSettingsLoader
            this.DefineConfiguracoes(services);

            //Adiciona API Warmer
            services.AdicionaApiWarmer();

            //Adicionando suporte a Requests Ajax (CORS)
            services.AdicionaModuloCORS(hostsAutorizadosCORS);

            //Adicionando módulo de Autenticação
            services.AdicionaModuloDeAutenticacao();

            //Adiciona módudo ASP.NET Core MVC
            services.AddMvc()

                //Converte inputs de pascalCase para CamelCase
                //e outputs de de CamelCase para pascalCase
                .ConfiguraTratamentoDeJson()

                //Define o modo de compatibilidade da API para ASP.NET Core 2.2
                .ConfiguraVersaoDeCompatibilidadeAspNetCore()

                //Configurações do Módulo WebAPI
                //Aplicadas ao utilizar Annotation [ApiController] nos Controllers
                .ConfiguraComportamentoDoModuloWebAPI();

            //HttpContextAccessor
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.TryAddScoped<IHttpContextAccessor, HttpContextAccessor>();
            services.TryAddTransient<IHttpContextAccessor, HttpContextAccessor>();

            //QOS
            services.AdicionaModuloQos();

            //Cache
            services.AdicionaModuloCache();

            //Serviço responsável por construir URLs da aplicação
            services.AdicionaModuloUrlFactory();

            //Adiciona o módulo de versionamento da API
            services.AdicionaModuloDeVersionamento();

            //Adiciona o módulo Swagger
            services.AdicionaModuloSwagger();

            //Adiciona o módulo de Log
            services.AdicionaModuloLog();

        }

        /// <summary>
        /// Utiliza os serviços padrões do Framework
        /// </summary>
        /// <param name="app"></param>
        public void UtilizaServicosPadroes(IApplicationBuilder app)
        {
            //utilizar as regras de liberação para requisições cross-origin
            app.UtilizaModuloCORS(Configuration);

            //habilita o detalhamento de erros
            app.UtilizaDetalhamentoDeErros();

            //define regras de mapeamento de arquivos
            app.UtilizaMapeamentoDeArquivos();

            //adicionando regras personalizadas de tratamento de headers
            app.UtilizaRegrasPersonalizadasDeHeaders();

            //adicionando o handler de exceção
            app.UtilizaManipuladorDeExcecoes();

            //ativa o módulo de autenticação
            app.UtilizaModuloDeAutenticacao();

            //utiliza o padrão de rota definido pelo Framework
            app.UtilizaRotasPadraoDoMvc();

            //utiliza o Swagger para geração de documentação da API
            app.UtilizaSwagger();
        }

        /// <summary>
        /// Método chamado em tempo de execução (runtime).
        /// Usado para adicionar serviços ao container da aplicação.
        /// </summary>
        /// <param name="services">Instância da coleção de serviços a serem injetados nos Controllers</param>
        public void ConfigureServices(IServiceCollection services)
        {
            //verifica se os parâmetros de configuração da aplicação e de ambiente foram definidos
            this.VerificaParametrosDeAmbiente();

            //injeta os serviços padrões do Framework
            InjetaServicosPadroesDoFramework(services);

            //injeta serviços específicos da aplicação
            InjetaServicosEspecificosDaAplicacao(services);
            
            //Faz o build
            services.BuildServiceProvider();
        }
             
        /// <summary>
        /// Método chamado em tempo de execução (runtime).
        /// Usado para configurar o pipeline das requisições HTTP.
        /// </summary>
        /// <param name="app">Instância do construtor da aplicação</param>
        protected virtual void Configure(IApplicationBuilder app, IApiVersionDescriptionProvider apiVersionDescriptionProvider)
        {
            //verifica se os parâmetros de configuração da aplicação e de ambiente foram definidos
            this.VerificaParametrosDeAmbiente();
            
            //define a ApiVersionDescriptionProvider
            this.DefineApiVersionDescriptionProvider(apiVersionDescriptionProvider);

            //utiliza os serviços injetados, com definições padrões do Framework
            UtilizaServicosPadroes(app);

            //utiliza os serviços injetados especificamente para o app que utiliza o Framework
            UtilizaServicosEspecificosDaAplicacao(app);
        }
        
        /// <summary>
        /// Método a ser implementado, que realiza a injeção de dependências de serviços da aplicação.
        /// </summary>
        public abstract void InjetaServicosEspecificosDaAplicacao(IServiceCollection services);

        public abstract void UtilizaServicosEspecificosDaAplicacao(IApplicationBuilder app);
                
        internal static void MantemPoolsDeConexoesAtivos(IConfiguration configuration)
        {
            //MantemPoolsDeConexoesAtivos(configuration);
        }
        
    }
}
