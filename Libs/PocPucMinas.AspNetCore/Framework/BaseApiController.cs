﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PocPucMinas.QosLib.Interfaces;

namespace PocPucMinas.AspNetCore.Framework
{
    [ApiController]
    [Route("api/v{version:apiVersion}/[area]/[controller]")]
    public class BaseApiController<T> : Controller where T : BaseApiController<T>
    {
        protected readonly IQosService _qosService;

        public BaseApiController(IQosService qosService)
        {            
            _qosService = qosService;            
        }
    }
}
