﻿using PocPucMinas.CommonLib.Services.Implementations;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace PocPucMinas.AspNetCore.Framework
{
    public class BaseWebApiProgram
    {
        public static IWebHost ConstruirAplicacao<TStartup>(string[] args) where TStartup : BaseStartup
        {
            var parametrosAplicacao = NormalizaArgumentos(args);

            return WebHost.CreateDefaultBuilder(args)

                //Define o Content Root
                .UseContentRoot(Directory.GetCurrentDirectory())

                //Setando variáveis dos arquivos appsettings
                .ConfigureAppConfiguration((hostingContext, config) =>
                {
                    config.Sources.Clear();
                })
                .UseStartup<TStartup>()
                .Build();
        }

        private static string[] NormalizaArgumentos(string[] args)
        {
            if (args == default(string[]))
            {
                args = new string[] { };
            }

            var listArgs = new List<string>();
            listArgs.AddRange(args);
            
            return listArgs.ToArray();
        }
        
    }
    
}
