﻿using Microsoft.AspNetCore.Mvc.Filters;
using PocPucMinas.AspNetCore.Security.Services.Interfaces;
using System;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;
using System.Text;
using PocPucMinas.AspNetCore.Security.DTO;
using PocPucMinas.AspNetCore.Security.Exceptions;

namespace PocPucMinas.AspNetCore.Security.Attributes
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class AutorizacaoAttribute : Attribute, IActionFilter
    {
        public bool ValidacaoRealizada { get; private set; }

        public AutorizacaoAttribute()
        {
           
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            //já recupera o token a partir do Header "Authorization" internamente no manipulador de Token
            //var authorizationHeader = context.HttpContext.Request.Headers["Authorization"];            
            var servicoAutenticacao = context.HttpContext.RequestServices.GetService<IAutenticacaoService>();

            //Verifica se o usuário está autenticado
            var jwtPayload = servicoAutenticacao.GetPayloadDoAccessTokenAtual();
            if (jwtPayload != default)
            {
                var identificadorUsuario = (long) jwtPayload["IdentificadorUsuario"];
                if (identificadorUsuario != default)
                {
                    ValidacaoRealizada = true;
                    return;
                }
                else
                {
                    throw new AutenticacaoException("Usuário não autenticado.");
                }

            }

            throw new AutenticacaoException("Usuário não autenticado");
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {

        }

    }

}
