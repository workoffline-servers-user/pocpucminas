namespace PocPucMinas.AspNetCore.Security.DTO
{
    public class JsonWebToken
    {
        public string AccessToken { get; set; }
        public long PrazoExpiracao { get; set; }
        public int IdentificadorUsuario { get; set; }
    }
}
