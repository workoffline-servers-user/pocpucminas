﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PocPucMinas.AspNetCore.Security.Exceptions
{
    class AutorizacaoException : Exception
    {
        public AutorizacaoException(string mensagem) : base(mensagem)
        {

        }
    }
}
