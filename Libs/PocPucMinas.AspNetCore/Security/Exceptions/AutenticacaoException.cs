﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PocPucMinas.AspNetCore.Security.Exceptions
{
    public class AutenticacaoException : Exception
    {
        public AutenticacaoException(string mensagem) : base(mensagem)
        {

        }
    }
}
