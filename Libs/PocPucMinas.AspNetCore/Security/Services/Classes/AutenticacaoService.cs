using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using PocPucMinas.AspNetCore.Security.DTO;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using PocPucMinas.DataDriver.UnitsOfWork;
using PocPucMinas.AspNetCore.Security.Services.Interfaces;
using PocPucMinas.DataLib.Models;

namespace PocPucMinas.AspNetCore.Security.Services.Classes
{
    public class AutenticacaoService : IAutenticacaoService
    {  
        private readonly IAutenticacaoCriptografiaService _autenticacaoCriptografiaService;
        private readonly IParametrosAutenticacao _parametrosAutenticacao;

        private readonly IBaseUnitOfWork _unitOfWork;
        private readonly IHttpContextAccessor _httpContextAccessor;
  
        public AutenticacaoService(
            IParametrosAutenticacao parametrosAutenticacao,
            IBaseUnitOfWork unitOfWork,
            IHttpContextAccessor httpContextAccessor,
            IAutenticacaoCriptografiaService autenticacaoCriptografiaService)
        {        
            _autenticacaoCriptografiaService = autenticacaoCriptografiaService;

            _unitOfWork = unitOfWork;
            _parametrosAutenticacao = parametrosAutenticacao;
            _httpContextAccessor = httpContextAccessor;
        }

        /// <summary>
        /// Autentica o acesso do usuário
        /// </summary>
        /// <param name="usuario">Usuário a ser autenticado</param>
        /// <returns>JWT</returns>
        public async Task<JsonWebToken> AutenticaUsuario(string tokenUsuario, string senhaUsuario)
        {            
            //valida as credenciais de acesso do usuário
            var usuarioObject = await ValidaCredenciaisUsuario(tokenUsuario, senhaUsuario);
            if (usuarioObject == default)
            {
                throw new Exception("Credenciais inválidas.");
            }
            
            //instancia o JWT baseado no CPF/CNPJ do usuário
            var jsonWebToken = _autenticacaoCriptografiaService.CriaNovoTokenJwt(usuarioObject);

            //calcula Refresh Token com valor aleatório
            var identificadorRefreshToken = GetNovoIdentificadorRefreshTokenAleatorio();
            
            return jsonWebToken;            
        }
        
        /// <summary>
        /// Retorna o payload do Access Token informado, caso esteja válido e ativo
        /// </summary>
        /// <param name="accessToken">Access Token (criptografado)</param>
        /// <returns>Payload do JWT</returns>
        public Dictionary<string, object> GetPayloadDoAccessToken(string accessToken)
        {
            var dadosAccessToken = _autenticacaoCriptografiaService.DescriptografarToken(accessToken);
            if (dadosAccessToken != default
                && (long)dadosAccessToken["TimestampExpiracao"] > _autenticacaoCriptografiaService.GetTimestampAtualUtc())
            {
                return dadosAccessToken;
            }

            return default;
        }

        /// <summary>
        /// Get Payload do access Token atual
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, object> GetPayloadDoAccessTokenAtual()
        {
            return GetPayloadDoAccessToken(ObtemAccessTokenAtual());
        }

        /// <summary>
        /// Verifica se o access token passado por parâmetro está ativo
        /// </summary>
        /// <param name="accessToken">Access Token</param>
        /// <returns>TRUE caso o access token esteja ativo</returns>
        public bool VerificaSeAccessTokenEstaAtivo(string accessToken)
        {
            var dadosAccessToken = GetPayloadDoAccessToken(accessToken);            
            if(dadosAccessToken != default)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Verifica se o access token atual está ativo
        /// </summary>
        /// <returns>TRUE caso o access token atual esteja ativo</returns>
        public bool VerificaSeAccessTokenAtualEstaAtivo()
        {
            return VerificaSeAccessTokenEstaAtivo(ObtemAccessTokenAtual());
        }

        /// <summary>
        /// Obten o acess token atual a partir do request header
        /// </summary>
        /// <returns>Access token criptografado</returns>
        public string ObtemAccessTokenAtual()
        {
            //recupera o valor do token através do Header "Authorization"
            var authorizationHeader = _httpContextAccessor.HttpContext.Request.Headers["Authorization"];

            return authorizationHeader == StringValues.Empty
                ? string.Empty
                : authorizationHeader.Single().Split(" ").Last();
        }
        
        /// <summary>
        /// Gera novo identificador de Refresh Token Aleatório
        /// </summary>
        /// <returns>Hash do refresh token gerado</returns>
        private string GetNovoIdentificadorRefreshTokenAleatorio()
        {
            return CalculaHash(Guid.NewGuid());
        }

        /// <summary>
        /// Calcula Hash do GUID informado
        /// </summary>
        /// <param name="guid">Objeto GUID</param>
        /// <returns>Hash (utilizando algorítimo SHA512)</returns>
        private string CalculaHash(Guid guid)
        {
            //valida Guid informado
            if(guid == default)
            {
                throw new Exception("Identificador inválido.");
            }

            //faz o cálculo do Hash
            using (var algorithm = SHA512.Create())
            {
                var hashedBytes = algorithm.ComputeHash(Encoding.UTF8.GetBytes(guid.ToString()));
                return BitConverter.ToString(hashedBytes).Replace("-", "").ToLower();
            }
        }

        /// <summary>
        /// Verifica se as credenciais do cliente informadas são válidas
        /// </summary>
        /// <param name="cpfCnpjUsuario">CPF/CNPJ do cliente</param>
        /// <param name="chaveDeAcessoAPI">Chave de acesso à API recebida por SMS/email</param>
        /// <returns></returns>
        private async Task<UsuarioApi> ValidaCredenciaisUsuario(string tokenUsuario, string senhaUsuario)
        {
            var usuario = await _unitOfWork.Repository<UsuarioApi>()
                .FindAsync(x => x.TokenDeAcesso == tokenUsuario && x.SenhaDeAcesso == senhaUsuario && x.Ativo);

            if(usuario != default)
            {
                return usuario;
            }
            else
            {
                return default;
            }

        }        

    }

}
