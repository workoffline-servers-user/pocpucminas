using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using PocPucMinas.AspNetCore.Security.DTO;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using PocPucMinas.AspNetCore.Security.Services.Interfaces;
using PocPucMinas.DataLib.Models;
using Microsoft.IdentityModel.Logging;

namespace PocPucMinas.AspNetCore.Security.Services.Classes
{
    public class AutenticacaoCriptografiaService : IAutenticacaoCriptografiaService
    {
        private readonly IParametrosAutenticacao _parametrosAutenticacao;
        private readonly JwtSecurityTokenHandler _jwtSecurityTokenHandler = new JwtSecurityTokenHandler();
        private readonly SecurityKey _chaveCriptografia;
        private readonly EncryptingCredentials _credenciaisDeCriptografia;
        private readonly JwtHeader _headerJwt;

        public AutenticacaoCriptografiaService(IParametrosAutenticacao parametrosAutenticacao)
        {
            _parametrosAutenticacao = parametrosAutenticacao;

            var chaveCriptografiaFull = _parametrosAutenticacao.GetChaveCriptografiaJwt();
            var chaveCriptografia = chaveCriptografiaFull.Length > 32 ? chaveCriptografiaFull.Substring(0, 32) : chaveCriptografiaFull;

            _chaveCriptografia = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(chaveCriptografia));
            _credenciaisDeCriptografia = new EncryptingCredentials(
                _chaveCriptografia,
                SecurityAlgorithms.Aes256KW,
                SecurityAlgorithms.Aes256CbcHmacSha512);

            _headerJwt = new JwtHeader(_credenciaisDeCriptografia);
        }                

        /// <summary>
        /// Cria o token JWT, criptografado
        /// </summary>
        /// <param name="dadosPayload">Dados do Payload a serem inclu�dos</param>
        /// <returns>Objeto Json Web Token</returns>
        public JsonWebToken CriaNovoTokenJwt(UsuarioApi usuarioApi)
        {
            var dataAtualUtc = DateTime.UtcNow;
            var dataExpiracaoUtc = dataAtualUtc.Add(GetTempoDeExpiracaoAccessToken());
            var inicioEraTimestampUtc = new DateTime(1970, 1, 1).ToUniversalTime();

            //calcula timestamps a serem gravados no payload
            var timestampExpiracao = (long)(new TimeSpan(dataExpiracaoUtc.Ticks - inicioEraTimestampUtc.Ticks).TotalSeconds);
            var timestampDataAtual = (long)(new TimeSpan(dataAtualUtc.Ticks - inicioEraTimestampUtc.Ticks).TotalSeconds);

            //dados a serem criptografados no JWT
            var jwtPayload = new JwtPayload()
            {                
                { "Emissor", _parametrosAutenticacao.GetNomeEmissorTokenJwt() },
                { "TimestampLogin", timestampDataAtual },
                { "IdentificadorUsuario", usuarioApi.UsuarioApiId },
                { "TimestampExpiracao", timestampExpiracao }
            };
            
            var jwt = new JwtSecurityToken(_headerJwt, jwtPayload);
            var tokenCriptografado = _jwtSecurityTokenHandler.WriteToken(jwt);
            
            //Token de acesso + prazo de expira��o
            return new JsonWebToken
            {
                AccessToken = tokenCriptografado,
                IdentificadorUsuario = usuarioApi.UsuarioApiId,
                PrazoExpiracao = timestampExpiracao
            };
        }

        /// <summary>
        /// Descriptografa access token
        /// </summary>
        /// <param name="accessToken">Access token (criptografado)</param>
        /// <returns>Payload do JWT, contendo informa��es do usu�rio</returns>
        public Dictionary<string, object> DescriptografarToken(string accessToken)
        {
            try
            {
                IdentityModelEventSource.ShowPII = true;
                var validationParameters = new TokenValidationParameters
                {
                    TokenDecryptionKey = _credenciaisDeCriptografia.Key,
                    ValidateIssuerSigningKey = false,
                    ValidateAudience = false,
                    ValidateIssuer = false,
                    ValidateLifetime = false,
                    RequireSignedTokens = false
                };

                var chaims = _jwtSecurityTokenHandler.ValidateToken(accessToken, validationParameters, out SecurityToken securityToken);

                var jwtSecurityToken = (JwtSecurityToken)securityToken;
                var dadosPayload = (Dictionary<string, object>)jwtSecurityToken.Payload;

                if (dadosPayload.Count > 0)
                {
                    return dadosPayload;
                }
                
            }
            catch
            {
                return default;
            }

            return default;
        }
        
        public long GetTimestampAtualUtc()
        {
            var inicioEraTimestampUtc = new DateTime(1970, 1, 1).ToUniversalTime();
            var dataAtualUtc = DateTime.UtcNow;
            return (long)(new TimeSpan(dataAtualUtc.Ticks - inicioEraTimestampUtc.Ticks).TotalSeconds);
        }

        public TimeSpan GetTempoDeExpiracaoAccessToken()
        {
            return TimeSpan.FromMinutes(_parametrosAutenticacao.GetTempoDeExpiracaoAccessTokenEmMinutos());
        }

    }

}
