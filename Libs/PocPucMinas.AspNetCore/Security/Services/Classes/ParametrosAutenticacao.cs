﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using PocPucMinas.AspNetCore.Security.Services.Interfaces;
using PocPucMinas.CommonLib.Services.Interfaces;

namespace PocPucMinas.AspNetCore.Security.Services.Classes
{
    public class ParametrosAutenticacao : IParametrosAutenticacao
    {
        IConfiguration _configuracao;

        public ParametrosAutenticacao(IAppSettingsLoaderService appSettingsLoader)
        {
            _configuracao = appSettingsLoader.GetConfiguration();
        }       

        public int GetTempoDeExpiracaoAccessTokenEmMinutos()
        {
            return _configuracao.GetSection("Seguranca:TempoDeExpiracaoAccessTokenEmMinutos").Get<int>();
        }

        public string GetChaveCriptografiaJwt()
        {
            return _configuracao.GetSection("Seguranca:ChaveCriptografia").Get<string>();
        }

        public string GetNomeEmissorTokenJwt()
        {
            return _configuracao.GetSection("Seguranca:NomeEmissorToken").Get<string>();
        }

    }

}
