﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PocPucMinas.AspNetCore.Security.Services.Interfaces
{
    public interface IParametrosAutenticacao
    {
        int GetTempoDeExpiracaoAccessTokenEmMinutos();
        string GetChaveCriptografiaJwt();
        string GetNomeEmissorTokenJwt();
    }
}
