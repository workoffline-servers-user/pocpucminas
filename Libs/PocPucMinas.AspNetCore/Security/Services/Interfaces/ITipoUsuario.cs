﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PocPucMinas.AspNetCore.Security.Services.Interfaces
{
    public interface ITipoUsuario
    {
        string GetChaveHash();

        Task<bool> ValidaCredenciais();

        Dictionary<string, object> GetPayload();
    }
}
