using PocPucMinas.AspNetCore.Security.DTO;
using PocPucMinas.DataLib.Models;
using System;
using System.Collections.Generic;
using System.Security.Claims;

namespace PocPucMinas.AspNetCore.Security.Services.Interfaces
{
    public interface IAutenticacaoCriptografiaService
    {
        JsonWebToken CriaNovoTokenJwt(UsuarioApi usuarioApi);
        Dictionary<string, object> DescriptografarToken(string accessToken);
        long GetTimestampAtualUtc();
        TimeSpan GetTempoDeExpiracaoAccessToken();
    }
}
