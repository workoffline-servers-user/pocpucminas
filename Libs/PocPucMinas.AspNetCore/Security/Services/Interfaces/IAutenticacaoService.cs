
using PocPucMinas.AspNetCore.Security.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PocPucMinas.AspNetCore.Security.Services.Interfaces
{
    public interface IAutenticacaoService
    {
        Task<JsonWebToken> AutenticaUsuario(string tokenUsuario, string senhaUsuario);
        Dictionary<string, object> GetPayloadDoAccessToken(string accessToken);
        Dictionary<string, object> GetPayloadDoAccessTokenAtual();
        bool VerificaSeAccessTokenEstaAtivo(string accessToken);
        string ObtemAccessTokenAtual();
        bool VerificaSeAccessTokenAtualEstaAtivo();
    }
}
