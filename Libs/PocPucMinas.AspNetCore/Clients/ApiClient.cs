﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace PocPucMinas.AspNetCore.Clients
{
    public class ApiClient
    {
        protected HttpClient Client;
        private IEnumerable<MediaTypeFormatter> InputFormatters;
        private readonly IEnumerable<MediaTypeFormatter> OutputFormatters;

        public ApiClient(string baseAddress, TimeSpan? timeout = null)
        {
            Client = new HttpClient
            {
                BaseAddress = new Uri(baseAddress)
            };

            Client.DefaultRequestHeaders.Accept.Clear();
            Client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            if (!timeout.HasValue)
            {
                Client.Timeout = TimeSpan.FromMinutes(10);
            }
            else
            {
                Client.Timeout = timeout.Value;
            }

            Client.MaxResponseContentBufferSize = 2147483647; // 2 GB

            var jsonFormatter = new JsonMediaTypeFormatter();
            jsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            InputFormatters = new List<MediaTypeFormatter>
            {
                jsonFormatter
            };

            OutputFormatters = new List<MediaTypeFormatter>
            {
                jsonFormatter
            };
        }

        /// <summary>
        /// Construtor padrão
        /// Aceita serviços que respondem o requests em JSON (application/json)
        /// </summary>
        public ApiClient(string baseAddress, string accessToken)
            : this(baseAddress)
        {
            Client.DefaultRequestHeaders.Add("Authorization", accessToken);
        }

        /// <summary>
        /// Método responsável por realizar requisições utilizando verbo HTTP GET
        /// </summary>
        /// <typeparam name="OutputType">Tipo de dados de saída</typeparam>
        /// <param name="path">URL do request, com parâmetros</param>
        /// <returns>Objeto do tipo definido pelo parâmetro de tipo OutputType</returns>
        public async Task<OutputType> Get<OutputType>(string path)
        {
            HttpResponseMessage response = await Client.GetAsync(path);
            if (response.IsSuccessStatusCode)
            {
                var responseContent = await response.Content.ReadAsAsync<OutputType>(OutputFormatters);
                return responseContent;
            }
            return default(OutputType);
        }

        public async Task<IActionResult> Get(string path)
        {
            var response = await Client.GetAsync(path);
            if (response != null)
            {
                return await TrataResposta(response);
            }
            return default(IActionResult);
        }

        /// <summary>
        /// Método responsável por realizar requisições utilizando verbo HTTP POST
        /// </summary>
        /// <typeparam name="OutputType">Tipo de dados de saída</typeparam>
        /// <typeparam name="InputType">Tipo de dados de entrada</typeparam>
        /// <param name="path"></param>
        /// <param name="objetoInput">URL do request, com parâmetros</param>
        /// <returns>Objeto do tipo definido pelo parâmetro de tipo OutputType</returns>
        public async Task<OutputType> Post<OutputType, InputType>(string path, InputType objetoInput)
        {
            HttpResponseMessage response = await Client.PostAsync<InputType>(path, objetoInput, InputFormatters.FirstOrDefault());
            if (response.IsSuccessStatusCode)
            {
                var responseContent = await response.Content.ReadAsAsync<OutputType>(OutputFormatters);
                return responseContent;
            }
            return default(OutputType);
        }

        public async Task<IActionResult> Post<InputType>(string path, InputType objetoInput)
        {
            var response = await Client.PostAsync<InputType>(path, objetoInput, InputFormatters.FirstOrDefault());

            if (response != null)
            {
                return await TrataResposta(response);
            }
            return default(IActionResult);
        }

        /// <summary>
        /// Método responsável por realizar requisições utilizando verbo HTTP PUT
        /// </summary>
        /// <typeparam name="OutputType">Tipo de dados de saída</typeparam>
        /// <typeparam name="InputType">Tipo de dados de entrada</typeparam>
        /// <param name="path"></param>
        /// <param name="objetoInput">URL do request, com parâmetros</param>
        /// <returns>Objeto do tipo definido pelo parâmetro de tipo OutputType</returns>
        public async Task<OutputType> Put<OutputType, InputType>(string path, InputType objetoInput)
        {
            HttpResponseMessage response = await Client.PutAsync(path, objetoInput, InputFormatters.FirstOrDefault());
            if (response.IsSuccessStatusCode)
            {
                var responseContent = await response.Content.ReadAsAsync<OutputType>(OutputFormatters);
                return responseContent;
            }
            return default(OutputType);
        }

        public async Task<IActionResult> Put<InputType>(string path, InputType objetoInput)
        {
            var response = await Client.PutAsync(path, objetoInput, InputFormatters.FirstOrDefault());
            if (response != null)
            {
                return await TrataResposta(response);
            }
            return default(IActionResult);
        }

        /// <summary>
        /// Método responsável por realizar requisições utilizando verbo HTTP DELETE
        /// </summary>
        /// <typeparam name="OutputType">Tipo de dados de saída</typeparam>
        /// <param name="path">URL do request, com parâmetros</param>
        /// <returns>Objeto do tipo definido pelo parâmetro de tipo OutputType</returns>
        public async Task<OutputType> Delete<OutputType>(string path)
        {
            HttpResponseMessage response = await Client.DeleteAsync(path);
            if (response.IsSuccessStatusCode)
            {
                var responseContent = await response.Content.ReadAsAsync<OutputType>(OutputFormatters);
                return responseContent;
            }
            return default;
        }

        public async Task<IActionResult> Delete(string path)
        {
            var response = await Client.DeleteAsync(path);
            if (response != null)
            {
                return await TrataResposta(response);
            }
            return default;
        }

        public void Dispose()
        {
            ((IDisposable)Client).Dispose();
        }

        private async Task<IActionResult> TrataResposta(HttpResponseMessage response)
        {
            if (response.Content == null || response.Content.Headers == null ||
                response.Content.Headers.ContentType == null || response.Content.Headers.ContentType.MediaType == null)
            {
                return null;
            }

            var responseMediaType = response.Content.Headers.ContentType.MediaType;
            if (responseMediaType == "application/json")
            {
                return new JsonResult(await response.Content.ReadAsAsync<object>(OutputFormatters));
            }
            else if (responseMediaType == "application/octet-stream")
            {
                var fileStreamResult = new FileStreamResult(await response.Content.ReadAsStreamAsync(), response.Content.Headers.ContentType.MediaType);
                fileStreamResult.FileDownloadName = response.Content.Headers.ContentDisposition.FileName;
                return fileStreamResult;
            }
            else
            {
                return new ContentResult
                {
                    Content = await response.Content.ReadAsStringAsync(),
                    ContentType = responseMediaType,
                    StatusCode = (short)response.StatusCode
                };
            }
        }

        /// <summary>
        /// Destrutor padrão
        /// Cancela todas as conexões pendentes
        /// </summary>
        ~ApiClient()
        {
            if (Client != default(HttpClient))
            {
                try
                {
                    Client.CancelPendingRequests();
                }
                catch (Exception)
                {

                }
            }
        }
    }
}
