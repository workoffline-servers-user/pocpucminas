﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using PocPucMinas.CommonLib.Services.Interfaces;
using PocPucMinas.DataDriver.UnitsOfWork;
using PocPucMinas.QosLib.Interfaces;

namespace PocPucMinas.AspNetCore.BusinessServices
{
    public class BaseBusinessGenericService<T> where T : BaseBusinessGenericService<T>
    {
        protected IBaseUnitOfWork _unitOfWork { get; private set; }
        protected readonly IQosService _qosService;
        protected readonly IConfiguration _configuration;

        public BaseBusinessGenericService(IBaseUnitOfWork unitOfWork, IQosService qosService, IAppSettingsLoaderService appSettingsLoaderService)
        {
            _unitOfWork = unitOfWork;
            _qosService = qosService;
            _configuration = appSettingsLoaderService.GetConfiguration();
        }
    }
}
