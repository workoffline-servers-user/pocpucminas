﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using PocPucMinas.AspNetCore.Framework;
using PocPucMinas.AspNetCore.Services.Interfaces;
using PocPucMinas.CommonLib.Services.Interfaces;
using PocPucMinas.QosLib.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PocPucMinas.AspNetCore.Controllers.Api
{
    [ApiExplorerSettings(IgnoreApi = true)]
    [Area("Probe")]
    public class ReadyController : BaseApiController<ReadyController>
    {
        private readonly IApiWarmer ApiWarmer;
        private readonly IConfiguration Configuration;
        
        private static bool isWarmedUp = false;

        public ReadyController(
            IAppSettingsLoaderService appSettingsLoader, 
            IQosService qosService,
            IApiWarmer apiWarmer)
            :base(qosService)
        {
            ApiWarmer = apiWarmer;
            Configuration = appSettingsLoader.GetConfiguration();
        }

        [HttpGet, HttpHead]
        public async Task<IActionResult> Get()
        {
            try
            {
                if (!isWarmedUp)
                {
                    await ApiWarmer.RealizaPreAquecimento(Url, Request, "Ready");
                    isWarmedUp = true;
                }

                BaseStartup.MantemPoolsDeConexoesAtivos(Configuration);
            }
            catch
            {

            }

            return Ok("API pronta!");
        }
    }
}
