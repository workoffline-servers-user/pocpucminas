﻿using PocPucMinas.AspNetCore.Security.Attributes;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PocPucMinas.AspNetCore.Swagger
{
    class OperationFilter
    {
        public class SegurancaOperationFilter : IOperationFilter
        {
            public void Apply(Operation operation, OperationFilterContext context)
            {
                var attributes = context.MethodInfo.DeclaringType.GetCustomAttributes(true)
                    .Union(context.MethodInfo.GetCustomAttributes(true));

                var authAttributes = attributes.OfType<AutorizacaoAttribute>();

                if (authAttributes.Any())
                {
                    operation.Security = new List<IDictionary<string, IEnumerable<string>>>
                    {
                        new Dictionary<string, IEnumerable<string>>
                        {
                            {"Bearer", Array.Empty<string>()}
                        }
                    };

                    var descricaoSeguranca = string.Empty;
                    if (authAttributes.Any())
                    {
                        descricaoSeguranca += "<li tipo-autenticacao='autenticacao-usuario'>Autenticação de Usuário</li>";
                    }
                    
                    if (!string.IsNullOrEmpty(descricaoSeguranca))
                    {
                        operation.Description += $"<h3>Autorização</h3><strong><ul>{descricaoSeguranca}</ul></strong>";
                    }

                    operation.Responses.Add("401", new Response { Description = "Acesso não autorizado." });
                    operation.Responses.Add("403", new Response { Description = "Usuário não autenticado." });

                }

                if (!operation.Responses.Keys.Any(x => x == "412"))
                {
                    operation.Responses.Add("412", new Response { Description = "Erro de validação de parâmetros de entrada." });
                }

                if (!operation.Responses.Keys.Any(x => x == "400"))
                {
                    operation.Responses.Add("400", new Response { Description = "Requisição inválida." });
                }

                if (!operation.Responses.Keys.Any(x => x == "500"))
                {
                    operation.Responses.Add("500", new Response { Description = "Ocorreu um erro inesperado." });
                }

            }

        }
    }
}
