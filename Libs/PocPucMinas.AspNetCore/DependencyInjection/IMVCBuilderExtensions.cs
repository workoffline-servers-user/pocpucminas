using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using PocPucMinas.CommonLib.Support;
using PocPucMinas.ExceptionLib.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace PocPucMinas.AspNetCore.DependencyInjection
{
    public static partial class ServiceInjectionExtensions
    {
        public static IMvcBuilder ConfiguraTratamentoDeJson(this IMvcBuilder mvcBuilder)
        {
            return mvcBuilder.AddJsonOptions(options =>
            {
                var defaultSerializationSettings = JsonSerialization.GetDefaultSerializationSettings();

                options.SerializerSettings.ContractResolver = defaultSerializationSettings.ContractResolver;
                options.SerializerSettings.ReferenceLoopHandling = defaultSerializationSettings.ReferenceLoopHandling;
                options.SerializerSettings.DateFormatHandling = defaultSerializationSettings.DateFormatHandling;
                options.SerializerSettings.DateParseHandling = defaultSerializationSettings.DateParseHandling;
                options.SerializerSettings.DateTimeZoneHandling = defaultSerializationSettings.DateTimeZoneHandling;
            });
        }
        
        public static IMvcBuilder ConfiguraVersaoDeCompatibilidadeAspNetCore(this IMvcBuilder mvcBuilder)
        {
            return mvcBuilder.SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        public static IMvcBuilder ConfiguraComportamentoDoModuloWebAPI(this IMvcBuilder mvcBuilder)
        {
            return mvcBuilder.ConfigureApiBehaviorOptions(options =>
             {
                 options.SuppressConsumesConstraintForFormFileParameters = false;
                 options.SuppressInferBindingSourcesForParameters = false;
                 options.SuppressModelStateInvalidFilter = true;
                 options.SuppressMapClientErrors = false;
                 options.InvalidModelStateResponseFactory = ctx => new ValidationProblemDetailsResult();
                 options.ClientErrorMapping[404].Link = "https://httpstatuses.com/404";
             });
        }

    }
}
