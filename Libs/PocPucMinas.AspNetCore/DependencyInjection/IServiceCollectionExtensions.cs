using Gelf.Extensions.Logging;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.IdentityModel.Tokens;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using PocPucMinas.CommonLib.Services.Implementations;
using PocPucMinas.CommonLib.Extensions;
using PocPucMinas.CommonLib.Services.Interfaces;
using Microsoft.AspNetCore.Cors.Infrastructure;
using PocPucMinas.AspNetCore.Services.Interfaces;
using PocPucMinas.AspNetCore.Services.Implementations;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Routing;
using PocPucMinas.QosLib.DependencyInjection;
using PocPucMinas.CommonLib.DependencyInjection;
using static PocPucMinas.AspNetCore.Swagger.OperationFilter;
using System.Collections.Generic;

namespace PocPucMinas.AspNetCore.DependencyInjection
{
    public static partial class ServiceInjectionExtensions
    {
        public static IServiceCollection AdicionaModuloDeAutenticacao(this IServiceCollection services)
        {
            /*
            services.AddTransient<ConstanteComum, ConstanteAppSettingsComum>();
            services.AddTransient<ConfigurationHelper, ConfigurationAppSettingsHelper>();
            services.AddTransient<IParametrosAutenticacao, ParametrosAutenticacao>();
            services.AddTransient<FactoryFila<NodoA>, FactoryFilaRedis<NodoA>>();
            services.AddTransient<FactoryFila<NodoB>, FactoryFilaRedis<NodoB>>();
            services.AddTransient<ConstanteComunicacao, ConstanteAppSettingsComunicacao>();
            services.AddTransient<SenderStrategy, SenderRedisStrategy>();
            services.AddTransient<FactoryHash<NodoA>, FactoryHashRedis<NodoA>>();
            services.AddTransient<FactoryHash<NodoB>, FactoryHashRedis<NodoB>>();
            services.AddTransient<IAutenticacaoCriptografiaService, AutenticacaoCriptografiaService>();
            services.AddTransient<IHttpContextAccessor, HttpContextAccessor>();
            services.AddTransient<IAutenticacaoService, AutenticacaoService>();
                        
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(config =>
            {
                config.RequireHttpsMetadata = false; // true em produção
                config.SaveToken = true;

                config.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidIssuer = Configuration["JWT:Emissor"],
                    ValidAudience = Configuration["JWT:Emissor"],
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["JWT:Chave"])),

                    // Valida a assinatura de um token recebido
                    ValidateIssuerSigningKey = true,
                    ValidateAudience = true,

                    // Verifica se um token recebido ainda é válido
                    ValidateLifetime = true,

                    // Tempo de tolerância para a expiração de um token (utilizado
                    // caso haja problemas de sincronismo de horário entre diferentes
                    // computadores envolvidos no processo de comunicação)
                    ClockSkew = TimeSpan.Zero
                };
            });

            services.AddAuthorization(auth =>
            {
                auth.AddPolicy("Bearer", new AuthorizationPolicyBuilder()
                    .AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme‌​)
                    .RequireAuthenticatedUser().Build());
            });
            */

            return services;

        }

        public static IServiceCollection AdicionaApiWarmer(this IServiceCollection services)
        {
            services.AddSingleton<IApiWarmer, ApiWarmer>();
            return services;
        }

        public static IServiceCollection AdicionaModuloCORS(this IServiceCollection services, params string[] hostsDeOrigemAutorizados)
        {
            var allowSpecificOriginCorsBuilder = new CorsPolicyBuilder();
            allowSpecificOriginCorsBuilder.WithOrigins(hostsDeOrigemAutorizados);
            allowSpecificOriginCorsBuilder.AllowAnyHeader();
            allowSpecificOriginCorsBuilder.AllowAnyMethod();
            allowSpecificOriginCorsBuilder.AllowCredentials();

            var allowAnyOriginCorsBuilder = new CorsPolicyBuilder();
            allowAnyOriginCorsBuilder.AllowAnyOrigin();
            allowAnyOriginCorsBuilder.AllowAnyHeader();
            allowAnyOriginCorsBuilder.AllowAnyMethod();
            allowAnyOriginCorsBuilder.AllowCredentials();

            services.AddCors(options =>
            {
                options.AddPolicy("AllowSpecificOrigin", allowSpecificOriginCorsBuilder.Build());
                options.AddPolicy("AllowAnyOrigin", allowAnyOriginCorsBuilder.Build());
            });

            return services;
        }

        public static IServiceCollection AdicionaModuloCache(this IServiceCollection services)
        {
            services.AddSingleton<IActionContextAccessor, ActionContextAccessor>();
            return services;
        }

        public static IServiceCollection AdicionaModuloUrlFactory(this IServiceCollection services)
        {
            services.AddSingleton<IActionContextAccessor, ActionContextAccessor>();
            services.AddSingleton<IUrlHelper>(x => {
                var actionContext = x.GetRequiredService<IActionContextAccessor>().ActionContext;
                var factory = x.GetRequiredService<IUrlHelperFactory>();
                return factory.GetUrlHelper(actionContext);
            });

            services.AddSingleton<IUrlFactory, UrlFactory>();

            return services;
        }

        public static IServiceCollection AdicionaModuloDeVersionamento(this IServiceCollection services)
        {
            services.AddVersionedApiExplorer(o => o.GroupNameFormat = "'v'VVV");
            services.AddApiVersioning();

            return services;
        }

        public static IServiceCollection AdicionaModuloAspNetMvc(this IServiceCollection services)
        {
            //Adiciona módudo ASP.NET Core MVC
            services.AddMvc()

                //Converte inputs de pascalCase para CamelCase
                //e outputs de de CamelCase para pascalCase
                .ConfiguraTratamentoDeJson()

                //Define o modo de compatibilidade da API para ASP.NET Core 2.2
                .ConfiguraVersaoDeCompatibilidadeAspNetCore()

                //Configurações do Módulo WebAPI
                //Aplicadas ao utilizar Annotation [ApiController] nos Controllers
                .ConfiguraComportamentoDoModuloWebAPI();

            return services;
        }

        public static IServiceCollection AdicionaModuloSwagger(this IServiceCollection services)
        {
            var configuration = services.GetConfiguration();
            var nomeAPI = configuration.GetValue<string>("NomeAPI");
            var descricaoAPI = configuration.GetValue<string>("DescricaoAPI");
            var versaoAPI = configuration.GetValue<string>("VersaoAplicacao");

            if(string.IsNullOrEmpty(nomeAPI))
            {
                throw new Exception("Parâmetro NomeAPI não encontrado.");
            }

            if (string.IsNullOrEmpty(descricaoAPI))
            {
                throw new Exception("Parâmetro DescricaoAPI não encontrado.");
            }

            if (string.IsNullOrEmpty(versaoAPI))
            {
                throw new Exception("Parâmetro VersaoAplicacao não encontrado.");
            }

            services.AddSwaggerGen(
                options =>
                {
                    var provider = services.BuildServiceProvider().GetRequiredService<IApiVersionDescriptionProvider>();
                    foreach (var description in provider.ApiVersionDescriptions)
                    {
                        options.SwaggerDoc(
                            description.GroupName,
                            new Info()
                            {
                                Title = $"{nomeAPI} {description.ApiVersion}",
                                Version = description.ApiVersion.ToString(),
                                Description = descricaoAPI
                            }
                        );
                    }

                    var identificadorAplicacao = configuration.GetValue<string>("IdentificadorAplicacao");
                    var arquivoXml = Path.Combine(AppContext.BaseDirectory, "_Config", $"{identificadorAplicacao}.xml");

                    if (File.Exists(arquivoXml))
                    {
                        options.IncludeXmlComments(arquivoXml);
                    }

                    options.DescribeAllEnumsAsStrings();
                    options.OperationFilter<SegurancaOperationFilter>();

                    var security = new Dictionary<string, IEnumerable<string>>
                    {
                        {"Bearer", new string[] { }},
                    };

                    options.AddSecurityDefinition("Bearer", new ApiKeyScheme
                    {
                        Description = "Insira o token de autorização",
                        Name = "Authorization",
                        Type = "apiKey",
                        In = "header",
                    });

                }
           );

            return services;
        }
        
        public static void AdicionaTodosOsTiposDoMesmoNamespace<T>(this IServiceCollection services, ServiceLifetime tempoDeVida = ServiceLifetime.Transient)
        {
            var namespaceDaInterface = typeof(T).Namespace;
            var assemblyDaInterface = typeof(T).Assembly;

            var tiposInterfaceNoAssemblyComOMesmoNamespace = assemblyDaInterface.DefinedTypes.Where(x => x.IsInterface && x.Namespace == namespaceDaInterface);

            foreach(var tipoInterface in tiposInterfaceNoAssemblyComOMesmoNamespace)
            {
                var tipoConcreto = assemblyDaInterface.DefinedTypes.Where(t => t.GetInterfaces().Contains(tipoInterface)).FirstOrDefault();
                if(tipoConcreto != null)
                {
                    services.Add(new ServiceDescriptor(tipoInterface, tipoConcreto, tempoDeVida));
                }
            }
                        
        }
        
    }

}
