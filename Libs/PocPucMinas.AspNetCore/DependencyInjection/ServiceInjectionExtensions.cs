using PocPucMinas.AspNetCore.Framework;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using PocPucMinas.CommonLib.DependencyInjection;

namespace PocPucMinas.AspNetCore.DependencyInjection
{
    public static partial class ServiceInjectionExtensions
    {
        private static IHostingEnvironment HostingEnvironment = null;

        public static ILoggerFactory LoggerFactory = null;
        public static IApiVersionDescriptionProvider ApiVersionDescriptionProvider = null;

        public static void DefineParametrosDeConfiguracaoEAmbiente(this BaseStartup startupClass)
        {
            HostingEnvironment = startupClass.HostingEnvironment;
            LoggerFactory = startupClass.LoggerFactory;
        }

        public static void DefineConfiguracoes(this BaseStartup startupClass, IServiceCollection services)
        {
            startupClass.Configuration = services.GetConfiguration();
        }

        public static void DefineApiVersionDescriptionProvider(this BaseStartup startupClass, IApiVersionDescriptionProvider apiVersionDescriptionProvider)
        {
            ApiVersionDescriptionProvider = apiVersionDescriptionProvider;
        }
        
        public static void VerificaParametrosDeAmbiente(this BaseStartup startupClass)
        {
            if (HostingEnvironment == null)
            {
                throw new Exception("Parâmetros de ambiente não definidos.");
            }
        }

        public static void VerificaParametrosDeConfiguracoes(this BaseStartup startupClass)
        {
            if (startupClass.Configuration == null)
            {
                throw new Exception("Parâmetros de configuração não definidos.");
            }
        }

    }
}
