using PocPucMinas.ExceptionLib.Web;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpOverrides;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PocPucMinas.CommonLib.Extensions;
using Swashbuckle.AspNetCore.SwaggerUI;
using Microsoft.Extensions.Configuration;

namespace PocPucMinas.AspNetCore.DependencyInjection
{
    public static partial class ServiceInjectionExtensions
    {
        public static void UtilizaManipuladorDeExcecoes(this IApplicationBuilder app)
        {
            //adicionando o handler de exceção
            app.UseExceptionHandler(errorApp =>
            {
                errorApp.Run(async context =>
                {
                    await Task.Run(() => WebExceptionHandler.HandleException(context, LoggerFactory));
                });
            });
        }

        public static void UtilizaModuloCORS(this IApplicationBuilder app, IConfiguration configuration)
        {
            // Caso esteja sendo executado em ambiente de desenvolvimento
            if (!configuration.IsAmbienteProducao())
            {
                //Habilita requests Ajax provenientes de qualquer origem
                app.UseCors("AllowAnyOrigin");
            }
            else
            {
                //Habilita requests Ajax provenientes de qualquer origem
                app.UseCors("AllowAnyOrigin");
            }
        }

        public static void UtilizaRegrasPersonalizadasDeHeaders(this IApplicationBuilder app)
        {
            /*
            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                // Read and use headers coming from reverse proxy: X-Forwarded-For X-Forwarded-Proto
                // This is particularly important so that HttpContet.Request.Scheme will be correct behind a SSL terminating proxy
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto | ForwardedHeaders.XForwardedHost                
            });
            */
        }

        public static void UtilizaMapeamentoDeArquivos(this IApplicationBuilder app)
        {
            app.UseDefaultFiles();
            app.UseStaticFiles();
        }

        public static void UtilizaDetalhamentoDeErros(this IApplicationBuilder app)
        {
            //Habilita saída em tela do detalhamento de exceções
            app.UseDeveloperExceptionPage();

            //Habilita saída em tela do detalhamento de erros no Banco de Dados
            app.UseDatabaseErrorPage();
        }

        public static void UtilizaSwagger(this IApplicationBuilder app)
        {
            //adicionando o Swagger
            app.UseSwagger(options =>
            {
                options.RouteTemplate = "swagger/{documentName}/swagger.json";
                options.PreSerializeFilters.Add((swaggerDoc, httpRequest) =>
                {
                    var requestHeaders = httpRequest.Headers;

                    if (requestHeaders != null && requestHeaders.Keys != null && requestHeaders.Keys.Count > 0)
                    {
                        var proxyForwaredPathHeader = "X-Forwarded-Path";
                        if (requestHeaders.ContainsKey(proxyForwaredPathHeader))
                        {
                            try
                            {
                                var basePath = requestHeaders[proxyForwaredPathHeader];
                                swaggerDoc.BasePath = basePath;
                            }
                            catch (Exception ex)
                            {
                                throw new Exception("Falha ao definir Base Path para o Swagger", ex);
                            }
                        }

                        var proxyForwaredHostHeader = "X-Forwarded-Host";
                        if (requestHeaders.ContainsKey(proxyForwaredHostHeader))
                        {
                            try
                            {
                                var host = requestHeaders[proxyForwaredHostHeader];
                                swaggerDoc.Host = host;
                            }
                            catch (Exception ex)
                            {
                                throw new Exception("Falha ao definir Host para o Swagger", ex);
                            }
                        }

                        var proxyForwaredProtoHeader = "X-Forwarded-Proto";
                        if (requestHeaders.ContainsKey(proxyForwaredProtoHeader))
                        {
                            try
                            {
                                var scheme = requestHeaders[proxyForwaredProtoHeader];
                                if (swaggerDoc.Schemes == null)
                                {
                                    swaggerDoc.Schemes = new string[] { scheme };
                                }
                                else
                                {
                                    swaggerDoc.Schemes.Clear();
                                    swaggerDoc.Schemes.Add(scheme);
                                }

                            }
                            catch (Exception ex)
                            {
                                throw new Exception("Falha ao definir Scheme para o Swagger", ex);
                            }
                        }
                    }

                });

            });

            //adicionando o Swagger UI
            app.UseSwaggerUI(
                options =>
                {
                    foreach (var description in ApiVersionDescriptionProvider.ApiVersionDescriptions)
                    {
                        options.SwaggerEndpoint(
                            $"/swagger/{description.GroupName}/swagger.json",
                            description.GroupName.ToUpperInvariant());
                    }

                    options.DefaultModelRendering(ModelRendering.Model);
                    options.DefaultModelsExpandDepth(10);
                    options.DisplayOperationId();
                    options.DisplayRequestDuration();
                    options.DocExpansion(DocExpansion.List);
                    options.EnableDeepLinking();
                    options.EnableFilter();
                    options.ShowExtensions();
                    options.EnableValidator();

                });
        }

        public static void UtilizaRotasPadraoDoMvc(this IApplicationBuilder app)
        {
            //ativa o módulo ASP.NET MVC, definindo padrão de rotas com áreas
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "areaRoute",
                    template: "v{version:apiVersion}/{area:exists}/{controller=Home}/{action=Index}/{id?}");

                routes.MapRoute(
                    name: "default",
                    template: "v{version:apiVersion}/{controller=Home}/{action=Index}/{id?}");
            });
        }

        public static void UtilizaModuloDeAutenticacao(this IApplicationBuilder app)
        {
            //ativa o Middleware de Autenticação, utilizando JWT
            app.UseAuthentication();
        }

    }
}
