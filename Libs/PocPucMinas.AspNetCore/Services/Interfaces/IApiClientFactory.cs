﻿using PocPucMinas.AspNetCore.Clients;

namespace PocPucMinas.AspNetCore.Services.Interfaces
{
    public interface IApiClientFactory
    {
        ApiClient Factory(string baseUrl, bool enviarAccessTokenAtualSeDisponivel = true);
    }
}
