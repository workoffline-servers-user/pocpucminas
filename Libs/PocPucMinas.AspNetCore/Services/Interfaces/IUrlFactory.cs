﻿namespace PocPucMinas.AspNetCore.Services.Interfaces
{
    public interface IUrlFactory
    {
        string GetBaseUrl();
        string GetUrl(string routeName, object routeValues = null);
    }
}
