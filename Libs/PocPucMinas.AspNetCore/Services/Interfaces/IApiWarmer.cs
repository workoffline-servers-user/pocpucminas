﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PocPucMinas.AspNetCore.Services.Interfaces
{
    public interface IApiWarmer
    {
        Task RealizaPreAquecimento(IUrlHelper urlHelper, HttpRequest request, params string[] ignoredControllers);
    }
}
