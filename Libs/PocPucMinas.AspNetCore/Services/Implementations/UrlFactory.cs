﻿using PocPucMinas.AspNetCore.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace PocPucMinas.AspNetCore.Services.Implementations
{
    public class UrlFactory : IUrlFactory
    {
        private IHttpContextAccessor _httpContextAccessor;
        private IUrlHelper _urlHelper;

        public UrlFactory(IHttpContextAccessor httpContextAccessor, IUrlHelper urlHelper)
        {
            _httpContextAccessor = httpContextAccessor;
            _urlHelper = urlHelper;
        }

        public string GetBaseUrl()
        {
            if (_httpContextAccessor != null
                && _httpContextAccessor.HttpContext != null
                    && _httpContextAccessor.HttpContext.Request != null)
            {
                var request = _httpContextAccessor.HttpContext.Request;
                return string.Format("{0}://{1}", request.Scheme, request.Host.ToString());
            }

            return null;
        }

        public string GetUrl(string routeName, object routeValues = null)
        {
            if (_httpContextAccessor != null 
                && _httpContextAccessor.HttpContext != null 
                    && _httpContextAccessor.HttpContext.Request != null)
            {
                var request = _httpContextAccessor.HttpContext.Request;
                return _urlHelper.RouteUrl(routeName, routeValues, request.Scheme, request.Host.ToString());
            }

            return null;
        }

    }
}
