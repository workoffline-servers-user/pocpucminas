﻿using PocPucMinas.AspNetCore.Services.Interfaces;
using PocPucMinas.AspNetCore.Clients;
using Microsoft.AspNetCore.Http;

namespace PocPucMinas.AspNetCore.Services.Implementations
{
    public class ApiClientFactory : IApiClientFactory
    {
        private IHttpContextAccessor _httpContextAccessor;
        public ApiClientFactory(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public ApiClient Factory(string baseUrl, bool enviarAccessTokenAtualSeDisponivel=true)
        {
            if(enviarAccessTokenAtualSeDisponivel)
            {
                var accessToken = _httpContextAccessor.HttpContext.Request.Headers["Authorization"];
                if(!string.IsNullOrEmpty(accessToken))
                {
                    return new ApiClient(baseUrl, accessToken);
                }
            }

            return new ApiClient(baseUrl);
        }

    }
}
