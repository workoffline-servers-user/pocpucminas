﻿using PocPucMinas.AspNetCore.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace PocPucMinas.AspNetCore.Services.Implementations
{
    public class ApiWarmer : IApiWarmer, IDisposable
    {
        private readonly HttpClient HttpClient;
        private readonly IServiceProvider ServiceProvider;
        private readonly IServiceCollection Services;

        public ApiWarmer(IServiceCollection services, IServiceProvider serviceProvider)
        {
            HttpClient = new HttpClient();
            Services = services;
            ServiceProvider = serviceProvider;
        }

        public async Task RealizaPreAquecimento(IUrlHelper urlHelper, HttpRequest request, params string[] ignoredControllers)
        {
            //faz a chamada para todos os serviços Singleton da Aplicação
            RealizaPreCargaDosServices();

            //Faz as chamadas aos métodos GET da aplicação
            await RealizaPreCargaDosMetodos(urlHelper, request, ignoredControllers);
        }

        private void RealizaPreCargaDosServices()
        {
            var services = Services
                //.Where(descriptor => descriptor.Lifetime == ServiceLifetime.Singleton)
                .Where(descriptor => descriptor.ImplementationType != typeof(ApiWarmer))
                .Where(descriptor => !descriptor.ServiceType.Namespace.StartsWith("Microsoft."))
                .Select(descriptor => descriptor.ServiceType)
                .Distinct();

            foreach(var singletonService in services)
            {
                try
                {
                    ServiceProvider.GetService(singletonService);
                }
                catch
                {

                }                
            }
        }

        private async Task RealizaPreCargaDosMetodos(IUrlHelper urlHelper, HttpRequest request, params string[] ignoredControllers)
        {
            var controllerTypes =
                Assembly.GetEntryAssembly()
                    .GetTypes()
                    .Where(t => t.GetTypeInfo().IsClass)
                    .Where(t => typeof(Controller).IsAssignableFrom(t))
                    .Where(t => !ignoredControllers.Any(i => t.Name.ToLower().StartsWith(i.ToLower())))
                    .ToList();

            if (controllerTypes != null && controllerTypes.Count > 0)
            {
                var getEndpoints =
                controllerTypes
                    .SelectMany(c => c.GetMethods()
                        .Where(m => m.IsPublic && m.CustomAttributes.Any(a => a.AttributeType == typeof(HttpGetAttribute))));

                foreach (var method in getEndpoints)
                {
                    try
                    {
                        var routeValues = new RouteValueDictionary();

                        method.GetParameters()
                            .ToDictionary(
                                p => p.Name,
                                p => GetDefault(p.ParameterType))
                            .ToList()
                            .ForEach(rv => routeValues.Add(rv.Key, rv.Value));

                        var url = GetFullUrl(
                            request,
                            urlHelper.Action(
                                method.Name,
                                GetControllerName(method.DeclaringType.Name),
                                routeValues
                        ));

                        await HttpClient.GetAsync(url);
                    }
                    catch
                    {

                    }
                }
            }
        }

        private string GetControllerName(string className) =>
            className.EndsWith("Controller") ? className.Substring(0, className.Length - "Controller".Length) : className;

        public static object GetDefault(Type type) =>
            type == typeof(string) ?
                "foo" : type.GetTypeInfo().IsValueType ?
                Activator.CreateInstance(type) : null;

        private string GetFullUrl(HttpRequest request, string relativeUrl) =>
            $"{request.Scheme}://{request.Host}{relativeUrl}";

        public void Dispose()
        {
            HttpClient?.Dispose();
        }
    }
}
