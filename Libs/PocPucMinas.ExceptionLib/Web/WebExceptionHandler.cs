﻿using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Logging;
using System;
using System.Diagnostics;
using System.Reflection;

namespace PocPucMinas.ExceptionLib.Web
{
    public static class WebExceptionHandler
    {
        public static void HandleException(HttpContext context, ILoggerFactory loggerFactory)
        {
            var errorFeature = context.Features.Get<IExceptionHandlerFeature>();
            var exception = errorFeature.Error;

            // the IsTrusted() extension method doesn't exist and
            // you should implement your own as you may want to interpret it differently
            // i.e. based on the current principal

            var problemDetails = new ProblemDetails
            {
                Instance = $"urn:error:{Guid.NewGuid()}"
            };

            if (exception is BadHttpRequestException badHttpRequestException)
            {
                problemDetails.Title = "Requisição inválida.";
                problemDetails.Status = (int)typeof(BadHttpRequestException).GetProperty("StatusCode",
                    BindingFlags.NonPublic | BindingFlags.Instance).GetValue(badHttpRequestException);
                problemDetails.Detail = badHttpRequestException.Message;
            }
            else
            {
                problemDetails.Title = "Ocorreu um erro inesperado!";
                problemDetails.Status = 500;
                problemDetails.Detail = exception.Demystify().ToString();
            }

            // log the exception etc..

            context.Response.StatusCode = problemDetails.Status.Value;
            context.Response.WriteJson(problemDetails, "application/problem+json");
        }

    }
}
