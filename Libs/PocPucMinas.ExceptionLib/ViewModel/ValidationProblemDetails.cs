﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace PocPucMinas.ExceptionLib.ViewModel
{
    public class ValidationProblemDetails : ProblemDetails
    {
        public ICollection<ValidationError> ValidationErrors { get; set; }
    }
}