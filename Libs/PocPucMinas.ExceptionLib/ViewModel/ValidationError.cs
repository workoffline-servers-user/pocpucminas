﻿namespace PocPucMinas.ExceptionLib.ViewModel
{
    public class ValidationError
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
