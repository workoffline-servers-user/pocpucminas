﻿using PocPucMinas.CommonLib.Helpers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace PocPucMinas.CommonLib.Extensions
{
    public static class StringDateTimeExtensions
    {
        public static DateTime? ToDateObject(this string strDate, string format = DateTimeHelper.DATE_FORMAT_FOR_DTO)
        {
            var dateTimeObj = default(DateTime);
            DateTime.TryParseExact(strDate, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTimeObj);

            return dateTimeObj;
        }

        public static DateTime ToDateTimeObject(this string datetimeString, string format = DateTimeHelper.DATETIME_FORMAT_FOR_DTO)
        {
            var retorno = default(DateTime);
            DateTime.TryParseExact(datetimeString, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out retorno);
            return retorno;
        }
    }
}
