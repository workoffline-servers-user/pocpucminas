using PocPucMinas.CommonLib.Helpers;
using PocPucMinas.CommonLib.Support;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace PocPucMinas.CommonLib.Extensions
{
    public static class StringExtensions
    {
        public static string ToSnakeCase(this string input)
        {
          if (string.IsNullOrEmpty(input))
          {
            return input;
          }

          var startUnderscores = Regex.Match(input, @"^_+");
          return startUnderscores + Regex.Replace(input, @"([a-z0-9])([A-Z])", "$1_$2").ToLower();
        }

        

        public static string Reverse(this string s)
        {
            char[] charArray = s.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }

        public static List<string> TokenizeString(this string inputString)
        {
            if (!string.IsNullOrWhiteSpace(inputString))
            {
                var re = new Regex("(?<=\")[^\"]*(?=\")|[^\" ]+");
                return re.Matches(inputString).Cast<Match>().Select(m => m.Value).ToList();
            }
            else
            {
                return default;
            }
        }

    }
}
