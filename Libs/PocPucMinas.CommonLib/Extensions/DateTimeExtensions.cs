using PocPucMinas.CommonLib.Helpers;
using PocPucMinas.CommonLib.Support;
using System;

namespace PocPucMinas.CommonLib.Extensions
{
    public static class DateTimeExtensions
    {
        public static string ToDtoDateFormatString(this DateTime datetime)
        {
            return datetime.ToString(DateTimeHelper.DATE_FORMAT_FOR_DTO);
        }

        public static string ToDtoDateTimeFormatString(this DateTime datetime)
        {
            return datetime.ToString(DateTimeHelper.DATETIME_FORMAT_FOR_DTO);
        }

        public static string ToFileSystemDateFormatString(this DateTime datetime)
        {
            return datetime.ToString(DateTimeHelper.DATE_FORMAT_FOR_FILESYSTEM);
        }

        public static string ToFileSystemDateTimeFormatString(this DateTime datetime)
        {
            return datetime.ToString(DateTimeHelper.DATETIME_FORMAT_FOR_FILESYSTEM);
        }

        public static string ToHumanDateFormatString(this DateTime datetime)
        {
            return datetime.ToString(DateTimeHelper.DATE_FORMAT_FOR_HUMAN);
        }

        public static string ToHumanDateTimeFormatString(this DateTime datetime)
        {
            return datetime.ToString(DateTimeHelper.DATETIME_FORMAT_FOR_HUMAN);
        }
    }
}
