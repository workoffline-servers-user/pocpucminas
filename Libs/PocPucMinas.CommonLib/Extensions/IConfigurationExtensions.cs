﻿using Microsoft.Extensions.Configuration;
using PocPucMinas.CommonLib.Support;
using System;
using System.Collections.Generic;
using System.Text;

namespace PocPucMinas.CommonLib.Extensions
{
    public static class IConfigurationExtensions
    {
        public static bool IsCarregamentoAppSettingsRemotoAtivo(this IConfiguration configuration)
        {
            try
            {
                return configuration.GetValue<bool>("IsCarregamentoAppSettingsRemotoAtivo");
            }
            catch
            {
                return false;
            }
        }

        public static string GetAmbienteRedis(this IConfiguration configuration)
        {
            return configuration.GetValue<string>("IdentificadorAmbienteRedis");
        }

        public static string GetAmbienteExecucao(this IConfiguration configuration)
        {
            return configuration.GetValue<string>("IdentificadorAmbiente");
        }

        public static TimeSpan? GetPausaParaInstanciacaoDoBancoDeDados(this IConfiguration configuration)
        {
            var pausaEmSegundos = configuration.GetValue<int?>("PausaParaInstanciacalDoBancoDeDadosEmSegundos");
            if(pausaEmSegundos.HasValue && pausaEmSegundos != 0)
            {
                return TimeSpan.FromSeconds(pausaEmSegundos.Value);
            }
            else
            {
                return null;
            }
        }

        public static bool IsAmbienteDesenvolvimento(this IConfiguration configuration)
        {
            return configuration.GetValue<string>("IdentificadorAmbiente") == Constants.Ambiente.DESENVOLVIMENTO;
        }

        public static bool IsAmbienteProducao(this IConfiguration configuration)
        {
            return configuration.GetValue<string>("IdentificadorAmbiente") == Constants.Ambiente.PRODUCAO;
        }

        public static bool IsAmbienteHomologacao(this IConfiguration configuration)
        {
            return configuration.GetValue<string>("IdentificadorAmbiente") == Constants.Ambiente.HOMOLOGACAO;
        }

        public static bool IsAmbienteTeste(this IConfiguration configuration)
        {
            return configuration.GetValue<string>("IdentificadorAmbiente") == Constants.Ambiente.TESTE;
        }

        public static string GetIdentificadorServidor(this IConfiguration configuration)
        {
            return Environment.MachineName;
        }

        public static string GetIdentificadorAplicacao(this IConfiguration configuration)
        {
            return configuration.GetValue<string>("IdentificadorAplicacao");
        }

        public static string GetVersaoAplicacao(this IConfiguration configuration)
        {
            return configuration.GetValue<string>("VersaoAplicacao");
        }

    }
}
