﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Text;

namespace PocPucMinas.CommonLib.Support
{
    public static class JsonSerialization
    {
        public static JsonSerializerSettings GetDefaultSerializationSettings()
        {
            return new JsonSerializerSettings()
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,

                //Formato: 2012-07-19T14:30:00+09:30
                DateFormatHandling = DateFormatHandling.IsoDateFormat,
                DateParseHandling = DateParseHandling.DateTime,
                DateTimeZoneHandling = DateTimeZoneHandling.RoundtripKind
            };
        }

        public static void SetDefaultSerializationSettings()
        {
            JsonConvert.DefaultSettings = () => GetDefaultSerializationSettings();
        }

    }

}
