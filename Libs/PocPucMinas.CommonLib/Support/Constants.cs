namespace PocPucMinas.CommonLib.Support
{
    public static class Constants
    {
        public const short INTERVALO_EM_MINUTOS_ATUALIZACAO_AUTOMATICA_CONFIGURACOES_REMOTAS = 30;

        public static class Ambiente
        {
            public const string DESENVOLVIMENTO = "Desenvolvimento";
            public const string HOMOLOGACAO = "Homologacao";
            public const string PRODUCAO = "Producao";
            public const string TESTE = "Teste";
        }

    }
}
