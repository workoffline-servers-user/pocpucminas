using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace PocPucMinas.CommonLib.Clients
{
    /// <summary>
    /// Instancia cliente para consumir servi�os ASP.NET Core WebAPI
    /// </summary>
    public class AspNetCoreWebApiClient : IDisposable
    {
        public const long MAX_RESPONSE_CONTENT_BUFFER_DEFAULT_SIZE = 2147483647;

        protected HttpClient Client;
        private IEnumerable<MediaTypeFormatter> InputFormatters;
        private readonly IEnumerable<MediaTypeFormatter> OutputFormatters;

        public AspNetCoreWebApiClient(string baseAddress, string accessToken, TimeSpan timeout, long maxResponseContentBufferSize)
        {
            Client = new HttpClient
            {
                BaseAddress = new Uri(baseAddress)
            };

            Client.DefaultRequestHeaders.Accept.Clear();
            Client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            if(!string.IsNullOrEmpty(accessToken))
            {
                Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            }

            Client.Timeout = timeout;
            Client.MaxResponseContentBufferSize = maxResponseContentBufferSize; // 2 GB

            var jsonFormatter = new JsonMediaTypeFormatter();
            jsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            InputFormatters = new List<MediaTypeFormatter>
            {
                jsonFormatter
            };

            OutputFormatters = new List<MediaTypeFormatter>
            {
                jsonFormatter
            };

        }

        public AspNetCoreWebApiClient(string baseAddress, string accessToken, TimeSpan timeout) 
            : this(baseAddress, accessToken, timeout, MAX_RESPONSE_CONTENT_BUFFER_DEFAULT_SIZE)
        {

        }

        /// <summary>
        /// Construtor padr�o com autoriza��o
        /// Aceita servi�os que respondem o requests em JSON (application/json)
        /// </summary>
        public AspNetCoreWebApiClient(string baseAddress, string accessToken)
            : this(baseAddress, accessToken, TimeSpan.FromMinutes(10))
        {

        }

        /// <summary>
        /// Construtor padr�o
        /// Aceita servi�os que respondem o requests em JSON (application/json)
        /// </summary>
        public AspNetCoreWebApiClient(string baseAddress)
            : this(baseAddress, string.Empty, TimeSpan.FromMinutes(10))
        {
            
        }

        /// <summary>
        /// M�todo respons�vel por realizar requisi��es utilizando verbo HTTP GET
        /// </summary>
        /// <typeparam name="OutputType">Tipo de dados de sa�da</typeparam>
        /// <param name="path">URL do request, com par�metros</param>
        /// <returns>Objeto do tipo definido pelo par�metro de tipo OutputType</returns>
        public async Task<OutputType> Get<OutputType>(string path, bool exceptionSafe=false)
        {
            HttpResponseMessage response = await Client.GetAsync(path);
            if (response.IsSuccessStatusCode)
            {
                var responseContent = await response.Content.ReadAsAsync<OutputType>(OutputFormatters);
                return responseContent;
            }

            if(!exceptionSafe)
            {
                response.EnsureSuccessStatusCode();
            }
            
            return default;
        }

        /// <summary>
        /// M�todo respons�vel por realizar requisi��es utilizando verbo HTTP POST
        /// </summary>
        /// <typeparam name="OutputType">Tipo de dados de sa�da</typeparam>
        /// <typeparam name="InputType">Tipo de dados de entrada</typeparam>
        /// <param name="path"></param>
        /// <param name="objetoInput">URL do request, com par�metros</param>
        /// <returns>Objeto do tipo definido pelo par�metro de tipo OutputType</returns>
        public async Task<OutputType> Post<OutputType, InputType>(string path, InputType objetoInput, bool exceptionSafe = false)
        {
            HttpResponseMessage response = await Client.PostAsync<InputType>(path, objetoInput, InputFormatters.FirstOrDefault());
            if (response.IsSuccessStatusCode)
            {
                var responseContent = await response.Content.ReadAsAsync<OutputType>(OutputFormatters);
                return responseContent;
            }

            if (!exceptionSafe)
            {
                response.EnsureSuccessStatusCode();
            }

            return default;
        }

        /// <summary>
        /// M�todo respons�vel por realizar requisi��es utilizando verbo HTTP PUT
        /// </summary>
        /// <typeparam name="OutputType">Tipo de dados de sa�da</typeparam>
        /// <typeparam name="InputType">Tipo de dados de entrada</typeparam>
        /// <param name="path"></param>
        /// <param name="objetoInput">URL do request, com par�metros</param>
        /// <returns>Objeto do tipo definido pelo par�metro de tipo OutputType</returns>
        public async Task<OutputType> Put<OutputType, InputType>(string path, InputType objetoInput, bool exceptionSafe = false)
        {
            HttpResponseMessage response = await Client.PutAsync<InputType>(path, objetoInput, InputFormatters.FirstOrDefault());
            if (response.IsSuccessStatusCode)
            {
                var responseContent = await response.Content.ReadAsAsync<OutputType>(OutputFormatters);
                return responseContent;
            }

            if (!exceptionSafe)
            {
                response.EnsureSuccessStatusCode();
            }

            return default;
        }

        /// <summary>
        /// M�todo respons�vel por realizar requisi��es utilizando verbo HTTP DELETE
        /// </summary>
        /// <typeparam name="OutputType">Tipo de dados de sa�da</typeparam>
        /// <param name="path">URL do request, com par�metros</param>
        /// <returns>Objeto do tipo definido pelo par�metro de tipo OutputType</returns>
        public async Task<OutputType> Delete<OutputType>(string path, bool exceptionSafe = false)
        {
            HttpResponseMessage response = await Client.DeleteAsync(path);
            if (response.IsSuccessStatusCode)
            {
                var responseContent = await response.Content.ReadAsAsync<OutputType>(OutputFormatters);
                return responseContent;
            }

            if (!exceptionSafe)
            {
                response.EnsureSuccessStatusCode();
            }

            return default;
        }

        public void Dispose()
        {
            ((IDisposable)Client).Dispose();
        }

        /// <summary>
        /// Destrutor padr�o
        /// Cancela todas as conex�es pendentes
        /// </summary>
        ~AspNetCoreWebApiClient()
        {
            if (Client != default(HttpClient))
            {
                try
                {
                    Client.CancelPendingRequests();
                }
                catch (Exception)
                {

                }
            }
        }
    }
}
