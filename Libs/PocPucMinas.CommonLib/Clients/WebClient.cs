﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace PocPucMinas.CommonLib.Clients
{
    /// <summary>
    /// Classe Wrapper responsável por fazer requisições genéricas a recursos HTTP/HTTPS
    /// </summary>
    public class WebClient : IDisposable
    {
        public HttpWebRequest Request { get; private set; }
        public bool IsValidResponse { get; private set; }
        private HttpWebResponse Response;

        /// <summary>
        /// Construtor padrão do WebClient
        /// </summary>
        /// <param name="url">URL do recurso a ser baixado</param>
        public WebClient(string url, bool automaticRequest = true)
        {
            Request = (HttpWebRequest)WebRequest.Create(url);

            // Set some reasonable limits on resources used by this request
            Request.MaximumAutomaticRedirections = 4;
            Request.MaximumResponseHeadersLength = 4;

            // Set credentials to use for this request.
            Request.Credentials = CredentialCache.DefaultCredentials;          
            
            if(automaticRequest)
            {
                DoRequest();
            }
        }

        public HttpStatusCode GetOnlyStatusCode()
        {
            Request.Method = "HEAD";
            DoRequest();

            return Response.StatusCode;
        }

        public void DoRequest()
        {
            try
            {
                Response = (HttpWebResponse)Request.GetResponse();
                IsValidResponse = true;
            }
            catch (WebException e)
            {
                if (e.Response == null)
                {
                    throw e;
                }

                Response = (HttpWebResponse) e.Response;
                IsValidResponse = false;
            }

        }

        /// <summary>
        /// Método que retorna o Content-Length da Resposta
        /// </summary>
        /// <returns>Content-Length da Resposta (em bytes)</returns>
        public long? GetResponseContentLength()
        {
            if (Response == default(HttpWebResponse))
            {
                return null;
            }
            else
            {
                return Response.ContentLength;
            }            
        }

        /// <summary>
        /// Retorna o Content-Type da Resposta
        /// </summary>
        /// <returns>Content-Type da Resposta (String)</returns>
        public string GetResponseContentType()
        {
            if (Response == default(HttpWebResponse))
            {
                return null;
            }
            else
            {
                return Response.ContentType;
            }
        }

        /// <summary>
        /// Metódo que retorna o Conteúdo da Resposta como String codificada em UTF-8
        /// </summary>
        /// <returns>Conteúdo da Resposta como String codificada em UTF-8</returns>
        public Task<string> GetContentAsUTF8String()
        {
            return GetContentAsString(Encoding.UTF8);
        }

        public Task<string> GetContentAsAnsiString()
        {
            return GetContentAsString(Encoding.Default);
        }

        /// <summary>
        /// Método que retorna o conteúdo da Resposta em String
        /// </summary>
        /// <param name="encoding">Codificação de caracteres utilizada pelo StreamReader para ler o conteúdo da resposta.</param>
        /// <returns>Conteúdo da Resposta (em String)</returns>
        public async Task<string> GetContentAsString(Encoding encoding)
        {
            if(Response == default(HttpWebResponse))
            {
                DoRequest();
            }
            
            var receiveStream = Response.GetResponseStream();
            var readStream = new StreamReader(receiveStream, encoding);

            return await readStream.ReadToEndAsync();

        }

        #region IDisposable Support
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    Response.Dispose();
                }

                Request = null;
                disposedValue = true;
            }
        }

        // TODO: substituir um finalizador somente se Dispose(bool disposing) acima tiver o código para liberar recursos não gerenciados.
        // ~WebClient() {
        //   // Não altere este código. Coloque o código de limpeza em Dispose(bool disposing) acima.
        //   Dispose(false);
        // }

        // Código adicionado para implementar corretamente o padrão descartável.
        public void Dispose()
        {
            // Não altere este código. Coloque o código de limpeza em Dispose(bool disposing) acima.
            Dispose(true);
            // TODO: remover marca de comentário da linha a seguir se o finalizador for substituído acima.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
