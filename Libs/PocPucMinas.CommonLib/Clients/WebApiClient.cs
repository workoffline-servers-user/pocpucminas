using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace PocPucMinas.CommonLib.Clients
{
    /// <summary>
    /// Instancia cliente para consumir servi�os ASP.NET WebAPI
    /// </summary>
    public class WebApiClient : IDisposable
    {
        protected HttpClient Client;
        private readonly IEnumerable<MediaTypeFormatter> InputFormatters;
        private readonly IEnumerable<MediaTypeFormatter> OutputFormatters;

        /// <summary>
        /// Construtor padr�o
        /// Aceita servi�os que respondem o requests em JSON (application/json)
        /// </summary>
        public WebApiClient()
        {
            Client = new HttpClient();

            Client.DefaultRequestHeaders.Accept.Clear();
            Client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            Client.Timeout = TimeSpan.FromMinutes(10);
            Client.MaxResponseContentBufferSize = 2147483647; // 2 GB

            var jsonFormatter = new JsonMediaTypeFormatter();
            jsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            
            InputFormatters = new List<MediaTypeFormatter>
            {
                jsonFormatter
            };

            OutputFormatters = new List<MediaTypeFormatter>
            {
                jsonFormatter
            };
        }

        /// <summary>
        /// M�todo respons�vel por realizar requisi��es utilizando verbo HTTP GET
        /// </summary>
        /// <typeparam name="OutputType">Tipo de dados de sa�da</typeparam>
        /// <param name="path">URL do request, com par�metros</param>
        /// <returns>Objeto do tipo definido pelo par�metro de tipo OutputType</returns>
        public async Task<OutputType> Get<OutputType>(string path)
        {
            HttpResponseMessage response = await Client.GetAsync(path);
            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsAsync<OutputType>(OutputFormatters);
            }
            return default;
        }

        /// <summary>
        /// M�todo respons�vel por realizar requisi��es utilizando verbo HTTP POST
        /// </summary>
        /// <typeparam name="OutputType">Tipo de dados de sa�da</typeparam>
        /// <typeparam name="InputType">Tipo de dados de entrada</typeparam>
        /// <param name="path"></param>
        /// <param name="objetoInput">URL do request, com par�metros</param>
        /// <returns>Objeto do tipo definido pelo par�metro de tipo OutputType</returns>
        public async Task<OutputType> Post<OutputType, InputType>(string path, InputType objetoInput)
        {
            HttpResponseMessage response = await Client.PostAsync<InputType>(path, objetoInput, InputFormatters.FirstOrDefault());
            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsAsync<OutputType>(OutputFormatters);
            }
            return default;
        }

        /// <summary>
        /// M�todo respons�vel por realizar requisi��es utilizando verbo HTTP PUT
        /// </summary>
        /// <typeparam name="OutputType">Tipo de dados de sa�da</typeparam>
        /// <typeparam name="InputType">Tipo de dados de entrada</typeparam>
        /// <param name="path"></param>
        /// <param name="objetoInput">URL do request, com par�metros</param>
        /// <returns>Objeto do tipo definido pelo par�metro de tipo OutputType</returns>
        public async Task<OutputType> Put<OutputType, InputType>(string path, InputType objetoInput)
        {
            HttpResponseMessage response = await Client.PutAsync<InputType>(path, objetoInput, InputFormatters.FirstOrDefault());
            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsAsync<OutputType>(OutputFormatters);
            }
            return default;
        }

        /// <summary>
        /// M�todo respons�vel por realizar requisi��es utilizando verbo HTTP DELETE
        /// </summary>
        /// <typeparam name="OutputType">Tipo de dados de sa�da</typeparam>
        /// <param name="path">URL do request, com par�metros</param>
        /// <returns>Objeto do tipo definido pelo par�metro de tipo OutputType</returns>
        public async Task<OutputType> Delete<OutputType>(string path)
        {
            HttpResponseMessage response = await Client.DeleteAsync(path);
            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsAsync<OutputType>(OutputFormatters);
            }
            return default;
        }

        public void Dispose()
        {
            ((IDisposable)Client).Dispose();
        }

        /// <summary>
        /// Destrutor padr�o
        /// Cancela todas as conex�es pendentes
        /// </summary>
        ~WebApiClient()
        {
            if(Client != default(HttpClient))
            {
                try
                {
                    Client.CancelPendingRequests();
                }
                catch(Exception)
                {

                }
            }
        }
    }
}
