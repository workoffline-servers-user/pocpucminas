using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PocPucMinas.CommonLib.Services.Implementations;
using PocPucMinas.CommonLib.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace PocPucMinas.CommonLib.DependencyInjection
{
    public static class IServiceCollectionExtensions
    {
        private static IConfiguration Configuration = null;

        public static IServiceCollection AdicionaAppSettingsLoader(this IServiceCollection services)
        {
            services.AddSingleton<IAppSettingsLoaderService, AppSettingsLoaderService>();

            //invoca o loader
            var appSettingsLoader = services.BuildServiceProvider().GetRequiredService<IAppSettingsLoaderService>();
            var configuration = appSettingsLoader.GetConfiguration();

            if (configuration != default(IConfiguration))
            {
                Configuration = configuration;
            }

            return services;
        }

        public static IConfiguration GetConfiguration(this IServiceCollection services)
        {
            return Configuration;
        }

    }

}
