﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PocPucMinas.CommonLib.Services.Interfaces
{
    public interface IEncryptionKey
    {
        string GetKey();
    }
}
