﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace PocPucMinas.CommonLib.Services.Interfaces
{
    public interface IAppSettingsLoaderService
    {
        void ClearConfiguration();
        IConfiguration GetConfiguration();
        Task<IConfiguration> GetConfigurationAsync();
        Task<IConfigurationBuilder> GetConfigurationBuilder(IConfigurationBuilder config = null);
        Task<IConfigurationBuilder> GetConfigurationBuilder(Dictionary<string, string> parametrosAplicacao, IConfigurationBuilder configurationBuilder = null);
        string GetIdentificadorAmbiente();
        string GetIdentificadorAmbienteCorrente();
        string GetIdentificadorAplicacao();
        string GetIdentificadorServidor();
        List<string> GetPathsArquivosCarregados();
        string GetUrlRepositorioRemoto();
        string GetVersaoAplicacao();
        void LoadConfiguration();
    }
}