﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Threading;
using System.Timers;
using PocPucMinas.CommonLib.Support;
using PocPucMinas.CommonLib.Helpers;
using Timer = System.Timers.Timer;
using PocPucMinas.CommonLib.Extensions;
using PocPucMinas.CommonLib.Services.Interfaces;

namespace PocPucMinas.CommonLib.Services.Implementations
{
    public class AppSettingsLoaderService : IAppSettingsLoaderService
    {
        private const string _chaveCriptografia = "ZMhhfyXZCzKtWzErEOCT";

        private string _identificadorAplicacao;
        private string _versaoAplicacao;

        private string _identificadorAmbiente;
        private string _identificadorServidor;

        private string _configDirPath;
        private string _localConfigurationFilepath;
        private string _remoteConfigurationFilepath;
        private string _urlRepositorioRemoto;
        private List<string> _pathsArquivosCarregados;

        private IConfigurationBuilder _configurationBuilder;
        private IConfiguration _configuration;

        private IConfiguration _localConfiguration;
        private Timer _autoRefreshTimer;

        public AppSettingsLoaderService()
        {
            _pathsArquivosCarregados = new List<string>();

            SetConfigDirPath();
            SetLocalConfigurationFilePathAndObject();

            //seta o identificador do ambiente corrente
            //a partir do appsettings.json
            SetIdentificadorAmbiente();
            
            if (_localConfiguration.IsCarregamentoAppSettingsRemotoAtivo())
            {
                SetIdentificadorServidor();
                SetIdentificadorAplicacao();
                SetVersaoAplicacao();

                SetRemoteConfigurationFilepath();
                SetUrlRepositorioRemoto();

                //dispara timer para recarregamento automático das configurações remotas
                DisparaTimerAutoRefresh();
            }
        }

        public List<string> GetPathsArquivosCarregados()
        {
            return _pathsArquivosCarregados;
        }

        private void DisparaTimerAutoRefresh()
        {
            try
            {
                var _timerInterval = default(short);

                try
                {
                    _timerInterval = _localConfiguration.GetValue<short>("IntervaloEmMinutosAtualizacaoAutomaticaConfiguracoesRemotas");
                }
                catch
                {

                }

                if (_timerInterval == default)
                {
                    _timerInterval = Constants.INTERVALO_EM_MINUTOS_ATUALIZACAO_AUTOMATICA_CONFIGURACOES_REMOTAS;
                }

                _autoRefreshTimer = new Timer(TimeSpan.FromMinutes(_timerInterval).TotalMilliseconds);

                //ação a ser tomada a cada loop do timer
                _autoRefreshTimer.Elapsed += AtualizaConteudoArquivoConfiguracaoRemoto;
                _autoRefreshTimer.AutoReset = true;
                _autoRefreshTimer.Enabled = true;
            }
            catch
            {

            }
        }

        private void AtualizaConteudoArquivoConfiguracaoRemoto(object source, ElapsedEventArgs e)
        {
            try
            {
                ComparaVersoesAndFazDownloadVersaoMaisRecenteSeNecessario(true).GetAwaiter().GetResult();
            }
            catch
            {

            }
        }

        public void ClearConfiguration()
        {
            if (_configurationBuilder != null)
            {
                _configurationBuilder.Sources.Clear();
            }

            if (_configuration != null)
            {
                _configuration = null;
            }
        }

        public async Task<IConfiguration> GetConfigurationAsync()
        {
            if (_configuration == null)
            {
                await GetConfigurationBuilder();
            }

            return _configuration;
        }

        public IConfiguration GetConfiguration()
        {
            if (_configuration == null)
            {
                GetConfigurationBuilder().GetAwaiter().GetResult();
            }

            return _configuration;
        }

        public async void LoadConfiguration()
        {
            await GetConfigurationBuilder();
        }

        public async Task<IConfigurationBuilder> GetConfigurationBuilder(IConfigurationBuilder config = null)
        {
            return await GetConfigurationBuilder(null, config);
        }

        public async Task<IConfigurationBuilder> GetConfigurationBuilder(Dictionary<string, string> parametrosAplicacao, IConfigurationBuilder configurationBuilder = null)
        {
            if (configurationBuilder == null)
            {
                if (_configurationBuilder != null && _configurationBuilder.Sources.Count > 0)
                {
                    return _configurationBuilder;
                }

                configurationBuilder = new ConfigurationBuilder();
            }

            var identificadorAmbiente = GetIdentificadorAmbienteCorrente();
            var identificadorAplicacao = GetIdentificadorAplicacao();
            var identificadorServidor = GetIdentificadorServidor();

            if (parametrosAplicacao != default)
            {
                configurationBuilder.AddInMemoryCollection(parametrosAplicacao);
            }

            if (_localConfiguration.IsCarregamentoAppSettingsRemotoAtivo())
            {
                await ComparaVersoesAndFazDownloadVersaoMaisRecenteSeNecessario();
            }

            var rootDirPath = AppContext.BaseDirectory;
            var configDirPath = Path.Combine(AppContext.BaseDirectory, "_Config");

            if (!Directory.Exists(configDirPath))
            {
                Directory.CreateDirectory(configDirPath);
            }

            configurationBuilder.SetBasePath(rootDirPath);
            var arrPathsAppSettingsAplicacao = new List<string>
            {
                Path.Combine("_Config", $"appsettings.json")
                
            };

            if (_localConfiguration.IsCarregamentoAppSettingsRemotoAtivo())
            {
                arrPathsAppSettingsAplicacao.Add(Path.Combine("_Config", $"appsettings.remoto.json"));
            }
            else
            {
                arrPathsAppSettingsAplicacao.Add(Path.Combine("_Config", $"appsettings.{_identificadorAmbiente}.json"));
            }

            foreach (var pathAppSettingsAplicacao in arrPathsAppSettingsAplicacao)
            {
                var pathArquivo = Path.Combine(rootDirPath, pathAppSettingsAplicacao);
                if (File.Exists(pathArquivo))
                {
                    _pathsArquivosCarregados.Add(pathArquivo);
                    configurationBuilder.AddJsonFile(pathAppSettingsAplicacao, optional: true, reloadOnChange: false);
                }
            }

            _configurationBuilder = configurationBuilder;
            _configuration = configurationBuilder.Build();

            //sobrescreve o valor caso tenha sido definido por variável de ambiente
            _configuration["IdentificadorAmbiente"] = _localConfiguration["IdentificadorAmbiente"];
            return configurationBuilder;
        }

        public string GetIdentificadorAmbienteCorrente()
        {
            return _identificadorAmbiente;
        }

        private void SetConfigDirPath()
        {
            var configDirPath = Path.Combine(AppContext.BaseDirectory, "_Config");
            if (!Directory.Exists(configDirPath))
            {
                throw new Exception($"Diretório {configDirPath} não existe!");
            }

            _configDirPath = configDirPath;
        }

        private void SetRemoteConfigurationFilepath()
        {
            var remoteConfigurationFilename = "appsettings.remoto.json";
            var remoteConfigurationFilepath = Path.Combine(_configDirPath, remoteConfigurationFilename);

            _remoteConfigurationFilepath = remoteConfigurationFilepath;
        }

        private void SetLocalConfigurationFilePathAndObject()
        {
            if (_localConfiguration == null)
            {
                var configurationBuilder = new ConfigurationBuilder();

                var localConfigurationFilename = "appsettings.json";
                var localConfigurationFilepath = Path.Combine(_configDirPath, localConfigurationFilename);

                if (!Directory.Exists(_configDirPath) || !File.Exists(localConfigurationFilepath))
                {
                    throw new Exception($"Arquivo {localConfigurationFilepath} não existe!");
                }

                configurationBuilder.SetBasePath(_configDirPath);
                configurationBuilder.AddJsonFile(localConfigurationFilename);

                _localConfigurationFilepath = localConfigurationFilepath;
                _localConfiguration = configurationBuilder.Build();
            }

        }
        
        private void SetIdentificadorAmbiente()
        {
            var nomeVariavelDeAmbiente = "IDENTIFICADOR_AMBIENTE";
            var identificadorAmbiente = Environment.GetEnvironmentVariable(nomeVariavelDeAmbiente);

            var nomeParametroArquivoAppSettings = "IdentificadorAmbiente";
            if (!AppSettingsHelper.IsAmbienteDeExecucaoValido(identificadorAmbiente))
            {
                identificadorAmbiente = _localConfiguration.GetValue<string>(nomeParametroArquivoAppSettings);

                if (string.IsNullOrEmpty(identificadorAmbiente))
                {
                    throw new Exception($"Identificador do ambiente não informado através do parâmetro {nomeParametroArquivoAppSettings} no arquivo {_localConfigurationFilepath}");
                }
                else
                {
                    if (!AppSettingsHelper.IsAmbienteDeExecucaoValido(identificadorAmbiente))
                    {
                        throw new Exception($"Identificador do ambiente com valor inválido: {_identificadorAmbiente}");
                    }
                    else
                    {
                        Console.WriteLine($"Ambiente de execução {identificadorAmbiente} definido a partir da do parâmetro {nomeParametroArquivoAppSettings} no arquivo appsettings.json");
                    }
                }
            }
            else
            {
                _localConfiguration["IdentificadorAmbiente"] = identificadorAmbiente;
                Console.WriteLine($"Ambiente de execução {identificadorAmbiente} definido a partir da variável de ambiente {nomeVariavelDeAmbiente}");
            }

            _identificadorAmbiente = identificadorAmbiente;
        }

        private void SetIdentificadorAplicacao()
        {
            var nomeParametro = "IdentificadorAplicacao";
            var identificadorAplicacao = _localConfiguration.GetValue<string>(nomeParametro);

            if (string.IsNullOrEmpty(identificadorAplicacao))
            {
                throw new Exception($"Identificador da aplicação não informado através do parâmetro {nomeParametro} no arquivo {_localConfigurationFilepath}");
            }

            _identificadorAplicacao = identificadorAplicacao;
        }

        private void SetVersaoAplicacao()
        {
            var nomeParametro = "VersaoAplicacao";
            var versaoAplicacao = _localConfiguration.GetValue<string>(nomeParametro);

            if (string.IsNullOrEmpty(versaoAplicacao))
            {
                throw new Exception($"Versão da aplicação não informada através do parâmetro {nomeParametro} no arquivo {_localConfigurationFilepath}");
            }

            _versaoAplicacao = versaoAplicacao;
        }

        private void SetIdentificadorServidor()
        {
            _identificadorServidor = Environment.MachineName;
        }

        private void SetUrlRepositorioRemoto()
        {
            var nomeParametro = "UrlRepositorioConfiguracoes";
            var urlRepositorio = _localConfiguration.GetValue<string>(nomeParametro);

            if (string.IsNullOrEmpty(urlRepositorio))
            {
                throw new Exception($"Url do repositório de configurações não informada através do parâmetro {nomeParametro} no arquivo {_localConfigurationFilepath}");
            }

            _urlRepositorioRemoto = urlRepositorio;
        }

        public string GetIdentificadorAmbiente()
        {
            return _identificadorAmbiente;
        }

        public string GetIdentificadorAplicacao()
        {
            return _identificadorAplicacao;
        }

        public string GetVersaoAplicacao()
        {
            return _versaoAplicacao;
        }

        public string GetIdentificadorServidor()
        {
            return _identificadorServidor;
        }

        public string GetUrlRepositorioRemoto()
        {
            return _urlRepositorioRemoto;
        }

        private string GetVersaoArquivoConfiguracoesRemotoJaCarregado()
        {
            var filePath = _remoteConfigurationFilepath;

            if (File.Exists(filePath))
            {
                var dataHoraVersao = File.GetCreationTimeUtc(filePath);
                dataHoraVersao = dataHoraVersao.AddSeconds(59 - dataHoraVersao.Second);

                var dataHoraString = AppSettingsHelper.GetVersaoFromDataHora(dataHoraVersao);
                return dataHoraString;
            }

            return null;
        }

        private DateTime? GetDateTimeUtcFromVersao(string versao)
        {
            if (string.IsNullOrEmpty(versao))
            {
                return null;
            }

            var partes = versao.Split('.');
            if (partes.Length != 6)
            {
                return null;
            }

            try
            {
                var ano = Convert.ToInt16(partes[0]);
                var mes = Convert.ToInt16(partes[1]);
                var dia = Convert.ToInt16(partes[2]);
                var hora = Convert.ToInt16(partes[3]);
                var minuto = Convert.ToInt16(partes[4]);
                var segundo = Convert.ToInt16(partes[5]);

                return new DateTime(ano, mes, dia, hora, minuto, segundo, DateTimeKind.Utc);
            }
            catch
            {
                return null;
            }

        }



        private async Task ComparaVersoesAndFazDownloadVersaoMaisRecenteSeNecessario(bool exceptionSafe = false)
        {
            var versaoLocal = GetVersaoArquivoConfiguracoesRemotoJaCarregado();
            var versaoRemota = string.Empty;

            try
            {
                versaoRemota = await GetVersaoArquivoConfiguracoesRemotoMaisRecente(exceptionSafe);
            }
            catch (Exception ex)
            {
                if (!exceptionSafe)
                {
                    throw new Exception("Falha ao baixar arquivo de configurações remotas", ex);
                }
            }

            if (string.IsNullOrEmpty(versaoLocal) && string.IsNullOrEmpty(versaoRemota))
            {
                throw new Exception("Arquivo de configurações remotas não encontrado, e não foi possível baixar o identificador da versão mais recente.");
            }

            //caso não tinha sido possível o download da última versão (remota), retorna
            if (string.IsNullOrEmpty(versaoRemota))
            {
                return;
            }
            //caso a versão local não existia, ou seja inferior à última versão (remota), faz o download
            else if (string.IsNullOrEmpty(versaoLocal) || ComparaVersoes(versaoLocal, versaoRemota) < 0)
            {
                var conteudoJson = string.Empty;
                try
                {
                    conteudoJson = await GetConteudoArquivoConfiguracoesRemotoMaisRecente(exceptionSafe);
                }
                catch (Exception ex)
                {
                    if (!exceptionSafe)
                    {
                        throw ex;
                    }
                }

                if (string.IsNullOrEmpty(versaoLocal) && conteudoJson == null)
                {
                    throw new Exception("Arquivo de configurações remotas não encontrado, e não foi possível baixar o conteúdo da versão mais recente.");
                }

                if (conteudoJson != null)
                {
                    try
                    {
                        var json = JObject.Parse(conteudoJson);
                    }
                    catch (JsonReaderException)
                    {
                        if (string.IsNullOrEmpty(versaoLocal))
                        {
                            throw new Exception("Arquivo de configurações remotas não encontrado, e o conteúdo da versão mais recente está corrompido.");
                        }
                    }

                    //grava o json remoto em disco
                    await File.WriteAllTextAsync(_remoteConfigurationFilepath, conteudoJson);

                    var datetimeUtcVersaoMaisRecente = GetDateTimeUtcFromVersao(versaoRemota);

                    if (datetimeUtcVersaoMaisRecente.HasValue)
                    {
                        //define a data de criação do arquivo (UTC)
                        File.SetCreationTimeUtc(_remoteConfigurationFilepath, datetimeUtcVersaoMaisRecente.Value);
                    }

                }

            }

        }

        private async Task<string> GetVersaoArquivoConfiguracoesRemotoMaisRecente(bool exceptionSafe = false)
        {
            try
            {
                var uri = new Uri($"{_urlRepositorioRemoto}/api/AppSettings/GetIdentificadorVersaoMaisRecente/?identificadorAplicacao={_identificadorAplicacao}&versaoAplicacao={_versaoAplicacao}&identificadorAmbiente={_identificadorAmbiente}");
                var conteudoVersao = await GetConteudoRemotoAsync(uri, TimeSpan.FromSeconds(20));

                return conteudoVersao;
            }
            catch (Exception ex)
            {
                if (!exceptionSafe)
                {
                    throw new Exception("Falha ao baixar identificador da versão mais recente", ex);
                }
            }

            return null;
        }

        private async Task<string> GetConteudoArquivoConfiguracoesRemotoMaisRecente(bool exceptionSafe = false)
        {
            try
            {
                var uri = new Uri($"{_urlRepositorioRemoto}/api/AppSettings/GetConteudoVersaoMaisRecente/?identificadorAplicacao={_identificadorAplicacao}&versaoAplicacao={_versaoAplicacao}&identificadorAmbiente={_identificadorAmbiente}&identificadorServidor={_identificadorServidor}");
                var conteudoVersaoCritografado = await GetConteudoRemotoAsync(uri, TimeSpan.FromSeconds(60));

                if (conteudoVersaoCritografado != null)
                {
                    var arquivoJson = DescriptografaConteudo(conteudoVersaoCritografado);
                    if (arquivoJson != null)
                    {
                        return arquivoJson;
                    }
                    else
                    {
                        throw new Exception("Falha ao descriptografar conteúdo do arquivo de configurações remoto.");
                    }
                }
                else
                {
                    if (!exceptionSafe)
                    {
                        throw new Exception("Conteúdo do arquivo remoto de configuração está vazio.");
                    }
                }
            }
            catch (Exception ex)
            {
                if (!exceptionSafe)
                {
                    throw new Exception("Falha ao baixar identificador da versão mais recente", ex);
                }
            }

            return null;
        }

        private async Task<string> GetConteudoRemotoAsync(Uri uri, TimeSpan timeout, bool exceptionSafe = false)
        {
            try
            {
                using (HttpClient client = new HttpClient
                {
                    Timeout = timeout
                })
                using (HttpResponseMessage response = await client.GetAsync(uri))
                using (HttpContent content = response.Content)
                {
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var result = await content.ReadAsStringAsync();
                        if (result != null && result.Length >= 0)
                        {
                            return result;
                        }
                    }
                }

                return null;
            }
            catch (Exception ex)
            {
                if (exceptionSafe)
                {
                    return null;
                }
                else
                {
                    throw ex;
                }
            }

        }

        private int ComparaVersoes(string versao1, string versao2)
        {
            System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex(@"([\d]+)");
            System.Text.RegularExpressions.MatchCollection m1 = regex.Matches(versao1);
            System.Text.RegularExpressions.MatchCollection m2 = regex.Matches(versao2);
            int min = Math.Min(m1.Count, m2.Count);
            for (int i = 0; i < min; i++)
            {
                if (Convert.ToInt32(m1[i].Value) > Convert.ToInt32(m2[i].Value))
                {
                    return 1;
                }
                if (Convert.ToInt32(m1[i].Value) < Convert.ToInt32(m2[i].Value))
                {
                    return -1;
                }
            }
            return 0;
        }

        private string DescriptografaConteudo(string stringCriptografada)
        {
            return EncryptionHelper.Decrypt(stringCriptografada, _chaveCriptografia);
        }

    }

}
