﻿using PocPucMinas.CommonLib.Helpers;
using PocPucMinas.CommonLib.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace PocPucMinas.CommonLib.Services.Implementations
{
    public class EncryptionService<T> : IEncryptionService
        where T : IEncryptionKey, new()
    {
        private readonly string _encryptionPasswordString;
        public EncryptionService()
        {
            var encryptionKeyObject = new T();
            var encryptionKey = encryptionKeyObject.GetKey();
            if(string.IsNullOrEmpty(encryptionKey) || encryptionKey.Length < 10)
            {
                throw new Exception("Invalid encryption password");
            }
            _encryptionPasswordString = encryptionKey; //SHA256.Create().ComputeHash(Encoding.UTF8.GetBytes(password));            
        }

        public string Encrypt(string plainText)
        {
            return EncryptionHelper.Encrypt(plainText, _encryptionPasswordString);
        }

        public string Decrypt(string encryptedText)
        {
            return EncryptionHelper.Decrypt(encryptedText, _encryptionPasswordString);
        }

    }
}
