﻿using PocPucMinas.CommonLib.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PocPucMinas.CommonLib.Generators
{
    public static class TestDataGenerator
    {
        public static string Email(int numberOfCharacters = 10) // pass the number of characters for your email to be generated before '@'
        {
            var characters = "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz0123456789";
            var random = new Random();
            var emailAddress = new string(Enumerable.Repeat(characters, numberOfCharacters).Select(s => s[random.Next(s.Length)]).ToArray());
            emailAddress = emailAddress + "@test123.com";

            return emailAddress;
        }

        public static string Celular()
        {
            string telefone = null;
            int nums = 11;
            Random num = new Random();
            for (int i = 0; i < nums; i++)
            {
                telefone = num.Next(9) + telefone;
            }

            return long.Parse(telefone).ToString(@"(00) 00000-0000");
        }

    }
}
