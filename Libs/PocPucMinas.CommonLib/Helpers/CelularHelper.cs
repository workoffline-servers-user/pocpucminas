﻿using System;

namespace PocPucMinas.CommonLib.Helpers
{
    public static class CelularHelper
    {
        public enum TIPO_RETORNO_CHECA_TELEFONE
        {
            FORMATO_INVALIDO,
            FORMATO_DE_CELULAR_INVALIDO,
            OK
        }

        private static TIPO_RETORNO_CHECA_TELEFONE ChecaTelefone(string pNumeroTelefone)
        {

            long? vNumero = IntegerHelper.ToInt64(pNumeroTelefone);
            if (vNumero == null)
                return TIPO_RETORNO_CHECA_TELEFONE.FORMATO_INVALIDO;
            else
            {
                if (pNumeroTelefone.Length >= 10 && pNumeroTelefone.Length <= 11)
                {
                    string vStrPrimeiroDigito = pNumeroTelefone.Substring(2, 1);
                    short vPrimeiroDigito = IntegerHelper.ToInt16(vStrPrimeiroDigito).Value;

                    if (vPrimeiroDigito >= 0 && vPrimeiroDigito <= 5)
                        return TIPO_RETORNO_CHECA_TELEFONE.FORMATO_DE_CELULAR_INVALIDO;
                    else return TIPO_RETORNO_CHECA_TELEFONE.OK;

                }
                else
                {
                    return TIPO_RETORNO_CHECA_TELEFONE.FORMATO_INVALIDO;
                }
            }
        }

        /// <summary>
        /// Formatos aceitos: 
        ///     32991935402 - DD + 9 digitos
        ///     3291935402 - DD + 8 digitos
        ///     (32) 91935402 - qualquer mascara + DD + 8 digitos
        ///     (32) 991935402 - qualquer mascara + DD + 9 digitos
        /// </summary>
        /// <param name="pNumeroTelefoneCliente">A referencia é repassada pois se houver necessidade o proprio metodo irá padronizar para o padrao celular: 
        /// DD + 9 + 8 digitos - Ex.: 32991935402
        /// </param>
        /// <returns></returns>
        public static TIPO_RETORNO_CHECA_TELEFONE FormataTelefoneCelular(string pNumeroTelefoneCliente)
        {
            pNumeroTelefoneCliente = IntegerHelper.GetOnlyNumber(pNumeroTelefoneCliente);

            TIPO_RETORNO_CHECA_TELEFONE tipo = ChecaTelefone(pNumeroTelefoneCliente);
            if (tipo != TIPO_RETORNO_CHECA_TELEFONE.OK) return tipo;

            return ValidaCelularNonoDigito(ref pNumeroTelefoneCliente);
        }

        public static TIPO_RETORNO_CHECA_TELEFONE FormataTelefoneCelular(long pNumeroTelefoneCliente)
        {
            string token= pNumeroTelefoneCliente.ToString();
            TIPO_RETORNO_CHECA_TELEFONE tipo= FormataTelefoneCelular(token);
            if (tipo == TIPO_RETORNO_CHECA_TELEFONE.OK)
            {
                long x;
                if (long.TryParse(token, out x))
                {
                    pNumeroTelefoneCliente = x;
                }
            }

            return tipo;
        }

        private static TIPO_RETORNO_CHECA_TELEFONE ValidaCelularNonoDigito(ref string entradaTelefone)
        {
            var pNumeroTelefoneCliente = entradaTelefone;

            //remove o 0 utilizado para especificar o DDD
            if (pNumeroTelefoneCliente != null && pNumeroTelefoneCliente.StartsWith("0"))
                pNumeroTelefoneCliente = pNumeroTelefoneCliente.Substring(1, pNumeroTelefoneCliente.Length - 1);

            if (pNumeroTelefoneCliente.Length < 8)
                return TIPO_RETORNO_CHECA_TELEFONE.FORMATO_INVALIDO;

            string strDDD = pNumeroTelefoneCliente.Substring(0, 2);
            string celular = pNumeroTelefoneCliente.Substring(2);

            short? ddd = IntegerHelper.ToInt16(strDDD);
            if (ddd == null)
                return TIPO_RETORNO_CHECA_TELEFONE.FORMATO_INVALIDO;
            
            //usuario esqueceu de digitar no nono digito
            if (celular.Length == 8)
            {
                entradaTelefone = strDDD + "9" + celular;

            }
            return TIPO_RETORNO_CHECA_TELEFONE.OK;

        }        
           
    }

}
