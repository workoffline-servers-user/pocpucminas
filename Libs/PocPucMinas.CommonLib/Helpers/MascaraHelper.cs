﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PocPucMinas.CommonLib.Helpers
{
    public static class MascaraHelper
    {
        public static string GetValorStringSemMascara(string valorComMascara)
        {
            valorComMascara = valorComMascara ?? string.Empty;
            return new string(valorComMascara.Where(p => char.IsDigit(p)).ToArray());
        }

        public static long? GetValorInt64SemMascara(string valorComMascara)
        {
            var valorStringSemMascara = GetValorStringSemMascara(valorComMascara);
            if(!string.IsNullOrWhiteSpace(valorStringSemMascara) && 
                long.TryParse(valorStringSemMascara, out long valorLong))
            {
                return valorLong;
            }

            return null;
        }

        public static int? GetValorInt32SemMascara(string valorComMascara)
        {
            var valorStringSemMascara = GetValorStringSemMascara(valorComMascara);
            if (!string.IsNullOrWhiteSpace(valorStringSemMascara) &&
                int.TryParse(valorStringSemMascara, out int valorInt))
            {
                return valorInt;
            }

            return null;
        }

        public static short? GetValorInt16SemMascara(string valorComMascara)
        {
            var valorStringSemMascara = GetValorStringSemMascara(valorComMascara);
            if (!string.IsNullOrWhiteSpace(valorStringSemMascara) &&
                short.TryParse(valorStringSemMascara, out short valorShort))
            {
                return valorShort;
            }

            return null;
        }

        public static string GetValorComMascara(long valor, string mascara)
        {
            return valor.ToString(mascara);
        }

    }
}
