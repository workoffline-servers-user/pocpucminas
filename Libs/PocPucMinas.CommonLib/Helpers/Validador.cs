﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PocPucMinas.CommonLib.Helpers
{
    public class ValidacaoHelper 
    {
        public static bool ValidaCpf(long cpf)
        {
            return ValidaCpf(cpf.ToString());
        }

        private static bool DigitosIguais(string token)
        {
            char[] letras= token.ToCharArray();
            for (int i = 1; i < letras.Length; i++)
            {
                if (letras[0] != letras[i]) return false;
            }
            return true;
        }

        /// <summary>
        /// Nesse caso o CPF para validação 
        /// </summary>
        /// <param name="cpf"></param>
        /// <returns></returns>
        public static bool ValidaCpf(string cpf)
        {
            if (string.IsNullOrEmpty(cpf) || cpf.Equals("0")) return false;

            cpf = IntegerHelper.GetOnlyNumber(cpf);
            if (cpf.Length < 11)
                cpf = StringHelper.Lapd(11, cpf, '0');
            if (DigitosIguais(cpf)) return false;

            int[] multiplicador1 = new int[9] { 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            int[] multiplicador2 = new int[10] { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            string tempCpf;
            string digito;
            int soma;
            int resto;
            cpf = cpf.Trim();
            cpf = cpf.Replace(".", "").Replace("-", "");
            
            tempCpf = cpf.Substring(0, 9);
            soma = 0;

            for (int i = 0; i < 9; i++)
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador1[i];
            resto = soma % 11;
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            digito = resto.ToString();
            tempCpf = tempCpf + digito;
            soma = 0;
            for (int i = 0; i < 10; i++)
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador2[i];
            resto = soma % 11;
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            digito = digito + resto.ToString();
            return cpf.EndsWith(digito);
        }
        
        public static bool ValidaCnpj(string cnpj)
        {
            if (string.IsNullOrEmpty(cnpj) || cnpj.Equals("0")) return false;
            cnpj = IntegerHelper.GetOnlyNumber(cnpj);
            if (cnpj.Length < 14)
                cnpj = StringHelper.Lapd(14, cnpj, '0');
            if (DigitosIguais(cnpj)) return false;

            int[] multiplicador1 = new int[12] { 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
            int[] multiplicador2 = new int[13] { 6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
            int soma;
            int resto;
            string digito;
            string tempCnpj;
            cnpj = cnpj.Trim();
            cnpj = cnpj.Replace(".", "").Replace("-", "").Replace("/", "");
            if (cnpj.Length != 14)
                return false;
            tempCnpj = cnpj.Substring(0, 12);
            soma = 0;
            for (int i = 0; i < 12; i++)
                soma += int.Parse(tempCnpj[i].ToString()) * multiplicador1[i];
            resto = (soma % 11);
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            digito = resto.ToString();
            tempCnpj = tempCnpj + digito;
            soma = 0;
            for (int i = 0; i < 13; i++)
                soma += int.Parse(tempCnpj[i].ToString()) * multiplicador2[i];
            resto = (soma % 11);
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            digito = digito + resto.ToString();
            return cnpj.EndsWith(digito);
        }
        
        public static bool TryValidate(object obj, out ICollection<ValidationResult> results)
        {
            var context = new ValidationContext(obj, serviceProvider: null, items: null);
            results = new List<ValidationResult>();
            return Validator.TryValidateObject(
                obj, context, results,
                validateAllProperties: true
            );
        }

        public static bool ValidaCep(string cep)
        {
            string str = IntegerHelper.GetOnlyNumber(cep);
            return str.Length == 8;
        }
        
    }
}
