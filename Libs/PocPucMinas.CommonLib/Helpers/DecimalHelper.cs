﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace PocPucMinas.CommonLib.Helpers
{
    public static class DecimalHelper
    {
        private static CultureInfo CultureInfoBrazil
        {
            get
            {
                return CultureInfo.GetCultureInfo("pt-BR");
            }
        }

        public static string GetValorFormatado(decimal valor, short quantidadeCasasDecimais=2)
        {
            return valor.ToString($"N{quantidadeCasasDecimais}", CultureInfoBrazil);
        }

        public static string GetValorMonetarioFormatado(decimal valor)
        {
            return valor.ToString("C", CultureInfoBrazil);
        }

        public static decimal? GetValorMonetarioSemFormatacao(string valor)
        {
            if(string.IsNullOrWhiteSpace(valor))
            {
                return null;
            }

            var valorSemSimboloMoeda = valor.Replace("R$", "").Trim();
            if(string.IsNullOrWhiteSpace(valorSemSimboloMoeda))
            {
                return null;
            }

            return Convert.ToDecimal(valorSemSimboloMoeda, CultureInfoBrazil);
        }

    }
}
