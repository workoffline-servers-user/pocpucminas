﻿using System;

namespace PocPucMinas.CommonLib.Helpers
{
    public static class CpfCnpjHelper
    {
        public static string GetCnpjSemFormatacao(string cnpj)
        {
            var valorLong = MascaraHelper.GetValorInt64SemMascara(cnpj);
            if(valorLong.HasValue)
            {
                return GetCnpjSemFormatacao(valorLong.Value);
            }
            return cnpj;
        }

        public static string GetCnpjSemFormatacao(long cnpj)
        {
            string vStrCnpj = cnpj.ToString();

            for (int i = vStrCnpj.Length; i < 14; i++)
                vStrCnpj = "0" + vStrCnpj;
            return vStrCnpj;
        }

        public static string GetCpfSemFormatacao(string cpf)
        {
            var valorLong = MascaraHelper.GetValorInt64SemMascara(cpf);
            if (valorLong.HasValue)
            {
                return GetCpfSemFormatacao(valorLong.Value);
            }
            return cpf;
        }

        public static string GetCpfSemFormatacao(long cpf)
        {
            string strCpf = cpf.ToString();

            for (int i = strCpf.Length; i < 11; i++)
                strCpf = "0" + strCpf;
            return strCpf;
        }

        public static string GetCnpjFormatado(string cnpj)
        {
            var valorLong = MascaraHelper.GetValorInt64SemMascara(cnpj);
            if (valorLong.HasValue)
            {
                return GetCnpjFormatado(valorLong.Value);
            }
            return cnpj;
        }
        
        public static string GetCnpjFormatado(long cnpj)
        {
            return cnpj.ToString(@"00\.000\.000\/0000\-00");
        }

        public static string GetCpfFormatado(string cpf)
        {
            var valorLong = MascaraHelper.GetValorInt64SemMascara(cpf);
            if (valorLong.HasValue)
            {
                return GetCpfFormatado(valorLong.Value);
            }
            return cpf;
        }

        public static string GetCpfFormatado(long cpf)
        {
            return cpf.ToString(@"000\.000\.000\-00");
        }

        public static string GetCpfCnpjFormatado(string cpfCnpj)
        {
            var valorLong = MascaraHelper.GetValorInt64SemMascara(cpfCnpj);
            if(valorLong.HasValue)
            {
                return GetCpfCnpjFormatado(valorLong.Value);
            }
            return cpfCnpj;
        }

        /// <summary>
        /// Formata o CPF/CNPJ informado
        /// </summary>
        /// <param name="cpfCnpj">CPF/CNPJ numérico</param>
        /// <returns>CPF/CNPJ formatado</returns>
        public static string GetCpfCnpjSemFormatacao(long cpfCnpj)
        {
            if (ValidacaoHelper.ValidaCpf(cpfCnpj.ToString()))
                return GetCpfSemFormatacao(cpfCnpj);

            else if (ValidacaoHelper.ValidaCnpj(cpfCnpj.ToString()))
                return GetCnpjSemFormatacao(cpfCnpj);
            
            else
                return cpfCnpj.ToString();
        }

        public static string GetCpfCnpjFormatado(long cpfCnpj)
        {
            if (ValidacaoHelper.ValidaCpf(cpfCnpj.ToString()))
                return GetCpfFormatado(cpfCnpj);

            else if (ValidacaoHelper.ValidaCnpj(cpfCnpj.ToString()))
                return GetCnpjFormatado(cpfCnpj);

            else
                return cpfCnpj.ToString();
        }
    }
}
