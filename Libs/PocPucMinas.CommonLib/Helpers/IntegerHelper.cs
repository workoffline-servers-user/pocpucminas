﻿using System;
using System.Text;

namespace PocPucMinas.CommonLib.Helpers
{
    public static class IntegerHelper
    {
        public static short? ToInt16(string pValor)
        {
            short vValor;
            if (pValor == null)
                return null;
            else if (pValor.Length == 0)
                return null;
            else if (short.TryParse(pValor, out vValor))
                return Convert.ToInt16(pValor, 10);
            else return null;
        }

        public static int ToInt32(string pValor, int defaultValue)
        {
            try
            {
                int vValor;
                if (pValor == null || pValor.Length == 0)
                    return defaultValue;
                else if (int.TryParse(pValor, out vValor))
                    return Convert.ToInt32(pValor, 10);                
                else return defaultValue;
            }
            catch (Exception)
            {
                return defaultValue;
            }

        }

        public static int? ToInt32(string pValor)
        {
            return ToInt32(pValor, false);
        }

        public static int? ToInt32(string pValor, bool lancarExcecao)
        {
            try
            {
                int vValor;
                if (pValor == null || pValor.Length == 0)
                    return null;
                else if (int.TryParse(pValor, out vValor))
                    return Convert.ToInt32(pValor, 10);
                else if (lancarExcecao) throw new Exception();
                else return null;
            }
            catch (Exception ex)
            {
                if (lancarExcecao) throw ex;
                else return null;
            }

        }

        public static long? ToInt64(string pValor, long? defaultValue = null)
        {
            try
            {
                if (pValor == null || pValor.Length == 0)
                    return defaultValue;

                return Convert.ToInt64(pValor, 10);                
            }
            catch (Exception)
            {
                return defaultValue;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="token"></param>
        /// <returns>String.Empty - se não existir nenhum numero, c.c. a string do numero com 0 a esquerda se existir</returns>
        public static string GetOnlyNumber(string token)
        {
            if (string.IsNullOrEmpty(token)) return string.Empty;
            
            char[] letras = token.ToCharArray();
            StringBuilder sb = new StringBuilder();
            foreach (char l in letras)
            {
                int cod = (int)l;
                if (cod >= 48 && cod <= 57)
                    sb.Append(l);
            }
            return sb.ToString();
        }

        public static long? LimitNumber(long? telefone, int maxSize)
        {
            if (telefone != null)
            {
                string strTelefone = telefone.Value.ToString();
                if (strTelefone.Length > maxSize)
                {
                    strTelefone = strTelefone.Substring(0, maxSize);
                    return ToInt64(strTelefone);
                }
            }
            return telefone;
        }
    }
}
