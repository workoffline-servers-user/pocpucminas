﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace PocPucMinas.CommonLib.Helpers
{
    public class HttpHelper
    {
        public static string GetConteudoRemoto(Uri uri, TimeSpan timeout, bool exceptionSafe = false)
        {
            return GetConteudoRemotoAsync(uri, timeout, exceptionSafe).Result;
        }

        public static async Task<string> GetConteudoRemotoAsync(Uri uri, TimeSpan timeout, bool exceptionSafe = false)
        {
            try
            {
                using (HttpClient client = new HttpClient
                {
                    Timeout = timeout
                })
                using (HttpResponseMessage response = await client.GetAsync(uri))
                using (HttpContent content = response.Content)
                {
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var result = await content.ReadAsStringAsync();
                        if (result != null && result.Length >= 0)
                        {
                            return result;
                        }
                    }
                }

                return null;
            }
            catch (Exception ex)
            {
                if (exceptionSafe)
                {
                    return null;
                }
                else
                {
                    throw ex;
                }
            }
        }

    }

}
