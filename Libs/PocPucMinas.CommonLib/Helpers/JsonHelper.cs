﻿using Newtonsoft.Json;

namespace PocPucMinas.CommonLib.Helpers
{
    public static class JsonHelper
    {
        public static T ParseString<T>(string jsonInput)
        {
            return JsonConvert.DeserializeObject<T>(jsonInput);
        }

        public static string ToString(object objectInput)
        {
            return JsonConvert.SerializeObject(objectInput);
        }

    }
}
