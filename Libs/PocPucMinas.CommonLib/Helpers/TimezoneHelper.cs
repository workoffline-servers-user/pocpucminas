﻿using NodaTime.TimeZones;
using System;
using System.Linq;

namespace PocPucMinas.CommonLib.Helpers
{
    public static class TimezoneHelper
    {
        public static DateTime ConvertTimezonedDateTimeToUtc(DateTime originDateTime, string originTimezoneIdOlsonFormat)
        {
            try
            {
                var timeZoneOrigem = GetTimeZoneInfoFromOlsonFormat(originTimezoneIdOlsonFormat);
                return TimeZoneInfo.ConvertTimeToUtc(originDateTime, timeZoneOrigem);
            }
            catch (TimeZoneNotFoundException ex)
            {
                throw ex;
            }
            catch (InvalidTimeZoneException ex)
            {
                throw ex;
            }

        }

        public static DateTime ConvertTimezonedDateTimeToSystemTimezone(DateTime originDateTime, string originTimezoneIdOlsonFormat)
        {
            try
            {
                var localTimezone = TimeZoneInfo.Local;
                var utcDateTime = ConvertTimezonedDateTimeToUtc(originDateTime, originTimezoneIdOlsonFormat);

                return TimeZoneInfo.ConvertTimeFromUtc(utcDateTime, localTimezone);
            }
            catch (TimeZoneNotFoundException ex)
            {
                throw ex;
            }
            catch (InvalidTimeZoneException ex)
            {
                throw ex;
            }

        }

        public static DateTime ConvertDateTimeToTimezone(DateTime datetime, string destinationTimezoneIdOlsonFormat)
        {
            try
            {
                var timeZoneDestino = GetTimeZoneInfoFromOlsonFormat(destinationTimezoneIdOlsonFormat);
                return TimeZoneInfo.ConvertTimeFromUtc(datetime, timeZoneDestino);
            }
            catch (TimeZoneNotFoundException ex)
            {
                throw ex;
            }
            catch (InvalidTimeZoneException ex)
            {
                throw ex;
            }

        }
        
        public static DateTime ConvertCurrentDateTimeToTimezone(string destinationTimezoneIdOlsonFormat)
        {
            return ConvertDateTimeToTimezone(DateTime.UtcNow, destinationTimezoneIdOlsonFormat);
        }

        public static TimeZoneInfo GetTimeZoneInfoFromOlsonFormat(string timezoneIdOlsonFormat)
        {
            var timeZoneId = GetTimeZoneIdFromOlsonFormat(timezoneIdOlsonFormat);            
            return timezoneIdOlsonFormat == default ? default : TimeZoneInfo.FindSystemTimeZoneById(timeZoneId);
        }

        public static string GetTimeZoneIdFromOlsonFormat(string timezoneIdOlsonFormat)
        {
            var mappings = TzdbDateTimeZoneSource.Default.WindowsMapping.MapZones;
            var map = mappings.FirstOrDefault(x =>
                x.TzdbIds.Any(z => z.Equals(timezoneIdOlsonFormat, StringComparison.OrdinalIgnoreCase)));

            return map == default ? default : map.WindowsId;
        }

    }
}
