﻿using PocPucMinas.CommonLib.Support;
using System;
using System.Collections.Generic;
using System.Text;

namespace PocPucMinas.CommonLib.Helpers
{
    public static class AppSettingsHelper
    {
        public static string GetVersaoFromDataHora(DateTime dataHora)
        {
            var dataHoraString = string.Format("{0:0000}.{1:00}.{2:00}.{3:00}.{4:00}.{5:00}",
                    dataHora.Year, dataHora.Month, dataHora.Day,
                    dataHora.Hour, dataHora.Minute, dataHora.Second);

            return dataHoraString;
        }

        public static bool IsAmbienteDeExecucaoValido(string identificadorAmbiente)
        {
            return identificadorAmbiente == Constants.Ambiente.PRODUCAO
                        || identificadorAmbiente == Constants.Ambiente.HOMOLOGACAO
                            || identificadorAmbiente == Constants.Ambiente.DESENVOLVIMENTO;            
        }

    }
}
