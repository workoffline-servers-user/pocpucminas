﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace PocPucMinas.CommonLib.Helpers
{
    public static class DateTimeHelper
    {
        public const string DATE_FORMAT_FOR_HUMAN = "dd/MM/yyyy";
        public const string TIME_FORMAT_FOR_HUMAN = "HH:mm:ss";
        public const string DATETIME_FORMAT_FOR_HUMAN = "dd/MM/yyyy HH:mm:ss";
        
        public const string DATE_FORMAT_FOR_DTO = "yyyy-MM-dd";
        public const string TIME_FORMAT_FOR_DTO = "HH:mm:ss";
        public const string DATETIME_FORMAT_FOR_DTO = "yyyy-MM-dd HH:mm:ss";

        public const string DATE_FORMAT_FOR_FILESYSTEM = "yyyy-MM-dd";
        public const string DATETIME_FORMAT_FOR_FILESYSTEM = "yyyy-MM-dd_HH-mm-ss";

        public const string DATETIME_FORMAT_FOR_LOG = "yyyy-MM-dd HH:mm:ss";
        public const string TIME_HOUR_MINUTE = "HH:mm";
                

        //Constantes de data oracle
        //Wed, 08 May 2013 17:24:18 GMT
        public const string DATETIME_DESCRIPTION = "ddd, dd MM yyyy HH:mm:ss 'GMT'";

        public static long? GetDuracaoEmSegundos(DateTime? inicio, DateTime? fim)
        {
            if (inicio == null || fim == null) return null;
            else if (inicio.Value > fim.Value) return null;
            else
            {
                return (long)((fim.Value - inicio.Value).TotalSeconds);
            }
        }
        public static long? GetDuracaoEmDias(DateTime? inicio, DateTime? fim)
        {

            double? duracao = GetDuracaoEmSegundos(inicio, fim);
            if (duracao == null) return null;
            else
            {
                double dif = duracao.Value / (double)(24 * 60 * 60);
                return (long)dif;
            }
        }
        public static long? GetDuracaoEmMinutos(DateTime? inicio, DateTime? fim)
        {
            if (inicio == null || fim == null) return null;
            else if (inicio.Value > fim.Value) return null;
            else
            {
                double totalMinutos = (fim.Value - inicio.Value).TotalMinutes;
                long intTotalMinutos = (long)Math.Ceiling(totalMinutos);
                return intTotalMinutos;
            }
        }

        public static string DateTimeToString(DateTime date, string mascara = DATETIME_FORMAT_FOR_HUMAN)
        {
            if (mascara == null)
                mascara = DATETIME_FORMAT_FOR_HUMAN;
            return date.ToString(mascara, DateTimeFormatInfo.InvariantInfo);
        }

        public static DateTime? StringToDateTime(string pDateTime, string mascara = DATETIME_FORMAT_FOR_HUMAN)
        {
            if (mascara == null)
                mascara = DATETIME_FORMAT_FOR_HUMAN;
            try
            {
                DateTime? vTime = DateTime.ParseExact(pDateTime, mascara, CultureInfo.InvariantCulture);
                return vTime;
            }
            catch
            {
                return null;
            }
        }

        public static DateTime GetDateTimeUltimoDiaDoMes(short ano, short mes)
        {
            DateTime ultimoDiaDoMes = new DateTime(ano, mes, 1);
            return ultimoDiaDoMes.AddMonths(1).AddDays(-1);
        }

        public static long GetUltimoDiaDoMes(DateTime data)
        {
            DateTime novaData = data.AddMonths(1).AddDays(-1);
            return novaData.Day;
        }

        public static string DateBrazilToString(DateTime date)
        {
            return DateTimeToString(date, DATE_FORMAT_FOR_HUMAN);
        }

        public static string DateTimeBrazilToString(DateTime date)
        {
            return DateTimeToString(date, DATETIME_FORMAT_FOR_HUMAN);
        }

        public static DateTime? StringToDateBrazil(string date)
        {
            return StringToDateTime(date, DATETIME_FORMAT_FOR_HUMAN);
        }

        public static DateTime? StringToDateTimeBrazil(string date)
        {
            return StringToDateTime(date, DATETIME_FORMAT_FOR_HUMAN);
        }

        public static string GetDataPorExtensoBrazil(DateTime date)
        {
            var brazil = CultureInfo.CreateSpecificCulture("pt-BR");
            var dia = date.ToString("dd", brazil);
            var mes = date.ToString("MMMM", brazil);
            var ano = date.ToString("yyyy", brazil);

            return $"{dia} de {mes} de {ano}";
        }

        public static string GetFormattedTimezonedString(DateTime dateTime)
        {
            return dateTime.ToUniversalTime().ToString("s");
        }

        public static DateTime? GetDateTimeFromFormattedTimezonedString(string dateTimeJson)
        {
            if(DateTime.TryParseExact(dateTimeJson, "s", CultureInfo.InvariantCulture, DateTimeStyles.AssumeUniversal, out DateTime result))
            {
                return result;
            }

            return null;
        }

        public static DateTime? ConvertToTimezone(DateTime dateTime, string destinationTimezoneIdOlsonFormat)
        {
            try
            {
                return TimezoneHelper.ConvertDateTimeToTimezone(dateTime, destinationTimezoneIdOlsonFormat);
            }
            catch
            {
                return null;
            }            
        }

        public static double? GetDuracaoEmDias(DateTime? inicio, DateTime? fim, bool arredondaDataAcimaDe12Horas)
        {
            double? duracao = GetDuracaoEmSegundos(inicio, fim);
            if (duracao == null) return null;
            else
            {
                double dif = duracao.Value / (double)(24 * 60 * 60);
                if (arredondaDataAcimaDe12Horas)
                {
                    int total1 = inicio.Value.Hour * 60 + inicio.Value.Minute;
                    int total2 = fim.Value.Hour * 60 + fim.Value.Minute;
                    if (Math.Abs(total2 - total1) > 12 * 60) return dif + 1;
                    else return dif;
                }
                return dif;
            }
        }
    }
}
