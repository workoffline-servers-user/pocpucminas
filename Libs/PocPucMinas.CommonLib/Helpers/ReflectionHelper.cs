﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PocPucMinas.CommonLib.Helpers
{
    public static class ReflectionHelper
    {
        public static object GetPropertyValue(object obj, string propriedade)
        {
            if (obj == null)
            {
                return null;
            }

            var type = obj.GetType();
            if (type.GetProperties().Any(x => x.Name == propriedade))
            {
                var property = type.GetProperty(propriedade);
                if (property == null)
                {
                    return null;
                }

                return property.GetValue(obj);
            }

            return null;
        }

        public static object GetFieldValue(object obj, string propriedade)
        {
            if (obj == null)
            {
                return null;
            }

            var type = obj.GetType();
            if (type.GetFields().Any(x => x.Name == propriedade))
            {
                var property = type.GetField(propriedade);
                if (property == null)
                {
                    return null;
                }

                return property.GetValue(obj);
            }

            return null;
        }

        public static object GetFieldValue(object obj, string[] partes)
        {
            foreach (string parte in partes)
            {
                if (obj == null)
                {
                    return null;
                }

                var type = obj.GetType();
                if (type.GetProperties().Any(x => x.Name == parte))
                {
                    var property = type.GetProperty(parte);
                    if (property == null)
                    {
                        return null;
                    }

                    return property.GetValue(obj);
                }
                else if (type.GetFields().Any(x => x.Name == parte))
                {
                    var field = type.GetField(parte);
                    if (field == null)
                    {
                        return null;
                    }

                    return field.GetValue(obj);
                }

            }
            return obj;
        }

    }

}
