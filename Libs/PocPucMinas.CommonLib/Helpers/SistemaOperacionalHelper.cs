﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace PocPucMinas.CommonLib.Helpers
{
    public static class SistemaOperacionalHelper
    {
        public static bool IsWindows() =>
            RuntimeInformation.IsOSPlatform(OSPlatform.Windows);

        public static bool IsMacOS() =>
            RuntimeInformation.IsOSPlatform(OSPlatform.OSX);

        public static bool IsLinux() =>
            RuntimeInformation.IsOSPlatform(OSPlatform.Linux);

        public static string GetSistemaOperacional()
        {
            if(IsWindows())
            {
                return "Windows";
            }
            else if(IsLinux())
            {
                return "Linux";
            }
            else if(IsMacOS())
            {
                return "MacOS";
            }
            else
            {
                return string.Empty;
            }
        }
    }
}
