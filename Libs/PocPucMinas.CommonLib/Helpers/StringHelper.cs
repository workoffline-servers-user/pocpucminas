﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PocPucMinas.CommonLib.Helpers
{
    public class StringHelper
    {
        public static string Substring(string token, int size)
        {
            if (string.IsNullOrEmpty(token)) return token;

            if (token.Length <= size) return token;
            else return token.Substring(0, size);
        }

        public static string Lapd(int length, string token, char letra = ' ')
        {
            if (token == null) return null;
            if (token.Length > length)
                return token.Substring(0, length);
            else if (token.Length == length)
                return token;
            else
            {
                string pad = Padding(length - token.Length, letra);
                return pad + token;
            }
        }

        public static string Padding(int length, char letra = ' ')
        {
            string ret = "";
            for (int i = 0; i < length; i++)
                ret += letra;
            return ret;
        }
    }
}
