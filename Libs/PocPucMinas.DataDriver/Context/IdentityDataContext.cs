﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using PocPucMinas.CommonLib.Extensions;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using PocPucMinas.DataDriver.Extensions;

namespace PocPucMinas.CommonLib.DataContext
{
    public abstract class IdentityDataContext<TUser> : IdentityDbContext<TUser> where TUser : IdentityUser
    {

        /// <summary>
        /// Construtor padrão (único)
        /// </summary>
        /// <param name="options">Objeto do tipo DbContextOptions, passado para o construtor base IdentityDbContext</param>
        public IdentityDataContext(DbContextOptions options) : base(options)
        {

        }

        /// <summary>
        /// Método invocado pelo EF ao iniciar a aplicação
        /// Responsável por fazer a criação do modelo de dados no SGBD
        /// </summary>
        /// <param name="modelBuilder">Instância do ModelBuilder</param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            //Normaliza nome de objetos
            modelBuilder.NormalizeNames();

            //Adiciona indices às colunas
            AddIndexes(modelBuilder);
        }

        /// <summary>
        /// Método responsável por adicionar índices às colunas desejadas
        /// </summary>
        /// <param name="modelBuilder">Instância do ModelBuilder</param>
        protected abstract void AddIndexes(ModelBuilder modelBuilder);
    }
}
