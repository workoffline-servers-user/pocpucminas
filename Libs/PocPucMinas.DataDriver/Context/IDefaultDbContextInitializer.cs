﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PocPucMinas.DataDriver.Context
{
    public interface IDefaultDbContextInitializer
    {
        DbContext GetContext();
        bool EnsureCreated();
        void Migrate();
        Task Seed();
    }
}
