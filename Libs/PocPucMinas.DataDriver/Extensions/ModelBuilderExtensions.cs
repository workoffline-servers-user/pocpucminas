﻿using Microsoft.EntityFrameworkCore;
using PocPucMinas.CommonLib.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace PocPucMinas.DataDriver.Extensions
{
    public static class ModelBuilderExtensions
    {
        /// <summary>
        /// Métdo responsável por normalizar os nomes de tabelas, campos, chaves estrangeiras e índices
        /// </summary>
        /// <param name="modelBuilder">Instância do ModelBuilder</param>
        public static void NormalizeNames(this ModelBuilder modelBuilder)
        {
            foreach (var entity in modelBuilder.Model.GetEntityTypes())
            {
                // Remove 'AspNet' prefix and convert table name from PascalCase to snake_case. E.g. AspNetRoleClaims -> role_claims
                entity.Relational().TableName = entity.Relational().TableName.Replace("AspNet", "").ToSnakeCase();

                // Convert column names from PascalCase to snake_case.
                foreach (var property in entity.GetProperties())
                {
                    property.Relational().ColumnName = property.Name.ToSnakeCase();
                }

                // Convert primary key names from PascalCase to snake_case. E.g. PK_users -> pk_users
                foreach (var key in entity.GetKeys())
                {
                    key.Relational().Name = key.Relational().Name.ToSnakeCase();
                }

                // Convert foreign key names from PascalCase to snake_case.
                foreach (var key in entity.GetForeignKeys())
                {
                    key.Relational().Name = key.Relational().Name.ToSnakeCase();
                }

                // Convert index names from PascalCase to snake_case.
                foreach (var index in entity.GetIndexes())
                {
                    index.Relational().Name = index.Relational().Name.ToSnakeCase();
                }
            }
        }
    }
}
