﻿using Microsoft.EntityFrameworkCore;
using PocPucMinas.DataDriver.Context;
using PocPucMinas.DataDriver.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PocPucMinas.DataDriver.UnitsOfWork
{
    public class BaseUnitOfWork : IBaseUnitOfWork, IDisposable
    {
        protected readonly DbContext _dbContext;
        private readonly Dictionary<Type, object> _repositories = new Dictionary<Type, object>();
        private readonly Dictionary<Type, object> _customRepositories = new Dictionary<Type, object>();

        /// <summary>
        /// Repositório Genérico (baseado no tipo da entidade)
        /// </summary>
        private Dictionary<Type, object> Repositories
        {
            get { return _repositories; }
            set { Repositories = value; }
        }

        /// <summary>
        /// Repositório customizado (instância de classe herdeira do repositório base)
        /// </summary>
        private Dictionary<Type, object> CustomRepositories
        {
            get { return _customRepositories; }
            set { CustomRepositories = value; }
        }

        /// <summary>
        /// Construtor padrão
        /// </summary>
        /// <param name="dbContextInitializer">Contexto de Banco de Dados (injetado)</param>
        public BaseUnitOfWork(IDefaultDbContextInitializer dbContextInitializer)
        {
            _dbContext = dbContextInitializer.GetContext();
        }

        /// <summary>
        /// Adiciona em estrutura interna o repositório da entidade do tipo passado, 
        /// e reaproveita a instância em utilizações futuras.
        /// </summary>
        /// <typeparam name="T">Tipo do Repositório</typeparam>
        /// <returns>A instância do repositório para o tipo T especificado</returns>
        public IBaseRepository<T> Repository<T>() where T : class
        {
            if (Repositories.Keys.Contains(typeof(T)))
            {
                return Repositories[typeof(T)] as IBaseRepository<T>;
            }

            IBaseRepository<T> repo = new BaseRepository<T>(_dbContext);
            Repositories.Add(typeof(T), repo);
            return repo;
        }

        /// <summary>
        /// Adiciona em estrutura interna o repositório do tipo passado, 
        /// e reaproveita a instância em utilizações futuras.
        /// </summary>
        /// <typeparam name="T">Repositório customizado</typeparam>
        /// <typeparam name="U">Tipo da entidade</typeparam>
        /// <returns>A instância do repositório T especificado</returns>
        public T CustomRepository<T, U>() 
            where T : BaseRepository<U>
            where U : class
        {
            if (CustomRepositories.Keys.Contains(typeof(T)))
            {
                return CustomRepositories[typeof(T)] as T;
            }

            T repo = (T) Activator.CreateInstance(typeof(T), new object[] { _dbContext });

            CustomRepositories.Add(typeof(T), repo);
            return repo;
        }

        /// <summary>
        /// Realiza o commit da transação no DbContext, de forma assíncrona
        /// </summary>
        /// <returns></returns>
        public async Task<int> Commit()
        {
            return await _dbContext.SaveChangesAsync();
        }

        /// <summary>
        /// Realiza o Rollback na transação
        /// </summary>
        public void Rollback()
        {
            _dbContext.ChangeTracker.Entries().ToList().ForEach(x => x.Reload());
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _dbContext.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}
