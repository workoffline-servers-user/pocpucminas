﻿using PocPucMinas.DataDriver.Repositories;
using System.Threading.Tasks;

namespace PocPucMinas.DataDriver.UnitsOfWork
{
    public interface IBaseUnitOfWork
    {
        IBaseRepository<T> Repository<T>() 
            where T : class;

        T CustomRepository<T,U>()
            where T : BaseRepository<U>
            where U : class;

        Task<int> Commit();

        void Rollback();
    }
}