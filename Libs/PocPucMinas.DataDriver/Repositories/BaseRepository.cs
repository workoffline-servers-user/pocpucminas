﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace PocPucMinas.DataDriver.Repositories
{
    public class BaseRepository<T> :  IBaseRepository<T> where T : class
    {
        protected readonly DbContext _context;

        public BaseRepository(DbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Método que retorna o DbSet como Queryable, para ser utilizada com expressões Linq
        /// </summary>
        /// <returns>Instância de Queryable referente ao DbSet</returns>
        public IQueryable<T> Query()
        {
            return _context.Set<T>().AsQueryable();
        }

        /// <summary>
        /// Método que retorna todo o conjunsto de dados como lista
        /// </summary>
        /// <returns>Lista com todos os registros do DbSet</returns>
        public ICollection<T> GetAll()
        {
            return _context.Set<T>().ToList();
        }

        /// <summary>
        /// Método assíncrono que retorna todo o conjunsto de dados como lista
        /// </summary>
        /// <returns>Lista com todos os registros do DbSet</returns>
        public async Task<ICollection<T>> GetAllAsync()
        {
            return await _context.Set<T>().ToListAsync();
        }

        /// <summary>
        /// Método que retorna único registro, baseado no ID passado como parâmetro
        /// </summary>
        /// <param name="id">ID numérico do registro a ser recuperado</param>
        /// <returns>Instância do registro com o ID pesquisado</returns>
        public T GetById(int id)
        {
            return _context.Set<T>().Find(id);
        }

        /// <summary>
        /// Método assíncrono que retorna único registro, baseado no ID passado como parâmetro
        /// </summary>
        /// <param name="id">ID numérico do registro a ser recuperado</param>
        /// <returns>Instância do registro com o ID pesquisado</returns>
        public async Task<T> GetByIdAsync(int id)
        {
            return await _context.Set<T>().FindAsync(id);
        }

        public T GetByUniqueId(string id)
        {
            return _context.Set<T>().Find(id);
        }

        public async Task<T> GetByUniqueIdAsync(string id)
        {
            return await _context.Set<T>().FindAsync(id);
        }

        /// <summary>
        /// Método que retorna único registro, baseado na expressão de busca Linq informada como parâmetro
        /// </summary>
        /// <param name="match">Expressão Linq</param>
        /// <returns>Instância do primeiro registro que satisfaz os critérios da busca</returns>
        public T Find(Expression<Func<T, bool>> match)
        {
            return _context.Set<T>().FirstOrDefault(match);
        }

        /// <summary>
        /// Método assíncrono que retorna único registro, baseado na expressão de busca Linq informada como parâmetro
        /// </summary>
        /// <param name="match">Expressão Linq</param>
        /// <returns>Instância do primeiro registro que satisfaz os critérios da busca</returns>
        public async Task<T> FindAsync(Expression<Func<T, bool>> match)
        {
            return await _context.Set<T>().FirstOrDefaultAsync(match);
        }

        /// <summary>
        /// Método assíncrono que retorna único registro, baseado na expressão de busca Linq informada como parâmetro
        /// </summary>
        /// <param name="match">Expressão Linq</param>
        /// <returns>Instância do primeiro registro que satisfaz os critérios da busca</returns>
        public async Task<T> FindAsyncIncluding(Expression<Func<T, bool>> match, Func<IQueryable<T>, IQueryable<T>> includeFunction)
        {
            var result = _context.Set<T>().Where(match);
            return await includeFunction(result).FirstOrDefaultAsync();
        }

        /// <summary>
        /// Método que retorna coleção de registros, baseado na expressão de busca Linq informada como parâmetro
        /// </summary>
        /// <param name="match">Expressão Linq</param>
        /// <returns>Coleção de registros que satisfaz os critérios de busca</returns>
        public ICollection<T> FindAll(Expression<Func<T, bool>> match)
        {
            return _context.Set<T>().Where(match).ToList();
        }

        /// <summary>
        /// Método assíncrono que retorna coleção de registros, baseado na expressão de busca Linq informada como parâmetro
        /// </summary>
        /// <param name="match">Expressão Linq</param>
        /// <returns>Coleção de registros que satisfaz os critérios de busca</returns>
        public async Task<ICollection<T>> FindAllAsync(Expression<Func<T, bool>> match)
        {
            return await _context.Set<T>().Where(match).ToListAsync();
        }

        /// <summary>
        /// Método assíncrono que retorna coleção de registros, baseado na expressão de busca Linq informada como parâmetro
        /// </summary>
        /// <param name="match">Expressão Linq</param>
        /// <returns>Coleção de registros que satisfaz os critérios de busca</returns>
        public async Task<ICollection<T>> FindAllAsyncIncluding(Expression<Func<T, bool>> match, Func<IQueryable<T>, IQueryable<T>> includeFunction)
        {
            var result = _context.Set<T>().Where(match);
            return await includeFunction(result).ToListAsync();

        }

        /// <summary>
        /// Método responsável por inserir registro ao DbSet
        /// </summary>
        /// <param name="entity">Elemento a ser inserido</param>
        /// <returns>Instância do próprio elemento inserido</returns>
        public T Add(T entity)
        {
            _context.Set<T>().Add(entity);
            return entity;
        }

        /// <summary>
        /// Método assíncrono responsável por inserir registro ao DbSet
        /// </summary>
        /// <param name="entity">Elemento a ser inserido</param>
        /// <returns>Instância do próprio elemento inserido</returns>
        public async Task<T> AddAsync(T entity)
        {
            await _context.Set<T>().AddAsync(entity);
            return entity;
        }

        /// <summary>
        /// Método responsável por atualizar um registro no DbSet
        /// </summary>
        /// <param name="updated">Instância (já modificada) do registro a ser persistido no DbSet</param>
        /// <returns>A própria instância do registro modificado</returns>
        public T Update(T updated)
        {
            if (updated == null)
            {
                return null;
            }

            _context.Set<T>().Attach(updated);
            _context.Entry(updated).State = EntityState.Modified;

            return updated;
        }
        
        /// <summary>
        /// Remove um registro do DbSet
        /// </summary>
        /// <param name="t">Instância do registor a ser removido</param>
        public void Delete(T t)
        {
            _context.Set<T>().Remove(t);
        }

        /// <summary>
        /// Método responsável por retornar a quantidade de registros presentes no DbSet
        /// </summary>
        /// <returns>Quantidade total de registros presentes no DbSet</returns>
        public int Count()
        {
            return _context.Set<T>().Count();
        }

        /// <summary>
        /// Método assíncrono responsável por retornar a quantidade de registros presentes no DbSet
        /// </summary>
        /// <returns>Quantidade total de registros presentes no DbSet</returns>
        public async Task<int> CountAsync()
        {
            return await _context.Set<T>().CountAsync();
        }

        /// <summary>
        /// Método responsável por aplicar filtro ao DbSet
        /// </summary>
        /// <param name="filter">Expressão de filtro (similar à cláusula Where)</param>
        /// <param name="orderBy">Expressão de ordenação do ResultSet</param>
        /// <param name="includeProperties"></param>
        /// <param name="page">Página a ser recuperada (de acordo com o tamanho da página)</param>
        /// <param name="pageSize">Tamanho da página</param>
        /// <returns></returns>
        public IEnumerable<T> Filter(Expression<Func<T, bool>> filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, string includeProperties = "", int? page = null,
            int? pageSize = null)
        {
            IQueryable<T> query = _context.Set<T>();
            if (filter != null)
            {
                query = query.Where(filter);
            }

            if (orderBy != null)
            {
                query = orderBy(query);
            }

            if (includeProperties != null)
            {
                foreach (
                    var includeProperty in includeProperties.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    query = query.Include(includeProperty);
                }
            }

            if (page != null && pageSize != null)
            {
                query = query.Skip((page.Value - 1) * pageSize.Value).Take(pageSize.Value);
            }

            return query.ToList();
        }

        public IQueryable<T> FindBy(Expression<Func<T, bool>> predicate)
        {
            return _context.Set<T>().Where(predicate);
        }

        /// <summary>
        /// Método responsável por validar se existe algum elemento que satisfaz os critérios de busca
        /// </summary>
        /// <param name="predicate">Critérios de busca</param>
        /// <returns>True caso exista algum elemento, e false caso contrário</returns>
        public bool Exist(Expression<Func<T, bool>> predicate)
        {
            var exist = _context.Set<T>().Where(predicate);
            return exist.Any() ? true : false;
        }

        /// <summary>
        /// Método responsável por validar se existe algum elemento que satisfaz os critérios de busca
        /// </summary>
        /// <param name="predicate">Critérios de busca</param>
        /// <returns>True caso exista algum elemento, e false caso contrário</returns>
        public async Task<bool> ExistAsync(Expression<Func<T, bool>> predicate)
        {
            var exist = _context.Set<T>().Where(predicate);
            return (await exist.AnyAsync()) ? true : false;
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
