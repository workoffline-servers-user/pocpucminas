﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace PocPucMinas.DataDriver.Repositories
{
    public interface IBaseRepository<T> : IDisposable
    {        
        IQueryable<T> Query();

        ICollection<T> GetAll();

        Task<ICollection<T>> GetAllAsync();

        T GetById(int id);

        Task<T> GetByIdAsync(int id);

        T GetByUniqueId(string id);

        Task<T> GetByUniqueIdAsync(string id);

        T Find(Expression<Func<T, bool>> match);

        Task<T> FindAsync(Expression<Func<T, bool>> match);

        Task<T> FindAsyncIncluding(Expression<Func<T, bool>> match, Func<IQueryable<T>, IQueryable<T>> includeFunction);

        ICollection<T> FindAll(Expression<Func<T, bool>> match);
        
        Task<ICollection<T>> FindAllAsync(Expression<Func<T, bool>> match);

        Task<ICollection<T>> FindAllAsyncIncluding(Expression<Func<T, bool>> match, Func<IQueryable<T>, IQueryable<T>> includeFunction);

        T Add(T entity);

        Task<T> AddAsync(T entity);

        T Update(T updated);
        
        void Delete(T t);
        
        int Count();

        Task<int> CountAsync();

        IEnumerable<T> Filter(
            Expression<Func<T, bool>> filter = null,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
            string includeProperties = "",
            int? page = null,
            int? pageSize = null);

        IQueryable<T> FindBy(Expression<Func<T, bool>> predicate);

        bool Exist(Expression<Func<T, bool>> predicate);

        Task<bool> ExistAsync(Expression<Func<T, bool>> predicate);

    }
}
