using Microsoft.AspNetCore.Mvc;
using PocPucMinas.AspNetCore.Security.Attributes;
using PocPucMinas.Business.Services.Interfaces;
using PocPucMinas.QosLib.Interfaces;
using PocPucMinas.SegurancaComunicacaoWebApi.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PocPucMinas.SegurancaComunicacaoWebApi.Areas.Service.Controllers
{
    public class ComunicacaoController : ServiceAreaController<ComunicacaoController>
    {
        private readonly ISegurancaComunicacaoService _service;

        public ComunicacaoController(ISegurancaComunicacaoService service, IQosService qosService) : base(qosService)
        {
            _service = service;
        }


        /// <remarks>
        /// Recupera as regiões potencialmente atingidas de acordo com os alertas emitidos pelas medições de sensores na última hora.
        /// Esse método utiliza a integração com o <b>Módulo de Monitoramento de Barragens</b> para recuperação das informações relacionadas às medições.
        /// </remarks>
        /// <returns>Identificadores das regiões de risco afetadas</returns>
        [HttpGet("GetRegioesAtingidasAlertasNaUltimaHora")]
        [Autorizacao]
        public async Task<IActionResult> GetRegioesAtingidasAlertasNaUltimaHora()
        {
            var retorno = await _service.GetRegioesAtingidasAlertasNaUltimaHora();
            return Ok(retorno);
        }


        /// <remarks>
        /// Recupera a lista de destinatários cadastrados para a região de risco informada
        /// </remarks>
        /// <param name="regiaoDeRisco">Identificador da Região de Risco</param>
        /// <returns></returns>
        [HttpGet("RecuperarDestinatariosMensagens/{regiaoDeRisco}")]
        [Autorizacao]
        public async Task<IActionResult> RecuperarDestinatariosMensagens(int regiaoDeRisco)
        {
            var retorno =  await _service.RecuperarDestinatariosMensagens(regiaoDeRisco);
            return Ok(retorno);
        }

        /// <remarks>
        /// Recupera a quantidade de destinatários cadastrados para a região de risco informada
        /// </remarks>
        /// <param name="regiaoDeRisco">Identificador da Região de Risco</param>
        /// <returns></returns>
        [HttpGet("RecuperarQuantidadeDestinatariosAlertaPotencialmenteAtingidos/{regiaoDeRisco}")]
        [Autorizacao]
        public async Task<IActionResult> RecuperarQuantidadeDestinatariosAlertaPotencialmenteAtingidos(int regiaoDeRisco)
        {
            var retorno = await _service.RecuperarQuantidadeDestinatariosAlertaPotencialmenteAtingidos(regiaoDeRisco);
            return Ok(retorno);
        }

        /// <remarks>
        /// Faz o envio de menasgens para os destinatários potencialmente atingidos nas áreas de risco onde houve medições de alto risco.
        /// As informações relacionadas às áreas de risco atingidas são obtidas através de comunicação com o <b>Módulo de Monitoramento de Barragens</b>.
        /// </remarks>
        /// <returns>Quantidade de mensagens enviadas</returns>
        [HttpPost("EnviarMensagensParaDestinatariosPotencialmenteAtingidos")]
        [Autorizacao]
        public async Task<IActionResult> EnviarMensagensParaDestinatariosPotencialmenteAtingidos()
        {
            var retorno = await _service.EnviarMensagensParaDestinatariosPotencialmenteAtingidos();
            return Ok(retorno);
        }

        /// <remarks>
        /// Recupera todas os envios de mensagens pelo <b>Módulo de Segurança e Comunicação</b> ocorridos nos últimos minutos
        /// </remarks>
        /// <param name="quantidadeMinutosPassados">Quantidade de minutos</param>
        /// <returns></returns>
        [HttpGet("RecuperarMensagensEnviadas/{quantidadeMinutosPassados}")]
        [Autorizacao]
        public async Task<IActionResult> RecuperarMensagensEnviadas(int quantidadeMinutosPassados)
        {
            var retorno = await _service.RecuperarMensagensEnviadas(quantidadeMinutosPassados);
            return Ok(retorno);
        }

    }
}
