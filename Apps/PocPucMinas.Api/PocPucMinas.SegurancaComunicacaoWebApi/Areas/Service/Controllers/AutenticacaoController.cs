using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Text;
using PocPucMinas.DataLib.Models;
using PocPucMinas.AspNetCore.Services;
using PocPucMinas.SegurancaComunicacaoWebApi.Classes;
using PocPucMinas.AspNetCore.Services.Interfaces;
using PocPucMinas.QosLib.Interfaces;
using PocPucMinas.AspNetCore.Security.Services.Interfaces;

namespace PocPucMinas.SegurancaComunicacaoWebApi.Areas.Service.Controllers
{
    /// <summary>
    /// Controller de Autenticação aos serviços
    /// </summary>
    public class AutenticacaoController : ServiceAreaController<AutenticacaoController>
    {
        private readonly IAutenticacaoService _autenticacaoService;     

        public AutenticacaoController(
            IAutenticacaoService autenticacaoService,
            IQosService qosService) : base(qosService)
        {
            _autenticacaoService = autenticacaoService;
        }

        /// <remarks>
        /// Valida as credenciais informadas e gera Access Token caso a auteiticação seja positiva.
        /// </remarks>
        /// <param name="tokenDeAcesso">Login do usuário</param>
        /// <param name="senhaDeAcesso">Senha de acesso</param>
        /// <returns>Em caso de sucesso, retorna o Token JWT</returns>
        [AllowAnonymous]
        [HttpPost("LoginUsuarioApi")]
        [Produces("application/json")]
        public async Task<IActionResult> LoginUsuarioApi(string tokenDeAcesso, string senhaDeAcesso)
        {
            // Ensure the username and password is valid.
            var tokenJwt = await _autenticacaoService.AutenticaUsuario(tokenDeAcesso, senhaDeAcesso);
            if (tokenJwt == default)
            {
                throw new Exception("Usuário não autenticado");
            }
                        
            _qosService.GravaQosEstatistico("Login realizado no Backoffice (ID do usuário: {IdUsuario})", tokenJwt.IdentificadorUsuario.ToString());            
            return Ok(tokenJwt);
        }

    }
}
