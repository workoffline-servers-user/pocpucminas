using System;
using System.IO;
using System.Reflection;
using System.Threading;
using PocPucMinas.DataLib.Context;
using Gelf.Extensions.Logging;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using PocPucMinas.AspNetCore.Framework;
using PocPucMinas.CommonLib.Extensions;
using PocPucMinas.CommonLib.Services.Interfaces;
using PocPucMinas.DataDriver.Context;

namespace PocPucMinas.SegurancaComunicacaoWebApi
{
    public class Program : BaseWebApiProgram
    {
        public static void Main(string[] args)
        {
            var webHost = ConstruirAplicacao<Startup>(args);

            using (var serviceScope = webHost.Services.CreateScope())
            {
                var dbInitializer = serviceScope.ServiceProvider.GetRequiredService<IDefaultDbContextInitializer>();
                var appsettingsLoader = serviceScope.ServiceProvider.GetRequiredService<IAppSettingsLoaderService>();
                var configuration = appsettingsLoader.GetConfiguration();

                Console.WriteLine($"Ambiente de execução carregado na aplicação: {configuration.GetAmbienteExecucao()}");
                var pausaInstanciacaoBancoDeDados = configuration.GetPausaParaInstanciacaoDoBancoDeDados();
                if (pausaInstanciacaoBancoDeDados.HasValue)
                {             
                    Console.WriteLine($"Pausando execução por {pausaInstanciacaoBancoDeDados.Value.TotalSeconds} segundos para aguardar instanciação do banco de dados.");
                    Thread.Sleep(pausaInstanciacaoBancoDeDados.Value);
                }

                // Cria o banco de dados
                try
                {
                    dbInitializer.EnsureCreated();
                }
                catch { }

                // Aplica migrações pendentes no banco de dados
                try
                {
                    dbInitializer.Migrate();
                }
                catch { }

                dbInitializer.Seed().GetAwaiter().GetResult();

            }

            webHost.Run();
        }
        
    }

}
