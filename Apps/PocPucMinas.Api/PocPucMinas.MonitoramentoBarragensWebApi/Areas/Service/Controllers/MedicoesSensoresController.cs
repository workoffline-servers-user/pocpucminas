using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using PocPucMinas.MonitoramentoBarragensWebApi.Classes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PocPucMinas.QosLib.Interfaces;
using PocPucMinas.Business.Services.Interfaces;
using PocPucMinas.DataLib.Models;
using PocPucMinas.AspNetCore.Security.Attributes;

namespace PocPucMinas.MonitoramentoBarragensWebApi.Areas.Service.Controllers
{

    /// <remarks>
    /// Controller de serviço, responsável por recuperr dados 
    /// relacionados à medições de sensores e parâmetros relacionados
    /// </remarks>
    public class MedicoesSensoresController : ServiceAreaController<MedicoesSensoresController>
    {
        private readonly IMonitoramentoBarragensService _service;

        public MedicoesSensoresController(IMonitoramentoBarragensService service, IQosService qosService) : base(qosService)
        {
            _service = service;
        }

        /// <remarks>
        /// Recupera a lista de medições ocorridas na última hora consideradas de alto risco de acordo com as faixas de medição parametrizadas por tipo de sensor
        /// </remarks>
        /// <returns>Lista de medições</returns>
        [HttpGet("GetListaDeMedicoesDeAltoRisco")]
        [Autorizacao]
        public async Task<IActionResult> GetListaDeMedicoesDeAltoRisco()
        {
            var retorno = await _service.GetListaDeMedicoesDeAltoRisco();
            return Ok(retorno);
        }

        /// <remarks>
        /// Persiste medições simuladas em banco de dados.
        /// </remarks>
        /// <param name="quantidadePorSensor">Quantidade de medições simuladas por sensor cadastrado</param>
        /// <returns>Quantidade total de medições simuladas inseridas</returns>
        [HttpPost("InserirMedicoesSimuladas")]
        [Autorizacao]
        public async Task<IActionResult> InserirMedicoesSimuladas([FromBody] int quantidadePorSensor)
        {
            var retorno = await _service.InserirMedicoesSimuladas(quantidadePorSensor);
            return Ok(retorno);
        }

        /// <remarks>
        /// Recupera todas as medições de sensores existentes, ordenadas pela data que ocorreu a medição.
        /// </remarks>
        /// <returns>Lista detalhada das medições</returns>
        [HttpGet("RecuperarMedicoes")]
        [Autorizacao]
        public async Task<IActionResult> RecuperarMedicoes()
        {
            var retorno = await _service.RecuperarMedicoes();
            return Ok(retorno);
        }

        /// <remarks>
        ///  Recupera os níveis de risco cadastrados no sistema.
        /// </remarks>
        /// <returns>Lista com informações relacionadas aos níveis de risco</returns>
        [HttpGet("RecuperarNiveisDeRisco")]
        [Autorizacao]
        public async Task<IActionResult> RecuperarNiveisDeRisco()
        {
            var retorno = await _service.RecuperarNiveisDeRisco();
            return Ok(retorno);
        }

        /// <remarks>
        ///  Recupera os parâmetros de medição cadastrados de acordo com o tipo de sensor.
        /// </remarks>
        /// <returns>Lista com informações relacionadas aos parâmetros de medição</returns>
        [HttpGet("RecuperarParametrosDeMedicao")]
        [Autorizacao]
        public async Task<IActionResult> RecuperarParametrosDeMedicao()
        {
            var retorno = await _service.RecuperarParametrosDeMedicao();
            return Ok(retorno);
        }
    }
}
