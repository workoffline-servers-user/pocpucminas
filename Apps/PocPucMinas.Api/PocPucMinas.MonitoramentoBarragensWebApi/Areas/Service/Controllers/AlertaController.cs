using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using PocPucMinas.MonitoramentoBarragensWebApi.Classes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PocPucMinas.QosLib.Interfaces;
using PocPucMinas.Business.Services.Interfaces;
using PocPucMinas.DataLib.Models;
using PocPucMinas.AspNetCore.Security.Attributes;

namespace PocPucMinas.MonitoramentoBarragensWebApi.Areas.Service.Controllers
{

    /// <remarks>
    /// Controller de serviço, responsável por retornar informações relacionadas à alertas ocorridos no escopo do Monitoramento de Barragens
    /// </remarks>
    public class AlertaController : ServiceAreaController<AlertaController>
    {
        private readonly IMonitoramentoBarragensService _service;

        public AlertaController(IMonitoramentoBarragensService service, IQosService qosService) : base(qosService)
        {
            _service = service;
        }

        /// <remarks>
        /// Recupera as Regiões de Alerta cadastradas.
        /// </remarks>
        /// <returns>Lista com informações das regiões de alerta</returns>
        [HttpGet("RecuperarRegioesDeAlerta")]
        [Autorizacao]
        public async Task<IActionResult> RecuperarRegioesDeAlerta()
        {
            var retorno = await _service.RecuperarRegioesDeAlerta();
            return Ok(retorno);
        }

        /// <remarks>
        /// Recupera os Identificadores das Regiões de Alerta cadastradas.
        /// </remarks>
        /// <returns>Lista com identificadores das regiões de alerta</returns>
        [HttpGet("GetIdsRegioesDeAlertaAtingidas")]
        [Autorizacao]
        public async Task<IActionResult> GetIdsRegioesDeAlertaAtingidas()
        {
            var retorno = await _service.GetIdsRegioesDeAlertaAtingidas();
            return Ok(retorno);
        }

        /// <remarks>
        /// Verifica a existência de alertas ocorridos na última hora.
        /// </remarks>
        /// <returns>TRUE caso existam alertas ocorridos</returns>
        [HttpGet("VerificarExistenciaAlerta")]
        [Autorizacao]
        public async Task<IActionResult> VerificarExistenciaAlerta()
        {
            var retorno = await _service.VerificarExistenciaAlerta();
            return Ok(retorno);
        }
    }
}
