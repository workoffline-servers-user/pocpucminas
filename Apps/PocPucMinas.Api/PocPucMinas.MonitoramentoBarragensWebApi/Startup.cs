using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Newtonsoft.Json.Serialization;
using Microsoft.AspNetCore.Mvc.Razor;
using PocPucMinas.DataLib.Models;
using PocPucMinas.DataLib.Context;
using Microsoft.AspNetCore.Mvc;
using System.Reflection;
using PocPucMinas.ExceptionLib.Web;
using PocPucMinas.ExceptionLib.ViewModel;
using Swashbuckle.AspNetCore.Swagger;
using PocPucMinas.AspNetCore.Services;
using PocPucMinas.Business.Services.Interfaces;
using PocPucMinas.Business.Services.Implementations;
using AutoMapper;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using PocPucMinas.DataDriver.UnitsOfWork;
using System.Threading.Tasks;
using PocPucMinas.Business.Services.Implementations.Aeroportos;
using Microsoft.AspNetCore.Antiforgery;
using System;
using Microsoft.AspNetCore.Http;
using PocPucMinas.AspNetCore.Services.Interfaces;
using PocPucMinas.AspNetCore.Services.Implementations;
using PocPucMinas.AspNetCore.Framework;
using PocPucMinas.CommonLib.Extensions;
using PocPucMinas.DataDriver.Context;
using PocPucMinas.AspNetCore.Security.Services.Classes;
using PocPucMinas.AspNetCore.Security.Services.Interfaces;

namespace PocPucMinas.MonitoramentoBarragensWebApi
{
    public class Startup : BaseStartup
    {
        public IHostingEnvironment CurrentEnvironment { get; protected set; }
        public IAntiforgery AntiForgery { get; protected set; }

        public Startup(IConfiguration configuration, IHostingEnvironment hostingEnvironment, ILoggerFactory loggerFactory)
            : base(configuration, hostingEnvironment, loggerFactory)
        {
            
        }

        /// <summary>
        /// Método chamado em tempo de execução (runtime).
        /// Usado para adicionar configurar o pipeline das requisições HTTP.
        /// </summary>
        /// <param name="app">Instância do construtor da aplicação</param>
        /// <param name="antiforgery">Estrutura para previnir acessos de locais não autorizados</param>
        /// <param name="apiDescriptionProvider">Descritor da API</param>
        public void Configure(IApplicationBuilder app, IAntiforgery antiforgery, IApiVersionDescriptionProvider apiDescriptionProvider)
        {
            AntiForgery = antiforgery;
            base.Configure(app, apiDescriptionProvider);
        }

        public override void InjetaServicosEspecificosDaAplicacao(IServiceCollection services)
        {
            //Caso seja ambiente de teste, cria instância de banco de dados SQLite em memória
            if (Configuration.IsAmbienteTeste())
            {
                //Não implementado no escopo da POC
            }
            //Caso contrário utiliza banco de dados PostgreSQL
            else
            {
                services.AddEntityFrameworkNpgsql().AddDbContext<PocPucMinasDbContext>(options =>
                {
                    options.UseNpgsql(Configuration.GetConnectionString("PostgreSQL"),
                        b => b.MigrationsAssembly(Assembly.GetAssembly(typeof(PocPucMinasDbContext)).ToString()));
                });
            }

            //Antiforgery
            services.AddAntiforgery(options => options.HeaderName = "X-XSRF-TOKEN");
                        
            //Adiciona serviços da aplicação que possam ser injetados nos métodos dos Controllers da API
            //Services:
            services.AddScoped<IMonitoramentoBarragensService, MonitoramentoBarragensService>();
            services.AddTransient<IAutenticacaoCriptografiaService, AutenticacaoCriptografiaService>();
            services.AddTransient<IAutenticacaoService, AutenticacaoService>();
            services.AddTransient<IParametrosAutenticacao, ParametrosAutenticacao>();

            //Adiciona Units Of Work da aplicação que possam ser injetados nos métodos dos Controllers da API
            services.AddTransient<IBaseUnitOfWork, BaseUnitOfWork>();

            //Injeta o serviço para inicialização do banco de dados (seed)
            services.AddTransient<IDefaultDbContextInitializer, PocPucMinasDbContextInitializer>();            
        }

        public override void UtilizaServicosEspecificosDaAplicacao(IApplicationBuilder app)
        {                        
            app.Use(next => context =>
            {
                string path = context.Request.Path.Value;

                if (
                    string.Equals(path, "/", StringComparison.OrdinalIgnoreCase) ||
                    string.Equals(path, "/index.html", StringComparison.OrdinalIgnoreCase))
                {
                    // The request token can be sent as a JavaScript-readable cookie, 
                    // and Angular uses it by default.
                    var tokens = AntiForgery.GetAndStoreTokens(context);
                    context.Response.Cookies.Append("XSRF-TOKEN", tokens.RequestToken,
                        new CookieOptions() { HttpOnly = false });
                }

                return next(context);
            });
        }
    }

}
