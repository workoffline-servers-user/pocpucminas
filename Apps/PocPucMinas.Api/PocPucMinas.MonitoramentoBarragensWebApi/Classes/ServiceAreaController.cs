using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PocPucMinas.QosLib.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PocPucMinas.MonitoramentoBarragensWebApi.Classes
{
    [AllowAnonymous]
    [Area("Service")]
    public class ServiceAreaController<T> : PocPucMinasWebApiController<T> where T : PocPucMinasWebApiController<T>
    {
        const string CONTEXTO_QOS = "API_SERVICE_AREA";
        public ServiceAreaController(IQosService qosService) : base(qosService)
        {

        }
    }
}
