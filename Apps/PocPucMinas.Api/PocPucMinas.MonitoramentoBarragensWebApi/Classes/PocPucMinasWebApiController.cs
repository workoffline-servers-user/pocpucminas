using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PocPucMinas.AspNetCore.Framework;
using PocPucMinas.QosLib.Interfaces;

namespace PocPucMinas.MonitoramentoBarragensWebApi.Classes
{
    [ApiVersion("1.0")]
    public class PocPucMinasWebApiController<T> : BaseApiController<T> where T : PocPucMinasWebApiController<T>
    {        
        public PocPucMinasWebApiController(IQosService qosService) : base(qosService)
        {
            
        }
    }
}
