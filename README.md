# POC - Arquitetura de Software Distribuído

Aplicações de API e bibliotecas de apoio.

Containers Docker para instanciação de servidor de banco de dados e monitoramento de logs.

## Visão-geral da Solução Proposta

- Tecnologias
  - ASP.NET Core 2.2;
  - Bibliotecas desenvolvidas em .NET Core 2.2;
  - PostgreSQL 9.5;
  - Entity Framework Core com EF Migrations;
  - JSON Web Token (JWT);
  - Swagger;
  - Graylog;
  - Docker utilizado para deploy do PostgreSQL, ASP.NET Core e Graylog.



## Instalação

1. Pré-Requisitos:
   - [.NET Core 2.2] (https://www.microsoft.com/net/core)
   - [Node.js >= v8] (https://nodejs.org/en/download/)
   - [Docker] (https://docs.docker.com/engine/installation/)
   - [Docker Compose] (https://docs.docker.com/compose/install/)
   - [Visual Studio Community] (https://visualstudio.microsoft.com/pt-br/vs/community/)
2. Navegue até `Apps\_Services\PostgreSQL` e execute o comando `docker-compose up -d` para iniciar o servidor de banco de dados PostgreSQL 9.5.
3. Abra o arquivo de *Solution* `Apps\PocPucMinas.Api\PocPucMinas.sln` no *Visual Studio Community* e execute as 2 aplicações Web API (já configuradas como *Startup Projects* na *Solution*)
4. Abra o Navegador de Internet e navegue até:
   1. [http://localhost:5000/swagger/index.html](http://localhost:5000/swagger/index.html) - Módulo de Monitoramento de Barragens
   2. [http://localhost:5001/swagger/index.html](http://localhost:5001/swagger/index.html) - Módulo de Segurança e Comunicação

Esse template foi desenvolvido e testado em ambiente Windows, mas provavelmente funcionará sem restrições em ambiente MacOSX.



## Funcionamento

1. PocPucMinas.MonitoramentoBarragensWebApi:
   - API do **Módulo de Monitoramento de Barragens**, tendo controladores responsáveis por:
     - Autenticação de usuários;
     - Recuperar medições e parâmetros dos sensores;
     - Prover informações sobre alertas, baseados nas medições coletadas.
   - Após a autenticação, copie o Access Token gerado e insira como Bearer Token. O próprio Swagger já possui interface para inserir o Access Token e utilizá-lo automaticamente nos métodos que possuem autenticação.
2. PocPucMinas.SegurancaComunicacaoWebApi:
   - API do **Módulo de Segurança e Comunicação**, tendo controladores responsáveis por:
     - Autenticação de usuários;
     - Emitir mensagens de alertas pelos canais de comunicação às populações potencialmente atingidas por rompimento de barragens
       - Utiliza a comunicação através de chamada de API (com autenticação) com o **Módulo de Monitoramento de Barragens**.
   - Após a autenticação, copie o Access Token gerado e insira como Bearer Token. O próprio Swagger já possui interface para inserir o Access Token e utilizá-lo automaticamente nos métodos que possuem autenticação.



## Credenciais para Autenticação

### Usuário de Teste 1

Token de Acesso: `45f6024b-b563-403d-a4a5-c1a1de91b772`

Senha: `123456`



### Usuário de Teste 2

Token de Acesso: `7ed24851-487d-43b2-9e06-5fa283001529`

Senha: `654321`



## Modelo de Dados

![](Assets\modelo-dados.png)